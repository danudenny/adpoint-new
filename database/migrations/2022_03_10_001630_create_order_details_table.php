<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrderDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        if (!Schema::hasTable('order_details')) {
            Schema::create('order_details', function (Blueprint $table) {
                $table->integer('id', true);
                $table->integer('order_id');
                $table->integer('seller_id')->nullable();
                $table->integer('product_id');
                $table->text('variation')->nullable();
                $table->float('price', 24)->nullable();
                $table->float('total', 24)->nullable();
                $table->integer('quantity')->nullable();
                $table->date('start_date')->nullable();
                $table->date('end_date')->nullable();
                $table->integer('status')->nullable();
                $table->string('rejected')->nullable();
                $table->boolean('is_paid')->default(0);
                $table->string('file_advertising')->nullable();
                $table->timestamps();
                $table->index(['order_id', 'start_date', 'end_date'], 'INDEXBY_DATE');
            });
        }
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('order_details');
	}

}
