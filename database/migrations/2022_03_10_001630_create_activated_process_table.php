<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateActivatedProcessTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('activated_process', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('order_detail_id');
			$table->integer('status')->nullable();
			$table->timestamps();
			$table->dateTime('time_1')->nullable();
			$table->dateTime('time_2')->nullable();
			$table->dateTime('time_3')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('activated_process');
	}

}
