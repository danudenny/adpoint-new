<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateXenditPaymentHistoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        if (!Schema::hasTable('xendit_payment_histories')) {
            Schema::create('xendit_payment_histories', function (Blueprint $table) {
                $table->increments('id');
                $table->string('xendit_id', 191);
                $table->string('status', 191);
                $table->string('currency', 191);
                $table->string('owner_id', 191);
                $table->string('external_id', 191);
                $table->string('bank_code', 191);
                $table->string('merchant_code', 191);
                $table->string('name', 191);
                $table->string('account_number', 191);
                $table->decimal('expected_amount', 24);
                $table->dateTime('expiration_date');
                $table->timestamps();
                $table->integer('order_detail_id');
                $table->integer('user_id')->nullable();
            });
        }
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('xendit_payment_histories');
	}

}
