<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEvidencesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        if (!Schema::hasTable('evidences')) {
            Schema::create('evidences', function (Blueprint $table) {
                $table->integer('id', true);
                $table->integer('order_detail_id')->nullable();
                $table->string('no_bukti')->nullable();
                $table->string('no_order')->nullable();
                $table->text('file', 65535)->nullable();
                $table->integer('status')->nullable();
                $table->timestamps();
            });
        }
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('evidences');
	}

}
