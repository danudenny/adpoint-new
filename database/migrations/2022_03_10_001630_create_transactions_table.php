<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTransactionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        if (!Schema::hasTable('transactions')) {
            Schema::create('transactions', function (Blueprint $table) {
                $table->integer('id', true);
                $table->integer('user_id')->nullable();
                $table->string('code')->nullable();
                $table->boolean('payment_status')->nullable()->default(0);
                $table->string('payment_type')->nullable();
                $table->string('payment_detail')->nullable();
                $table->string('file_advertising')->nullable();
                $table->string('description')->nullable();
                $table->integer('viewed')->nullable();
                $table->string('status')->nullable();
                $table->string('is_rejected')->nullable();
                $table->timestamps();
            });
        }
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('transactions');
	}

}
