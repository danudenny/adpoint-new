<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateConfirmPaymentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        if (!Schema::hasTable('confirm_payments')) {
            Schema::create('confirm_payments', function (Blueprint $table) {
                $table->integer('id', true);
                $table->integer('user_id')->nullable();
                $table->string('code_trx', 128)->nullable();
                $table->string('nama')->nullable();
                $table->string('nama_bank')->nullable();
                $table->string('no_rek')->nullable();
                $table->string('bukti')->nullable();
                $table->boolean('approved')->nullable()->default(0);
                $table->timestamps();
            });
        }
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('confirm_payments');
	}

}
