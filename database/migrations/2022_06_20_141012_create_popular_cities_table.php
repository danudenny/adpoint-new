<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePopularCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('popular_cities')) {
            Schema::create('popular_cities', function (Blueprint $table) {
                $table->id();
                $table->string('city');
                $table->string('image');
                $table->text('description')->nullable();
                $table->tinyInteger('is_active')->default(0);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('popular_cities');
    }
}
