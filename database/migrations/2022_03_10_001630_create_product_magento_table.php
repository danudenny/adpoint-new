<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductMagentoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        if (!Schema::hasTable('product_magento')) {
            Schema::create('product_magento', function (Blueprint $table) {
                $table->string('sku')->nullable();
                $table->string('attribute_set_code')->nullable();
                $table->string('categories')->nullable();
                $table->string('name')->nullable();
                $table->string('description')->nullable();
                $table->string('thumbnail_image')->nullable();
                $table->string('additional_attributes')->nullable();
                $table->string('additional_images')->nullable();
                $table->text('custom_options')->nullable();
            });
        }
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('product_magento');
	}

}
