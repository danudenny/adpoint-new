<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('blog_categories')) {
            Schema::create('blog_categories', function (Blueprint $table) {
                $table->increments('id');
                $table->string('title')->nullable(false);
                $table->timestamps();
            });
        }

        if (!Schema::hasTable('blog_tags')) {
            Schema::create('blog_tags', function (Blueprint $table) {
                $table->increments('id');
                $table->string('title')->nullable(false);
                $table->timestamps();
            });
        }

        if (!Schema::hasTable('blog_images')) {
            Schema::create('blog_images', function (Blueprint $table) {
                $table->increments('id');
                $table->string('title')->nullable(false);
                $table->string('path')->nullable(false);
                $table->string('url');
                $table->text('blob');
                $table->string('alt_images');
                $table->string('original_filename')->nullable(false);
                $table->string('uploaded_filename')->nullable(false);
                $table->timestamps();
            });
        }

        if (!Schema::hasTable('blog_posts')) {
            Schema::create('blog_posts', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('author_id');
                $table->string('title')->nullable(false);
                $table->string('slug');
                $table->string('meta_title');
                $table->text('content')->nullable(false);
                $table->tinyInteger('published')->default(0);
                $table->integer('category_id');
                $table->integer('blog_image_id');
                $table->integer('tag_id')->nullable(false);
                $table->integer('count_views')->default(0);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog_categories');
        Schema::dropIfExists('blog_tags');
        Schema::dropIfExists('blog_images');
        Schema::dropIfExists('blog_posts');
    }
}
