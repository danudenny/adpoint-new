<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        if (!Schema::hasTable('orders')) {
            Schema::create('orders', function (Blueprint $table) {
                $table->integer('id', true);
                $table->integer('user_id')->nullable();
                $table->integer('transaction_id')->nullable();
                $table->integer('seller_id')->nullable();
                $table->text('code', 16777215)->nullable();
                $table->integer('approved')->nullable();
                $table->float('grand_total', 24)->nullable();
                $table->float('total', 24)->nullable();
                $table->float('tax', 24)->nullable();
                $table->float('adpoint_earning', 24)->nullable();
                $table->text('address')->nullable();
                $table->string('rejected')->nullable();
                $table->timestamps();
            });
        }
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('orders');
	}

}
