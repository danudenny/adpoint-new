<?php

use Database\Seeders\BanksSeeder;
use Database\Seeders\CitiesSeeder;
use Database\Seeders\DistrictsSeeder;
use Database\Seeders\StatesSeeder;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(BanksSeeder::class);
        $this->call(CitiesSeeder::class);
        $this->call(DistrictsSeeder::class);
        $this->call(StatesSeeder::class);
    }
}
