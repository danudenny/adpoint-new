<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DistrictsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = __DIR__ . '/../districts.csv';
        $header = ['id', 'city_id', 'name'];
        $data = csv_to_array($file, $header);

        DB::table('districts')->insertOrIgnore($data);
    }
}
