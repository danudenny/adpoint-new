<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = __DIR__ . '/../cities.csv';
        $header = ['id', 'state_id', 'name'];
        $data = csv_to_array($file, $header);

        DB::table('cities')->insertOrIgnore($data);
    }
}
