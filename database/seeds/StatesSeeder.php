<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = __DIR__ . '/../states.csv';
        $header = ['id', 'name'];
        $data = csv_to_array($file, $header);

        DB::table('states')->insertOrIgnore($data);
    }
}
