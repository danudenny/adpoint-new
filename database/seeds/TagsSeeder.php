<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class TagsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data =  array(
            ['title' => 'news'],
            ['title' => 'marketing'],
            ['title' => 'adsense'],
            ['title' => 'tips'],
            ['title' => 'market'],
            ['title' => 'customer'],
            ['title' => 'habbit'],
            ['title' => 'fun']
        );

        DB::table('blog_tags')->insertOrIgnore($data);
    }
}
