<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class BanksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('banks')->truncate();
        $data =  array(
            ['name' => 'BANK MANDIRI', 'code' => '008', 'acc_number' => 13300290929],
            ['name' => 'BANK BCA', 'code' => '014', 'acc_number' => 14090279782]
        );
        DB::table('banks')->insertOrIgnore($data);
    }
}
