<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register admin routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/admin', 'HomeController@admin_dashboard')->name('admin.dashboard')->middleware(['auth', 'admin']);
Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'admin']], function () {


	Route::resource('categories', 'CategoryController');
	Route::get('/categories/destroy/{id}', 'CategoryController@destroy')->name('categories.destroy');
	Route::post('/categories/featured', 'CategoryController@updateFeatured')->name('categories.featured');

	Route::resource('transaction', 'TransactionController');
	Route::get('/transaction/details/{id}', 'TransactionController@transaction_details')->name('transaction.details');
	Route::get('/transaction/show_payment/{id}', 'TransactionController@show_payment')->name('transaction.show.payment');
	Route::get('/transaction/change_to_paid/{id}', 'TransactionController@change_to_paid')->name('transaction.change.to.paid');
	Route::get('/transaction/show_invoice/{id}', 'TransactionController@show_invoice')->name('transaction.show.invoice');
	Route::get('/transaction/pdf_inv/{id}', 'TransactionController@pdf_invoice')->name('transaction.pdf.invoice');
	Route::get('/transaction/paid_invoice/{id}', 'TransactionController@paid_invoice')->name('transaction.paid.invoice');

	Route::post('/reject/details', 'TransactionController@reject_detail')->name('reject.detail');
	Route::get('/confirm-reject-to-buyer/{id}', 'TransactionController@confirm_reject_to_buyer')->name('confirm.reject.to.buyer');
	Route::get('/reject/detail/proses/{od_id}', 'TransactionController@reject_detail_proses')->name('reject.detail.proses');
	Route::get('/approve/detail/proses/{od_id}', 'TransactionController@approve_detail_proses')->name('approve.detail.proses');
	Route::get('/reject/order_details/{id}', 'TransactionController@partialReject')->name('reject.order.details');



	Route::get('/payment/list', 'PaymentController@admin_payment_index')->name('admin.payment.index');
	Route::get('/payment/confirm', 'PaymentController@admin_payment_confirm')->name('admin.payment.confirm');

	Route::resource('subcategories', 'SubCategoryController');
	Route::get('/subcategories/destroy/{id}', 'SubCategoryController@destroy')->name('subcategories.destroy');

	Route::resource('subsubcategories', 'SubSubCategoryController');
	Route::get('/subsubcategories/destroy/{id}', 'SubSubCategoryController@destroy')->name('subsubcategories.destroy');

	Route::resource('brands', 'BrandController');
	Route::get('/brands/destroy/{id}', 'BrandController@destroy')->name('brands.destroy');

	Route::get('/products/admin', 'ProductController@admin_products')->name('products.admin');
	Route::get('/products/seller', 'ProductController@seller_products')->name('products.seller');
	Route::get('/products/create', 'ProductController@create')->name('products.create');
	Route::get('/products/admin/{id}/edit', 'ProductController@admin_product_edit')->name('products.admin.edit');
	Route::get('/products/seller/{id}/edit', 'ProductController@seller_product_edit')->name('products.seller.edit');
	Route::post('/products/todays_deal', 'ProductController@updateTodaysDeal')->name('products.todays_deal');
	Route::post('/products/get_products_by_subsubcategory', 'ProductController@get_products_by_subsubcategory')->name('products.get_products_by_subsubcategory');
	Route::get('/products/bundle', 'ProductController@bundleProduct')->name('products.bundle.index');
	Route::get('/products/bundle/create', 'ProductController@create_bundleProduct')->name('products.bundle.create');

	//	Route::resource('bundle', 'BundleController');
	Route::post('bundle', 'BundleController@store')->name('bundle.store');
	Route::get('bundle/create', 'BundleController@create')->name('bundle.create');
	Route::get('bundle/{bundle}/edit', 'BundleController@edit')->name('bundle.edit');
	Route::patch('bundle/{bundle}', 'BundleController@update')->name('bundle.update');
	Route::post('bundle/status', 'BundleController@updateActive')->name('bundle.status');
	Route::get('bundle/{bundle}', 'BundleController@destroy')->name('bundle.destroy');

	Route::resource('sellers', 'SellerController');
	Route::get('/sellers/destroy/{id}', 'SellerController@destroy')->name('sellers.destroy');
	Route::get('/sellers/view/{id}/verification', 'SellerController@show_verification_request')->name('sellers.show_verification_request');
	Route::get('/sellers/approve/{id}', 'SellerController@approve_seller')->name('sellers.approve');
	Route::get('/sellers/reject/{id}', 'SellerController@reject_seller')->name('sellers.reject');
	Route::post('/sellers/payment_modal', 'SellerController@payment_modal')->name('sellers.payment_modal');
	Route::get('/seller/payments', 'PaymentController@payment_histories')->name('sellers.payment_histories');
	Route::get('/seller/payments/show/{id}', 'PaymentController@show')->name('sellers.payment_history');
	Route::get('/seller/details/{id}', 'SellerController@show_details_seller')->name('sellers.details');
	Route::get('/seller/assign_products', 'SellerController@assign_seller_product')->name('sellers.assignproduct');

	Route::resource('customers', 'CustomerController');
	Route::post('/customer/show_customer', 'CustomerController@showCustomer')->name('customer.show_customer');
	Route::post('/customer/accept', 'CustomerController@accept')->name('customer.accept');
	Route::post('/customer/reject', 'CustomerController@reject')->name('customer.reject');
	Route::get('/customers/destroy/{id}', 'CustomerController@destroy')->name('customers.destroy');
	Route::get('/newsletter', 'NewsletterController@index')->name('newsletters.index');
	Route::post('/newsletter/send', 'NewsletterController@send')->name('newsletters.send');

	Route::resource('profile', 'ProfileController');

	Route::post('/business-settings/update', 'BusinessSettingsController@update')->name('business_settings.update');
	Route::post('/business-settings/update/activation', 'BusinessSettingsController@updateActivationSettings')->name('business_settings.update.activation');
	Route::get('/activation', 'BusinessSettingsController@activation')->name('activation.index');
	Route::get('/payment-method', 'BusinessSettingsController@payment_method')->name('payment_method.index');
	Route::get('/social-login', 'BusinessSettingsController@social_login')->name('social_login.index');
	Route::get('/smtp-settings', 'BusinessSettingsController@smtp_settings')->name('smtp_settings.index');
	Route::get('/whatsapp-settings', 'BusinessSettingsController@whatsapp_settings')->name('whatsapp_settings.index');
	Route::get('/email-settings', 'BusinessSettingsController@email_settings')->name('email_setting.index');
	Route::post('/update-email-settings', 'BusinessSettingsController@update_email_setting')->name('update.email.settings');
	Route::post('/update-content-email', 'BusinessSettingsController@update_content_email')->name('update.content.email');
	Route::get('/google-analytics', 'BusinessSettingsController@google_analytics')->name('google_analytics.index');
	Route::get('/facebook-chat', 'BusinessSettingsController@facebook_chat')->name('facebook_chat.index');
	Route::post('/env_key_update', 'BusinessSettingsController@env_key_update')->name('env_key_update.update');
	Route::post('/whatsapp_chat_update', 'BusinessSettingsController@whatsapp_chat_update')->name('whatsapp_chat_update.update');
	Route::post('/payment_method_update', 'BusinessSettingsController@payment_method_update')->name('payment_method.update');
	Route::post('/google_analytics', 'BusinessSettingsController@google_analytics_update')->name('google_analytics.update');
	Route::post('/facebook_chat', 'BusinessSettingsController@facebook_chat_update')->name('facebook_chat.update');
	Route::get('/currency', 'CurrencyController@currency')->name('currency.index');
	Route::post('/currency/update', 'CurrencyController@updateCurrency')->name('currency.update');
	Route::post('/your-currency/update', 'CurrencyController@updateYourCurrency')->name('your_currency.update');
	Route::get('/verification/form', 'BusinessSettingsController@seller_verification_form')->name('seller_verification_form.index');
	Route::post('/verification/form', 'BusinessSettingsController@seller_verification_form_update')->name('seller_verification_form.update');
	Route::get('/vendor_commission', 'BusinessSettingsController@vendor_commission')->name('business_settings.vendor_commission');
	Route::post('/vendor_commission_update', 'BusinessSettingsController@vendor_commission_update')->name('business_settings.vendor_commission.update');

	Route::resource('/languages', 'LanguageController');
	Route::post('/languages/update_rtl_status', 'LanguageController@update_rtl_status')->name('languages.update_rtl_status');
	Route::get('/languages/destroy/{id}', 'LanguageController@destroy')->name('languages.destroy');
	Route::get('/languages/{id}/edit', 'LanguageController@edit')->name('languages.edit');
	Route::post('/languages/{id}/update', 'LanguageController@update')->name('languages.update');
	Route::post('/languages/key_value_store', 'LanguageController@key_value_store')->name('languages.key_value_store');

	Route::get('/frontend_settings/home', 'HomeController@home_settings')->name('home_settings.index');
	Route::post('/frontend_settings/home/top_10', 'HomeController@top_10_settings')->name('top_10_settings.store');
	Route::post('/frontend_settings/home/how_to', 'HomeController@how_to_settings')->name('how_to_settings');
	Route::get('/sellerpolicy/{type}', 'PolicyController@index')->name('sellerpolicy.index');
	Route::get('/returnpolicy/{type}', 'PolicyController@index')->name('returnpolicy.index');
	Route::get('/supportpolicy/{type}', 'PolicyController@index')->name('supportpolicy.index');
	Route::get('/terms/{type}', 'PolicyController@index')->name('terms.index');
	Route::get('/privacypolicy/{type}', 'PolicyController@index')->name('privacypolicy.index');

	//Policy Controller
	Route::post('/policies/store', 'PolicyController@store')->name('policies.store');

	Route::group(['prefix' => 'frontend_settings'], function () {
		Route::resource('sliders', 'SliderController');
		Route::get('/sliders/destroy/{id}', 'SliderController@destroy')->name('sliders.destroy');

		Route::resource('home_banners', 'BannerController');
		Route::get('/home_banners/create/{position}', 'BannerController@create')->name('home_banners.create');
		Route::post('/home_banners/update_status', 'BannerController@update_status')->name('home_banners.update_status');
		Route::get('/home_banners/destroy/{id}', 'BannerController@destroy')->name('home_banners.destroy');

		Route::resource('home_categories', 'HomeCategoryController');
		Route::get('/home_categories/destroy/{id}', 'HomeCategoryController@destroy')->name('home_categories.destroy');
		Route::post('/home_categories/update_status', 'HomeCategoryController@update_status')->name('home_categories.update_status');
		Route::post('/home_categories/get_subsubcategories_by_category', 'HomeCategoryController@getSubSubCategories')->name('home_categories.get_subsubcategories_by_category');
	});

	Route::resource('roles', 'RoleController');
	Route::get('/roles/destroy/{id}', 'RoleController@destroy')->name('roles.destroy');

	Route::resource('staffs', 'StaffController');
	Route::get('/staffs/destroy/{id}', 'StaffController@destroy')->name('staffs.destroy');

	Route::resource('flash_deals', 'FlashDealController');
	Route::get('/flash_deals/destroy/{id}', 'FlashDealController@destroy')->name('flash_deals.destroy');
	Route::post('/flash_deals/update_status', 'FlashDealController@update_status')->name('flash_deals.update_status');
	Route::post('/flash_deals/product_discount', 'FlashDealController@product_discount')->name('flash_deals.product_discount');
	Route::post('/flash_deals/product_discount_edit', 'FlashDealController@product_discount_edit')->name('flash_deals.product_discount_edit');

	Route::get('/orders', 'OrderController@admin_orders')->name('orders.index.admin');
	Route::get('/orders/list', 'OrderController@list_orders')->name('orders.list.orders');
	Route::get('/orders/approve_by_admin/{id}', 'OrderController@approve_by_admin')->name('approve.by.admin');
	Route::post('/orders/confirm_to_buyer', 'OrderController@confirm_to_buyer')->name('confirm.to.buyer');
	Route::post('/orders/proses_confirm_to_buyer', 'OrderController@proses_confirm_to_buyer')->name('proses.confirm.to.buyer');
	Route::post('/orders/disapprove_by_admin', 'OrderController@disapprove_by_admin')->name('disapprove.by.admin');
	Route::get('/orders/{id}/show', 'OrderController@show')->name('orders.show');
	Route::get('/orders/destroy/{id}', 'OrderController@destroy')->name('orders.destroy');
	Route::get('/sales', 'OrderController@sales')->name('sales.index');

    Route::get('/invoices', 'InvoiceController@index')->name('invoices.index');
    Route::get('/invoices/detail/{id}', 'TransactionController@show_invoice')->name('invoices.detail');

	Route::resource('links', 'LinkController');
	Route::get('/links/destroy/{id}', 'LinkController@destroy')->name('links.destroy');

	Route::resource('generalsettings', 'GeneralSettingController');
	Route::get('/logo', 'GeneralSettingController@logo')->name('generalsettings.logo');
	Route::post('/logo', 'GeneralSettingController@storeLogo')->name('generalsettings.logo.store');
	Route::get('/color', 'GeneralSettingController@color')->name('generalsettings.color');
	Route::post('/color', 'GeneralSettingController@storeColor')->name('generalsettings.color.store');

	Route::resource('seosetting', 'SEOController');

	Route::post('/pay_to_seller', 'CommissionController@pay_to_seller')->name('commissions.pay_to_seller');

	//Reports
	Route::get('/stock_report', 'ReportController@stock_report')->name('stock_report.index');
	Route::get('/in_house_sale_report', 'ReportController@in_house_sale_report')->name('in_house_sale_report.index');
	Route::get('/seller_report', 'ReportController@seller_report')->name('seller_report.index');
	Route::get('/seller_sale_report', 'ReportController@seller_sale_report')->name('seller_sale_report.index');
	Route::get('/wish_report', 'ReportController@wish_report')->name('wish_report.index');

	//Coupons
	Route::resource('coupon', 'CouponController');
	Route::post('/coupon/get_form', 'CouponController@get_coupon_form')->name('coupon.get_coupon_form');
	Route::post('/coupon/get_form_edit', 'CouponController@get_coupon_form_edit')->name('coupon.get_coupon_form_edit');
	Route::get('/coupon/destroy/{id}', 'CouponController@destroy')->name('coupon.destroy');

	//Reviews
	Route::get('/reviews', 'ReviewController@index')->name('reviews.index');
	Route::post('/reviews/published', 'ReviewController@updatePublished')->name('reviews.published');

	//Support_Ticket
	Route::get('support_ticket/', 'SupportTicketController@admin_index')->name('support_ticket.admin_index');
	Route::get('support_ticket/{id}/show', 'SupportTicketController@admin_show')->name('support_ticket.admin_show');
	Route::post('support_ticket/reply', 'SupportTicketController@admin_store')->name('support_ticket.admin_store');

	// ! Blog Post Route -- Admin
	Route::group(['prefix' => 'blog', 'as' => 'blog.'], function() {
		Route::get('/', 'BlogController@index')->name('index');
		Route::get('/post', 'BlogController@create')->name('create');
        Route::get('/edit/{id}', 'BlogController@edit')->name('edit');
        Route::patch('/{id}', 'BlogController@update')->name('update');
        Route::post('/', 'BlogController@store')->name('store');
        Route::post('/published', 'BlogController@updatePublished')->name('published');
        Route::get('/destroy/{id}', 'BlogController@destroy')->name('destroy');
	});

	//  ! Blog Categories Route -- Admin
    Route::group(['prefix' => '/blog-categorie', 'as' => 'blog_categories.'], function () {
        Route::get('/', 'BlogCategoryController@index')->name('admin');
        Route::get('/show/{id}', 'BlogCategoryController@show')->name('show.admin');
        Route::post('/post', 'BlogCategoryController@store')->name('create.admin');
        Route::post('/update', 'BlogCategoryController@update')->name('update.admin');
        Route::get('/destroy/{id}', 'BlogCategoryController@destroy')->name('delete.admin');
    });

    Route::group(['prefix' => '/blog-tags', 'as' => 'blog_tags.'], function () {
        Route::get('/', 'BlogTagController@index')->name('index');
        Route::post('/post', 'BlogTagController@store')->name('store');
        Route::post('/update', 'BlogTagController@update')->name('update');
        Route::get('/destroy/{id}', 'BlogTagController@destroy')->name('destroy');
    });

    Route::group(['prefix' => '/popular-city', 'as' => 'popular-city.'], function () {
        Route::get('/', 'PopularCityController@index')->name('index');
        Route::get('/create', 'PopularCityController@create')->name('create');
        Route::post('/', 'PopularCityController@store')->name('store');
        Route::get('/edit/{id}', 'PopularCityController@edit')->name('edit');
        Route::patch('/{id}', 'PopularCityController@update')->name('update');
        Route::post('/activated', 'PopularCityController@updateActivated')->name('activated');
        Route::get('/destroy/{id}', 'PopularCityController@destroy')->name('destroy');
    });

    Route::group(['prefix' => '/testimonials', 'as' => 'testimonials.'], function() {
        Route::get('/', 'TestimonialController@index')->name('index');
        Route::get('/create', 'TestimonialController@create')->name('create');
        Route::post('/', 'TestimonialController@store')->name('store');
        Route::get('/edit/{id}', 'TestimonialController@edit')->name('edit');
        Route::patch('/{id}', 'TestimonialController@update')->name('update');
        Route::post('/activated', 'TestimonialController@updateActivated')->name('activated');
        Route::get('/destroy/{id}', 'TestimonialController@destroy')->name('destroy');
    });

    Route::group(['prefix' => '/benefits', 'as' => 'benefits.'], function() {
        Route::get('/', 'BenefitController@index')->name('index');
        Route::post('/', 'BenefitController@store')->name('store');
        Route::patch('/', 'BenefitController@update')->name('update');
        Route::get('/destroy/{id}', 'BenefitController@destroy')->name('destroy');
    });

    //monitoring chat
    Route::group(['prefix' => '/chat-monitoring', 'as' => 'chat-monitoring.'], function() {
        Route::get('/', 'MonitoringChatController@index')->name('index');
    });
});
