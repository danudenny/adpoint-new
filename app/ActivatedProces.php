<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActivatedProces extends Model
{
    protected $table = "activated_process";

    protected $fillable = [
        'order_detail_id',
        'status',
        'time_1',
        'time_2',
        'time_3',
        'created_at',
        'updated_at'
    ];
}
