<?php

namespace App\Providers;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Routing\UrlGenerator;

class AppServiceProvider extends ServiceProvider
{
  /**
   * Bootstrap any application services.
   *
   * @return void
   */
  public function boot(UrlGenerator $url)
  {
    Schema::defaultStringLength(191);
    if (app()->environment('production')) {
      $url->forceScheme('https');
    };
    Paginator::defaultView('pagination::bootstrap-4');
  }

  /**
   * Register any application services.
   *
   * @return void
   */
  public function register()
  {
    $this->app->register(\L5Swagger\L5SwaggerServiceProvider::class);
    if ($this->app->environment('local')) {
      //$this->app->register(\Laravel\Telescope\TelescopeServiceProvider::class);
      //$this->app->register(TelescopeServiceProvider::class);
    }
  }
}
