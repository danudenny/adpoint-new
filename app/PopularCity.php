<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PopularCity extends Model
{
    use HasFactory;

    protected $fillable = [
      'city',
      'image',
      'description'
    ];
    protected $appends  = array('count');

    /**
     * @return mixed
     */
    public function getCountAttribute() {
//        return 1;
        return Product::where('kota', 'LIKE', '%'.$this->city.'%')
            ->orWhere('alamat', 'LIKE', '%'.$this->city.'%')
            ->count();
    }
}
