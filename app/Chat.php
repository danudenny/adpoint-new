<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Chat extends Model
{
    use HasFactory, SoftDeletes;

    public function room()
    {
        return $this->belongsTo(ChatRoom::class);
    }

    public function userFrom()
    {
        return $this->belongsTo(User::class, 'from', 'id');
    }

    public function userTo()
    {
        return $this->belongsTo(User::class, 'to', 'id');
    }
}
