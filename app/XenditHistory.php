<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class XenditHistory extends Model
{
    protected $table = 'xendit_payment_histories';
    protected $fillable = [
        "status",
        "currency",
        "owner_id",
        "external_id",
        "bank_code",
        "merchant_code",
        "name",
        "account_number",
        "expected_amount",
        "expiration_date",
        "xendit_id",
        "order_detail_id",
        "user_id"
    ];
}
