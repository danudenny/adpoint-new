<?php

namespace App\Http\Controllers;

use App\BlogTag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Yajra\DataTables\Facades\DataTables;

class BlogTagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $tags = BlogTag::all();

        if ($request->ajax()) {

            $hasil = [];
            foreach ($tags as $data) {
                $hasil[] = $data;
            }

            return DataTables::of($hasil)
                ->addIndexColumn()
                ->addColumn('action', function ($tag) {
                    return "<div>
                                 <a class='btn btn-warning btn-sm' data-title='$tag->title' data-id='$tag->id' id='updateTag' href='javascript:void(0)'><i class='fa fa-pencil'></i> Edit</a>
                                 <a class='btn btn-danger btn-sm'
                                 onclick=confirm_modal('".route('blog_tags.destroy', $tag->id)."')><i class='fa fa-trash'></i> Delete</a>
                            </div>";
                })
                ->editColumn('created_at', function ($tag) {
                    $date = $tag->created_at;
                    return date('d-m-Y', strtotime($date));
                })
                ->rawColumns([
                    'action',
                ])
                ->make(true);
        }
        return view('blogs.tag.index', compact('tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
        ]);

        BlogTag::create([
            'title' => $request->title,
        ]);

        $notification = array(
            'message' => 'Create Blog Tag Successfully!',
            'alert-type' => 'success'
        );
        return Redirect::to('admin/blog-tags')->with($notification);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->validate([
            'id' => 'required',
            'title' => 'required',
        ]);

        $blogTag = BlogTag::findOrFail($request->id);
        if ($blogTag != null) {
            $blogTag->title = $request->title;
            $blogTag->save();
        }

        $notification = array(
            'message' => 'Update Blog Tag Successfully!',
            'alert-type' => 'success'
        );
        return Redirect::to('admin/blog-tags')->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $blogTag = BlogTag::findOrFail($id);

        if($blogTag->delete()) {
            $notification = array(
                'message' => 'Delete Blog Tag Successfully!',
                'alert-type' => 'success'
            );
            return Redirect::to('admin/blog-tags')->with($notification);
        }
        return abort(404);
    }
}
