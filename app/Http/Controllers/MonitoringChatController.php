<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ChatRoom;

class MonitoringChatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = ChatRoom::with('chats:id,chat_room_id,from,to')
        ->with(['chats.userFrom:id,name,user_type','chats.userTo:id,name,user_type'])
        ->whereHas('chats')->get();

        return view('chat_monitoring.index', compact('list'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
