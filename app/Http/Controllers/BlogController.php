<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BlogPost;
use App\BlogCategory;
use App\BlogTag;
use App\BlogImages;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use DB;

class BlogController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\View
     */
    public function index(Request $request)
    {
        $data = BlogPost::orderBy('created_at', 'desc')->paginate(10);
        return view('blogs.post.index', compact('data'));
    }

    /**
     * @return \Illuminate\Contracts\View\View
     */
    public function create()
    {
        $categories = BlogCategory::all(['id', 'title']);
        $tags = BlogTag::all(['id', 'title']);

        return view('blogs.post.create', compact('categories', 'tags'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {

            $imageId = array();
            if ($request->hasFile('images')) {
                foreach ($request->images as $image) {
                    $path = $image->store('uploads/blog/post');

                    $blogImages = new BlogImages;
                    $blogImages->title             = $image->getClientOriginalName();
                    $blogImages->path              = $path;
                    $blogImages->url               = url($path);
                    $blogImages->blob              = base64_encode($image);
                    $blogImages->alt_images        = $image->getClientOriginalName();
                    $blogImages->original_filename = $image->getClientOriginalName();
                    $blogImages->uploaded_filename = $image->getClientOriginalName();

                    if($blogImages->save()) {
                        array_push($imageId, $blogImages->id);
                    }
                }
            }

            $post = new BlogPost;
            $post->author_id = Auth::user()->id;
            $post->slug = Str::slug($request->title);
            $post->title = $request->title;
            $post->meta_title = $request->meta_title;
            $post->tag_id = json_encode($request->tag_id);
            $post->blog_image_id = json_encode($imageId);
            $post->content = $request->content;
            $post->category_id = $request->category_id;
            if ($post->save()) {
                DB::commit();
                flash(__('Blog post has been inserted successfully'))->success();
                return redirect()->route('blog.index');
            }

        } catch (\Throwable $th) {
            DB::rollback();
            flash(__('Something went wrong '. $th->getMessage()))->error();
            return back();
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $post = BlogPost::findOrFail(decrypt($id));
        $categories = BlogCategory::all(['id', 'title']);
        $tags = BlogTag::all(['id', 'title']);

        return view('blogs.post.edit', compact('post', 'categories', 'tags'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|void
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $post = BlogPost::findOrFail(decrypt($id));
            $imageId = array();
            if ($request->hasFile('images')) {

                //clear history images
                $images = json_decode($post->blog_image_id);
                $checkFile = BlogImages::whereIn('id', $images)->pluck('path');

                if(BlogImages::whereIn('id', $images)->delete()) {
                    foreach ($checkFile as $file) {
                        $file_path = public_path() . '/' . $file;
                        unlink($file_path);
                    }
                }

                foreach ($request->images as $image) {
                    $path = $image->store('uploads/blog/post');

                    $blogImages = new BlogImages;
                    $blogImages->title             = $image->getClientOriginalName();
                    $blogImages->path              = $path;
                    $blogImages->url               = url($path);
                    $blogImages->blob              = base64_encode($image);
                    $blogImages->alt_images        = $image->getClientOriginalName();
                    $blogImages->original_filename = $image->getClientOriginalName();
                    $blogImages->uploaded_filename = $image->getClientOriginalName();

                    if($blogImages->save()) {
                        array_push($imageId, $blogImages->id);
                    }
                }
                $post->blog_image_id = json_encode($imageId);
            }

            $post->author_id = Auth::user()->id;
            $post->slug = Str::slug($request->title);
            $post->title = $request->title;
            $post->meta_title = $request->meta_title;
            $post->content = $request->content;
            $post->category_id = $request->category_id;
            $post->tag_id = json_encode($request->tag_id);

            if ($post->save()) {
                DB::commit();
                flash(__('Blog post has been inserted successfully'))->success();
                return redirect()->route('blog.index');
            }

        } catch (\Throwable $th) {
            DB::rollback();
            flash(__('Something went wrong '. $th->getMessage()))->error();
            return back();
        }

    }

    /**
     * Update published blog post
     * @param Request $request
     * @return int
     */
    public function updatePublished(Request $request)
    {
        $post = BlogPost::findOrFail($request->id);
        $post->published = $request->published;
        if($post->save()) {
            flash(__('Post published updated successfully'))->success();
            return 1;
        }
        flash(__('Something went wrong'))->error();
        return 0;
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|void
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $post = BlogPost::findOrFail($id);
            $images = json_decode($post->blog_image_id);
            $checkFile = BlogImages::whereIn('id', $images)->pluck('path');

            if($post->delete() && BlogImages::whereIn('id', $images)->delete()) {
                //makesure for delete file images too
                foreach ($checkFile as $file) {
                    $file_path = public_path().'/'.$file;
                    unlink($file_path);
                }
                DB::commit();
                flash(__('Blog post has been deleted successfully'))->success();
                return redirect()->route('blog.index');
            }

        } catch (\Throwable $th) {
            DB::rollback();
            flash(__('Something went wrong '. $th->getMessage()))->error();
            return back();
        }
    }
}
