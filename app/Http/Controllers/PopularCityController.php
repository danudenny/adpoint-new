<?php

namespace App\Http\Controllers;

use App\PopularCity;
use Illuminate\Http\Request;

class PopularCityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = PopularCity::paginate(10);
        return view('popular_city.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('popular_city.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $path = null;
        if ($request->hasFile('image')) {
            $path = $request->image->store('uploads/locations');
        }

        $data = new PopularCity();
        $data->city = $request->city;
        $data->image = $path;
        $data->description = $request->description;
        if($data->save()) {
            flash(__('Popular city has been inserted successfully'))->success();
        } else {
            flash(__('something went wrong'))->error();
        }
        return redirect()->route('popular-city.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = PopularCity::findOrFail(decrypt($id));
        return view('popular_city.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = PopularCity::findOrFail($id);
        $path = $data->image;
        if ($request->hasFile('image')) {
            removeFile($path);
            $path = $request->image->store('uploads/locations');
        }
        $data->city = $request->city;
        $data->image = $path;
        $data->description = $request->description;

        if($data->save()) {
            flash(__('Popular city has been updated successfully'))->success();
        } else {
            flash(__('something went wrong'))->error();
        }
        return redirect()->route('popular-city.index');
    }

    /**
     * Update activated status popular city for show to users
     *
     * @param Request $request
     * @return int
     */
    public function updateActivated(Request $request)
    {
        $data = PopularCity::findOrFail($request->id);
        $data->is_active = (int) $request->is_active;
        if($data->save()) {
            flash(__('Activated updated successfully'))->success();
            return 1;
        }
        flash(__('Something went wrong'))->error();
        return 0;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = PopularCity::findOrFail($id);
        if(removeFile($data->image) && $data->delete()) {
            flash(__('Popular city has been deleted successfully'))->success();
            return redirect()->route('popular-city.index');
        }
        flash(__('Something went wrong'))->error();
        return back();
    }
}
