<?php

namespace App\Http\Controllers\Api;

use App\ActivatedProces;
use App\ConfirmPayment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Invoice;
use App\OrderDetail;
use App\Product;
use App\Transaction;
use Xendit\Xendit;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\XenditHistory;

class PaymentCtrl extends Controller
{
    public function getListVa()
    {
        Xendit::setApiKey(env('XENDIT_API_KEY'));

        $getVaList = \Xendit\VirtualAccounts::getVABanks();

        return response()->json([
            'data' => $getVaList
        ])->setStatusCode(200);
    }

    public function createVa(Request $request)
    {
        Xendit::setApiKey(env('XENDIT_API_KEY'));

        $params = [
            "external_id" => $request->external_id,
            "bank_code" => $request->bank_code,
            "name" => $request->name,
            "expected_amount" => $request->expected_amount,
            "expiration_date" => Carbon::now()->addHours(3)->toDateTimeLocalString()
        ];

        try {
            $createVA = \Xendit\VirtualAccounts::create($params);
            return response()->json([
                'data' => $createVA
            ])->setStatusCode(200);
        } catch (\Exception $e) {
            dd($e);
        }
    }

    public function xenditHistories(Request $request)
    {
        $data = [
            "status" =>  $request->status,
            "currency" => $request->currency,
            "owner_id" => $request->owner_id,
            "external_id" => $request->external_id,
            "bank_code" => $request->bank_code,
            "merchant_code" => $request->merchant_code,
            "name" => $request->name,
            "account_number" => $request->account_number,
            "expected_amount" => $request->expected_amount,
            "expiration_date" => $request->expiration_date,
            "xendit_id" => $request->xendit_id,
            "order_detail_id" => $request->order_detail_id,
        ];

        XenditHistory::create($data);

        return response()->json([
            'message' => 'Success'
        ])->setStatusCode(200);
    }

    public function callbackVa(Request $request)
    {

        $od_id = DB::table('xendit_payment_histories')
            ->select('xendit_payment_histories.order_detail_id', "orders.user_id", "xendit_payment_histories.name", 'xendit_payment_histories.account_number', 'xendit_payment_histories.expiration_date')
            ->leftJoin('order_details', 'order_details.id', 'xendit_payment_histories.order_detail_id')
            ->leftJoin('orders', 'orders.id', 'order_details.order_id')
            ->where('xendit_payment_histories.external_id', $request->external_id)
            ->where('xendit_payment_histories.status', 'PENDING')
            ->first();

        $payment = OrderDetail::where('id', $od_id->order_detail_id)
            ->exists();

        if ($payment) {
            $updateOrderDetails = OrderDetail::where('id', $od_id->order_detail_id)
                ->update([
                    'is_paid' => true,
                ]);

            if ($updateOrderDetails == 1) {
                $dataXenditHistory = [
                    "status" =>  'ACTIVE',
                    "currency" => $request->currency,
                    "owner_id" => $request->owner_id,
                    "external_id" => $request->external_id,
                    "bank_code" => $request->bank_code,
                    "merchant_code" => $request->merchant_code,
                    "name" => $od_id->name,
                    "account_number" => $od_id->account_number,
                    "expected_amount" => $request->amount,
                    "expiration_date" => $od_id->expiration_date,
                    "xendit_id" => $request->id,
                    "order_detail_id" => $od_id->order_detail_id,
                    "user_id" => $od_id->user_id
                ];

                $xenditExists = XenditHistory::where('order_detail_id', $od_id->order_detail_id)
                    ->where('status', 'ACTIVE')
                    ->exists();

                if (!$xenditExists) {
                    $createXendit = XenditHistory::create($dataXenditHistory);
                }

                if ($createXendit) {
                    $updateTrx = Transaction::leftJoin('orders', 'transactions.id', 'orders.transaction_id')
                        ->leftJoin('order_details', 'orders.id', 'order_details.order_id')
                        ->where('order_details.id', $od_id->order_detail_id)
                        ->update([
                            'transactions.status' => 'paid',
                            'transactions.payment_status' => 1,
                        ]);

                    if ($updateTrx == 1) {
                        $getTrxCode = OrderDetail::select('transactions.code', 'transactions.id')
                            ->leftJoin('orders', 'orders.id', 'order_details.order_id')
                            ->leftJoin('transactions', 'transactions.id', 'orders.transaction_id')
                            ->where('order_details.id', $od_id->order_detail_id)
                            ->first();

                        $dataConfirmedPayment = [
                            'user_id' => $od_id->user_id,
                            'code_trx' => $getTrxCode->code,
                            'nama' =>  $od_id->name,
                            'nama_bank' => $request->bank_code,
                            'no_rek' => $od_id->account_number,
                            'approved' => 1
                        ];

                        $createConfirmedPayment = ConfirmPayment::create($dataConfirmedPayment);

                        if ($createConfirmedPayment) {
                            $dataActivation = [
                                'order_detail_id' => $od_id->order_detail_id,
                                'status' => 0,
                                'created_at' => Carbon::now(),
                                'updated_at' => Carbon::now()
                            ];

                            ActivatedProces::create($dataActivation);

                            Product::leftJoin('order_details', 'order_details.product_id', 'products.id')
                                ->where('order_details.id', $od_id->order_detail_id)
                                ->update([
                                    'products.available' => 0
                                ]);

                            Invoice::create([
                                'transaction_id' => $getTrxCode->id,
                                'code' => 'INV-' . time()
                            ])->save();
                        }
                    }
                }
            }

            return response()->json([
                "message" => 'Gagal Bayar'
            ])->setStatusCode(400);
        } else {
            return response()->json([
                "message" => 'Order Not Found'
            ])->setStatusCode(400);
        }
    }

    public function createInvoice(Request $request, $args)
    {
        $date = new \DateTime();
        $redirectUrl = '';
        $defParams = [
            'external_id' => $request->external_id,
            'payer_email' => $request->payer_email,
            'description' => $request->description,
            'failure_redirect_url' => $redirectUrl,
            'success_redirect_url' => $redirectUrl
        ];

        $data = json_decode(json_encode($args), true);
        $defParams['failure_redirect_url'] = $data['redirect_url'];
        $defParams['success_redirect_url'] = $data['redirect_url'];
        $params = array_merge($defParams, $data);
        $response = [];

        try {
            $response = \Xendit\Invoice::create($params);
        } catch (\Throwable $e) {
            $response['message'] = $e->getMessage();
        }

        logger($response);
        return $response;
    }

    public function my_order($id)
    {
        $order = DB::table('order_details')
            ->select('orders.code', 'order_details.id as order_detail_id', 'users.name as user_name', 'users.email', 'users.name', 'order_details.variation', 'transactions.payment_type', 'products.name', 'order_details.total')
            ->leftJoin('orders', 'orders.id', 'order_details.order_id')
            ->leftJoin('products', 'products.id', 'order_details.product_id')
            ->leftJoin('users', 'users.id', 'orders.user_id')
            ->leftJoin('transactions', 'orders.transaction_id', 'transactions.id')
            ->where('order_details.id', $id)
            ->first();
        return response()->json($order);
    }

    // !HINT For Demo Purpose Only
    public function bypass_payment($id)
    {
        $order = DB::table('order_details')
            ->select('orders.code', 'order_details.id as order_detail_id', 'users.name as user_name', 'users.email', 'users.name', 'order_details.variation', 'transactions.payment_type', 'products.name', 'order_details.total')
            ->leftJoin('orders', 'orders.id', 'order_details.order_id')
            ->leftJoin('products', 'products.id', 'order_details.product_id')
            ->leftJoin('users', 'users.id', 'orders.user_id')
            ->leftJoin('transactions', 'orders.transaction_id', 'transactions.id')
            ->where('order_details.id', $id)
            ->first();
        return response()->json($order);
    }
}
