<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UploadCtrl extends Controller
{
    public function single_upload(Request $request)
    {
        switch ($request->parent) {
            case 'users':
                if ($request->child == "ktp") {
                    return $this->respone_success($request);
                } else if ($request->child == 'npwp') {
                    return $this->respone_success($request);
                } else {
                    return $this->respone_error('c');
                }
                break;
            case 'brands':
                if ($request->child == "logo") {
                    return $this->respone_success($request);
                } else {
                    return $this->respone_error('c');
                }
                break;
            case 'categories':
                if ($request->child == "banner") {
                    return $this->respone_success($request);
                } else if ($request->child == "icon") {
                    return $this->respone_success($request);
                } else {
                    return $this->respone_error('c');
                }
                break;
            case 'products':
                if ($request->child == "photos") {
                    return $this->respone_success($request);
                } else if ($request->child == "thumbnail") {
                    return $this->respone_success($request);
                } else if ($request->child == "featured") {
                    return $this->respone_success($request);
                } else if ($request->child == "flashdeal") {
                    return $this->respone_success($request);
                } else if ($request->child == "meta") {
                    return $this->respone_success($request);
                } else if ($request->child == "pdf") {
                    return $this->respone_success($request);
                } else {
                    return $this->respone_error('c');
                }
                break;
            default:
                return $this->respone_error('p');
                break;
        }
    }

    private function respone_success($request)
    {
        return response()->json([
            'file' => $request->file('file')->store('uploads/api/' . $request->parent . '/' . $request->child)
        ], 200);
    }

    private function respone_error($type)
    {
        if ($type == 'p') {
            return response()->json([
                'success'   => false,
                'message'   => 'Parent dir tidak sesuai!, silahkan sesuaikan parent dan child dengan contoh descripsi'
            ], 401);
        } else if ($type == 'c') {
            return response()->json([
                'success'   => false,
                'message'   => 'Child dir tidak sesuai!, silahkan sesuaikan parent dan child dengan contoh descripsi'
            ], 401);
        }
    }
}
