<?php

namespace App\Http\Controllers;

use App\BlogCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Yajra\DataTables\Facades\DataTables;

class BlogCategoryController extends Controller
{

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\View
     * @throws \Exception
     */
    public function index(Request $request)
    {
        $blogCategory = BlogCategory::all();
        if ($request->ajax()) {

            $hasil = [];
            foreach ($blogCategory as $data) {
                $hasil[] = $data;
            }

            return DataTables::of($hasil)
                ->addIndexColumn()
                ->addColumn('action', function ($blogCategory) {
                    return "<div>
                                 <a class='btn btn-warning btn-sm' data-title='$blogCategory->title' data-id='$blogCategory->id' id='updateCategory' href='javascript:void(0)'><i class='fa fa-pencil'></i> Edit</a>
                                 <a class='btn btn-danger btn-sm'
                                 onclick=confirm_modal('".route('blog_categories.delete.admin', $blogCategory->id)."')><i class='fa fa-trash'></i> Delete</a>
                            </div>";
                })
                ->addColumn('jumlah_post', function ($blogCategory) {
                    $jmlPost = DB::table('blog_posts as bp')
                        ->select('bp.id')
                        ->leftJoin('blog_categories as bc', 'bp.category_id', '=','bc.id')
                        ->where('bc.id', '=', $blogCategory->id)
                        ->get()->count();
                    return "<span class='label label-dark'>$jmlPost</span>";
                })
                ->editColumn('created_at', function ($blogCategory) {
                    $date = $blogCategory->created_at;
                    return date('d-m-Y', strtotime($date));
                })
                ->rawColumns([
                    'jumlah_post',
                    'action',
                ])
                ->make(true);
        }

        return view('blogs.category.index', compact('blogCategory'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
        ]);

        BlogCategory::create([
            'title' => $request->title,
        ]);

        $notification = array(
            'message' => 'Sukses Create Category!',
            'alert-type' => 'success'
        );
        return Redirect::to('admin/blog-categories')->with($notification);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request)
    {
        $request->validate([
            'id' => 'required',
            'title' => 'required',
        ]);

        $blogCategory = BlogCategory::findOrFail($request->id);
        if ($blogCategory != null) {
            $blogCategory->title = $request->title;
            $blogCategory->save();
        } else {
            abort(404);
        }

        $notification = array(
            'message' => 'Sukses Update Category',
            'alert-type' => 'success'
        );
        return Redirect::to('admin/blog-categories')->with($notification);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|never
     */
    public function destroy($id)
    {
        $blogCategory = BlogCategory::findOrFail($id);

        if($blogCategory->delete()) {
            $notification = array(
                'message' => 'Sukses Delete Category',
                'alert-type' => 'success'
            );
            return Redirect::to('admin/blog-categories')->with($notification);
        }
        return abort(404);
    }
}
