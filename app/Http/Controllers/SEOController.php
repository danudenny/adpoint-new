<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SeoSetting;

class SEOController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $seosetting = SeoSetting::first();
        return view('seo_settings.index', compact("seosetting"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $seosetting = SeoSetting::first();
        $seosetting->keyword = implode('|',$request->tags);
        $seosetting->author = $request->author;
        $seosetting->revisit = $request->revisit;
        $seosetting->sitemap_link = $request->sitemap;
        $seosetting->description = $request->description;
        if($seosetting->save()){
            flash(__('SEO Setting has been updated successfully'))->success();
            return redirect()->route('seosetting.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }

}
