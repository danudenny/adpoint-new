<?php

namespace App\Http\Controllers;

use App\Invoice;
use Illuminate\Http\Request;
use App\Order;
use Illuminate\Support\Facades\DB;
use PDF;
use Auth;

class InvoiceController extends Controller
{
    public function index(Request $request) {
        $invoices = DB::table('invoices as i')
            ->select('i.id', 'i.code as inv_number', 't.code as tr_number', 't.created_at as transaction_date', 't.payment_type')
            ->leftJoin('transactions as t', 't.id', 'i.transaction_id')
            ->where('t.payment_status', '=', 1)
            ->get();

        return view('invoices.index', compact('invoices'));
    }

    //downloads customer invoice
    public function customer_invoice_download($id)
    {
        $order = Order::findOrFail($id);
        $pdf = PDF::setOptions([
                        'isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true,
                        'logOutputFile' => storage_path('logs/log.htm'),
                        'tempDir' => storage_path('logs/')
                    ])->loadView('invoices.customer_invoice', compact('order'));
        return $pdf->download('order-'.$order->code.'.pdf');
    }

    //downloads seller invoice
    public function seller_invoice_download($id)
    {
        $order = Order::findOrFail($id);
        $pdf = PDF::setOptions([
                        'isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true,
                        'logOutputFile' => storage_path('logs/log.htm'),
                        'tempDir' => storage_path('logs/')
                    ])->loadView('invoices.seller_invoice', compact('order'));
        return $pdf->download('order-'.$order->code.'.pdf');
    }
}
