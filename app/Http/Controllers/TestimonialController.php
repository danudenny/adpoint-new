<?php

namespace App\Http\Controllers;

use App\Testimonial;
use Illuminate\Http\Request;

class TestimonialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Testimonial::orderBy('created_at', 'desc')->paginate(10);
        return view('testimonials.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('testimonials.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $path = null;
        if ($request->hasFile('avatar')) {
            $path = $request->avatar->store('uploads/users');
        }

        $data = new Testimonial();
        $data->name = $request->name;
        $data->avatar = $path;
        $data->office = $request->office;
        $data->position = $request->position;
        $data->testimoni = $request->testimoni;
        $data->rating = $request->rating;
        $data->via = $request->via;
        if($data->save()) {
            flash(__('Testimoni has been inserted successfully'))->success();
        } else {
            flash(__('something went wrong'))->error();
        }
        return redirect()->route('testimonials.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Testimonial::findOrFail(decrypt($id));
        return view('testimonials.update', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Testimonial::findOrFail($id);
        $path = $data->avatar;
        if ($request->hasFile('image')) {
            removeFile($path);
            $path = $request->avatar->store('uploads/users');
        }
        $data->name = $request->name;
        $data->avatar = $path;
        $data->office = $request->office;
        $data->position = $request->position;
        $data->testimoni = $request->testimoni;
        $data->rating = $request->rating;
        $data->via = $request->via;

        if($data->save()) {
            flash(__('Testimoni has been updated successfully'))->success();
        } else {
            flash(__('something went wrong'))->error();
        }
        return redirect()->route('testimonials.index');
    }

    /**
     * Update activated testimonial
     * @param Request $request
     * @return int
     */
    public function updateActivated(Request $request)
    {
        $testimoni = Testimonial::findOrFail($request->id);
        $testimoni->status = $request->status;
        if($testimoni->save()) {
            flash(__('Testimoni activated successfully'))->success();
            return 1;
        }
        flash(__('Something went wrong'))->error();
        return 0;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Testimonial::findOrFail($id);
        if(removeFile($data->avatar) && $data->delete()) {
            flash(__('Testimoni has been deleted successfully'))->success();
            return redirect()->route('testimonials.index');
        }
        flash(__('Something went wrong'))->error();
        return back();
    }
}
