<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use App\Bundle;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BundleController extends Controller
{
    public function store(Request $request)
    {

        $bundle = new Bundle;
        $bundle->name = $request->name;
        $bundle->products = json_encode($request->products);
        $bundle->description = $request->description;
        $bundle->created_by = Auth::user()->id;
        $bundle->is_active = 1;

        $pricetag = DB::table('products')
            -> whereIn('id', $request->products)
            -> sum('unit_price');

        $bundle->total_price = $pricetag;

        if ($bundle->save()) {
            flash(__('Media Bundle Package has been inserted successfully'))->success();
            return redirect()->route('products.bundle.index');
        } else {
            flash(__('Something went wrong'))->error();
            return back();
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $bundle = Bundle::findOrFail(decrypt($id));
        $products = Product::all();
        return view('products.bundle.edit', compact('bundle', 'products'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {

        $bundle = Bundle::findOrFail($id);
        $bundle->name = $request->name;
        $bundle->products = json_encode($request->products);
        $bundle->description = $request->description;

        $pricetag = DB::table('products')
            -> whereIn('id', $request->products)
            -> sum('unit_price');

        $bundle->total_price = $pricetag;

        if ($bundle->save()) {
            flash(__('Media Bundle Package has been updated successfully'))->success();
            return redirect()->route('products.bundle.index');
        } else {
            flash(__('Something went wrong'))->error();
            return back();
        }
    }

    public function updateActive(Request $request)
    {
        $bundle = Bundle::findOrFail($request->id);
        $bundle->is_active = $request->status;
        if($bundle->save()){
            return 1;
        }
        return 0;
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $bundle = Bundle::where('id', $id)->first();

        if ($bundle->delete()) {
            flash(__('Media Bundle Package has been deleted successfully'))->success();
            return redirect()->route('products.bundle.index');
        }
        flash(__('Something went wrong'))->error();
        return back();
    }
}
