<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\ChatRoom;
use App\Chat;
use Illuminate\Support\Facades\Auth;
use App\Events\SendMessage;

class ChatController extends Controller
{
    /**
     * Display a listing of the resource.
     * List chat room
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userLogin = Auth::user()->id;
        $list = ChatRoom::with('chats:id,chat_room_id,from,to')
        ->with([
                'chats.userFrom:id,name,avatar_original',
                'chats.userFrom.shop:user_id,logo',
                'chats.userTo:id,name,avatar_original',
                'chats.userTo.shop:user_id,logo'])
        ->whereHas('chats', function($query) use ($userLogin) {
            $query->where('to', '=', $userLogin)->orWhere('from', '=', $userLogin);
        })->orderBy('id', 'desc')->get();

        return response()->json($list);
    }

    /**
     * Display a listing of the resource.
     * List chat by room id
     * @return \Illuminate\Http\Response
     */
    public function listChat(Request $request)
    {
        $list = Chat::where('chat_room_id', '=', $request->room_id)
        ->with(['userFrom:id,name,avatar_original',
                'userFrom.shop:user_id,logo',
                'userTo:id,name,avatar_original',
                'userTo.shop:user_id,logo'])
        ->get();

        return response()->json($list);
    }

    /**
     * create chat room
     * @return ChatRoom
     */
    public function createRoom()
    {
        $data = new ChatRoom();
        $data->name = Str::random(40);
        $data->save();

        return $data;
    }

    /**
     * send new message.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function sendMessage(Request $request)
    {
        $userLogin = Auth::user()->id;
        if ($request->get('room_id') == null) {
            $room = $this->createRoom()->id;
        } else {
            $room = $request->get('room_id');
        }

        $msg = new Chat();
        $msg->chat_room_id = $room;
        $msg->from = $userLogin;
        $msg->to = $request->to;
        $msg->body = $request->body;
        $msg->save();

        event(new SendMessage('New message', $room));

        return response()->json($msg);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateReadStatus($id)
    {
        $msg = Chat::findOrFail(decrypt($id));
        $msg->is_read = true;
        $msg->save();

        return response()->json($msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $chatRoom = ChatRoom::findOrFail(decrypt($id));
        if ($chatRoom->delete()) {
            $chatHistory = Chat::where('chat_room_id', '=', decrypt($id))->get();
            $chatHistory->delete();
        }
        return response()->json($chatRoom);
    }
}
