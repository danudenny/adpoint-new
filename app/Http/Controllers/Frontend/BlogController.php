<?php

namespace App\Http\Controllers\Frontend;

use App\BlogCategory;
use App\BlogPost;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Phpfastcache\Helper\Psr16Adapter;

class BlogController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\View
     */
    public function index(Request $request) {

        $posts = BlogPost::with('category:id,title')
            ->when($request->has('category'), function ($query) use ($request) {
                if ($request->get('category') === null) {
                    return $query;
                }
                return $query->where('category_id', '=', decrypt($request->category));
            })
            ->when($request->has('search'), function ($query) use ($request) {
                if ($request->get('search') === null) {
                    return $query;
                }
                return $query->where('title', 'like', '%' . $request->search . '%');
            })
            ->where('published', '=', 1)
            ->orderBy('id', 'desc')
            ->paginate(5);

        $popularPosts = $this->popularPosts();
        $blogCategories = $this->getBlogCategories();
        $igPost = [];// $this->igPost();

        return view('frontend.blog.post.index', compact('posts', 'popularPosts', 'blogCategories', 'igPost'));
    }

    /**
     * Display spesific resource of blog post
     *
     * @param $id
     * @return view
     */
    public function show($id) {
        $post = BlogPost::findOrFail(decrypt($id));
        $post->count_views += 1;
        $post->save();

        $popularPosts = $this->popularPosts();
        $blogCategories = $this->getBlogCategories();
        $igPost = [];// $this->igPost();

        return view('frontend.blog.post.detail', compact('post', 'popularPosts', 'blogCategories', 'igPost'));
    }

    /**
     * @return mixed
     */
    public function popularPosts()
    {
        return BlogPost::where('published', '=', 1)->orderBy('count_views', 'desc')->limit(2)->get();
    }

    /**
     * @return mixed
     */
    public function getBlogCategories()
    {
        return BlogCategory::select('id', 'title')->get();
    }

    /**
     * @return \InstagramScraper\Model\Media[]
     * @throws \InstagramScraper\Exception\InstagramAuthException
     * @throws \InstagramScraper\Exception\InstagramChallengeRecaptchaException
     * @throws \InstagramScraper\Exception\InstagramChallengeSubmitPhoneNumberException
     * @throws \InstagramScraper\Exception\InstagramException
     * @throws \InstagramScraper\Exception\InstagramNotFoundException
     * @throws \Phpfastcache\Exceptions\PhpfastcacheDriverCheckException
     * @throws \Phpfastcache\Exceptions\PhpfastcacheInvalidConfigurationException
     * @throws \Phpfastcache\Exceptions\PhpfastcacheLogicException
     * @throws \Psr\SimpleCache\InvalidArgumentException
     * @throws \ReflectionException
     */
    public function igPost()
    {
        $username = env('INSTAGRAM_LOGIN');
        $password = env('INSTAGRAM_PASSWORD');
        $instagram = \InstagramScraper\Instagram::withCredentials(
            $username,
            $password,
            new \Phpfastcache\Helper\Psr16Adapter('Files',
                new \Phpfastcache\Config\ConfigurationOption(['defaultTtl' => 43200]) //Auth cache 1 day
            )
        );
        $instagram->login();
        $instagram->saveSession();
        $posts  = $instagram->getFeed();
        $result = array();
        foreach ($posts as $key => $post) {
            if(!empty($post->getImageHighResolutionUrl())) {
                $image = file_get_contents($post->getImageHighResolutionUrl());
            }
            $imageData = base64_encode($image);
            $result[$key]["url"] = $imageData;
            $result[$key]["link"] = $post->getLink();
        }
        return $result;
    }
}
