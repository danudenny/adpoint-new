<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Benefit;

class BenefitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $type = 0;
        $title = "Benefit Seller";
        if ($request->get('type') == "buyer") {
            $type = 1;
            $title = "Benefit Buyer";
        }
        $data = Benefit::where('type', '=', $type)->orderBy('created_at', 'desc')->paginate(10);
        return view('benefits.index', compact('data', 'title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $path = null;
        if ($request->hasFile('image')) {
            $path = $request->image->store('uploads');
        }
        $typeString = $request->type == 0 ? 'Seller' : 'Buyer';

        $data = new Benefit();
        $data->type = $request->type;
        $data->image = $path;
        $data->description = $request->description;
        if($data->save()) {
            flash(__($typeString.' Benefit has been inserted successfully'))->success();
        } else {
            flash(__('something went wrong'))->error();
        }
        return redirect()->route('benefits.index', ['type' => $request->type == 0 ? 'seller' : 'buyer' ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = Benefit::findOrFail($request->id);
        $path = $data->image;
        if ($request->hasFile('image')) {
            removeFile($path);
            $path = $request->image->store('uploads');
        }
        $typeString = $request->type == 0 ? 'Seller' : 'Buyer';
        $data->type = $request->type;
        $data->image = $path;
        $data->description = $request->description;

        if($data->save()) {
            flash(__($typeString.' Benefit has been updated successfully'))->success();
        } else {
            flash(__('something went wrong'))->error();
        }
        return redirect()->route('benefits.index', ['type' => $request->type == 0 ? 'seller' : 'buyer' ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Benefit::findOrFail($id);
        $typeString = $data->type == 0 ? 'Seller' : 'Buyer';

        if(removeFile($data->image) && $data->delete()) {
            flash(__($typeString.' Benefit has been deleted successfully'))->success();
            return redirect()->route('benefits.index', ['type' => $data->type == 0 ? 'seller' : 'buyer' ]);
        }
        flash(__('Something went wrong'))->error();
        return back();
    }
}
