<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class BlogPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'             => 'required',
            'meta_title'        => 'required',
            'content'           => 'required',
            'tag_id'            => 'required',
            'category_id'       => 'required',
            'images'            => 'required',
            'images.*'          => 'image|mimes:jpeg,png,jpg,gif|max:2048',
        ];
    }
}
