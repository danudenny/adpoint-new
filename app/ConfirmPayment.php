<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConfirmPayment extends Model
{
    protected $fillable = [
        'user_id', 'code_trx', 'nama', 'nama_bank', 'no_rek', 'approved', 'bukti'
    ];
}
