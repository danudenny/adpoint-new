<?php return array (
  'app' => 
  array (
    'name' => 'Adpoint Indonesia',
    'vapid' => 'BJ8UEGdJOmmGa6SvxZyMJSkv_xaeRJ9mjG58CZU-gu3gZCu47AIAWLpgmTsVrGzjBKX655plYWTDP8FnYKUAtyk',
    'env' => 'local',
    'debug' => true,
    'url' => 'http://27.112.78.147/',
    'timezone' => 'Asia/Jakarta',
    'locale' => 'en',
    'fallback_locale' => 'en',
    'key' => 'base64:KO3McK7TY8dsxRi4WmgV8C0IkQ4oUdbI7rjCvRyR1tA=',
    'cipher' => 'AES-256-CBC',
    'providers' => 
    array (
      0 => 'Illuminate\\Auth\\AuthServiceProvider',
      1 => 'Illuminate\\Broadcasting\\BroadcastServiceProvider',
      2 => 'Illuminate\\Bus\\BusServiceProvider',
      3 => 'Illuminate\\Cache\\CacheServiceProvider',
      4 => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
      5 => 'Illuminate\\Cookie\\CookieServiceProvider',
      6 => 'Illuminate\\Database\\DatabaseServiceProvider',
      7 => 'Illuminate\\Encryption\\EncryptionServiceProvider',
      8 => 'Illuminate\\Filesystem\\FilesystemServiceProvider',
      9 => 'Illuminate\\Foundation\\Providers\\FoundationServiceProvider',
      10 => 'Illuminate\\Hashing\\HashServiceProvider',
      11 => 'Illuminate\\Mail\\MailServiceProvider',
      12 => 'Illuminate\\Notifications\\NotificationServiceProvider',
      13 => 'Illuminate\\Pagination\\PaginationServiceProvider',
      14 => 'Illuminate\\Pipeline\\PipelineServiceProvider',
      15 => 'Illuminate\\Queue\\QueueServiceProvider',
      16 => 'Illuminate\\Redis\\RedisServiceProvider',
      17 => 'Illuminate\\Auth\\Passwords\\PasswordResetServiceProvider',
      18 => 'Illuminate\\Session\\SessionServiceProvider',
      19 => 'Illuminate\\Translation\\TranslationServiceProvider',
      20 => 'Illuminate\\Validation\\ValidationServiceProvider',
      21 => 'Illuminate\\View\\ViewServiceProvider',
      22 => 'Laracasts\\Flash\\FlashServiceProvider',
      23 => 'Laravel\\Socialite\\SocialiteServiceProvider',
      24 => 'Barryvdh\\DomPDF\\ServiceProvider',
      25 => 'Yajra\\DataTables\\DataTablesServiceProvider',
      26 => 'App\\Providers\\AppServiceProvider',
      27 => 'App\\Providers\\AuthServiceProvider',
      28 => 'App\\Providers\\BroadcastServiceProvider',
      29 => 'App\\Providers\\EventServiceProvider',
      30 => 'App\\Providers\\RouteServiceProvider',
      31 => 'L5Swagger\\L5SwaggerServiceProvider',
      32 => 'PragmaRX\\Tracker\\Vendor\\Laravel\\ServiceProvider',
      33 => 'Igoshev\\Captcha\\Providers\\CaptchaServiceProvider',
      34 => 'SimpleSoftwareIO\\QrCode\\QrCodeServiceProvider',
      35 => 'Ixudra\\Curl\\CurlServiceProvider',
    ),
    'aliases' => 
    array (
      'App' => 'Illuminate\\Support\\Facades\\App',
      'Artisan' => 'Illuminate\\Support\\Facades\\Artisan',
      'Auth' => 'Illuminate\\Support\\Facades\\Auth',
      'Blade' => 'Illuminate\\Support\\Facades\\Blade',
      'Broadcast' => 'Illuminate\\Support\\Facades\\Broadcast',
      'Bus' => 'Illuminate\\Support\\Facades\\Bus',
      'Cache' => 'Illuminate\\Support\\Facades\\Cache',
      'Config' => 'Illuminate\\Support\\Facades\\Config',
      'Cookie' => 'Illuminate\\Support\\Facades\\Cookie',
      'Crypt' => 'Illuminate\\Support\\Facades\\Crypt',
      'DB' => 'Illuminate\\Support\\Facades\\DB',
      'Eloquent' => 'Illuminate\\Database\\Eloquent\\Model',
      'Event' => 'Illuminate\\Support\\Facades\\Event',
      'File' => 'Illuminate\\Support\\Facades\\File',
      'Gate' => 'Illuminate\\Support\\Facades\\Gate',
      'Hash' => 'Illuminate\\Support\\Facades\\Hash',
      'Lang' => 'Illuminate\\Support\\Facades\\Lang',
      'Log' => 'Illuminate\\Support\\Facades\\Log',
      'Mail' => 'Illuminate\\Support\\Facades\\Mail',
      'Notification' => 'Illuminate\\Support\\Facades\\Notification',
      'Password' => 'Illuminate\\Support\\Facades\\Password',
      'Queue' => 'Illuminate\\Support\\Facades\\Queue',
      'Redirect' => 'Illuminate\\Support\\Facades\\Redirect',
      'Redis' => 'Illuminate\\Support\\Facades\\Redis',
      'Request' => 'Illuminate\\Support\\Facades\\Request',
      'Response' => 'Illuminate\\Support\\Facades\\Response',
      'Route' => 'Illuminate\\Support\\Facades\\Route',
      'Schema' => 'Illuminate\\Support\\Facades\\Schema',
      'Session' => 'Illuminate\\Support\\Facades\\Session',
      'Storage' => 'Illuminate\\Support\\Facades\\Storage',
      'URL' => 'Illuminate\\Support\\Facades\\URL',
      'Validator' => 'Illuminate\\Support\\Facades\\Validator',
      'View' => 'Illuminate\\Support\\Facades\\View',
      'Socialite' => 'Laravel\\Socialite\\Facades\\Socialite',
      'PDF' => 'Barryvdh\\DomPDF\\PDF',
      'Tracker' => 'PragmaRX\\Tracker\\Vendor\\Laravel\\Facade',
      'QrCode' => 'SimpleSoftwareIO\\QrCode\\Facades\\QrCode',
      'Curl' => 'Ixudra\\Curl\\Facades\\Curl',
    ),
  ),
  'auth' => 
  array (
    'defaults' => 
    array (
      'guard' => 'web',
      'passwords' => 'users',
    ),
    'guards' => 
    array (
      'web' => 
      array (
        'driver' => 'session',
        'provider' => 'users',
      ),
      'api' => 
      array (
        'driver' => 'jwt',
        'provider' => 'users',
      ),
    ),
    'providers' => 
    array (
      'users' => 
      array (
        'driver' => 'eloquent',
        'model' => 'App\\User',
      ),
    ),
    'passwords' => 
    array (
      'users' => 
      array (
        'provider' => 'users',
        'table' => 'password_resets',
        'expire' => 60,
      ),
    ),
  ),
  'bone' => 
  array (
    'captcha' => 
    array (
      'middleware' => 
      array (
        0 => 'web',
      ),
      'routes' => 
      array (
        'image' => 'captcha/image',
        'image_tag' => 'captcha/image_tag',
      ),
      'blade' => 'captcha',
      'validator' => 'captcha',
      'generator' => 'Igoshev\\Captcha\\Captcha\\Generator\\GeneratorWaves',
      'storage' => 'Igoshev\\Captcha\\Captcha\\Storage\\SessionStorage',
      'code' => 'Igoshev\\Captcha\\Captcha\\Code\\SimpleCode',
      'font' => '/Users/sicepat/Sites/adpoint-new/vendor/bonecms/laravel-captcha/src/resources/fonts/IndiraK.ttf',
      'fontSize' => 26,
      'letterSpacing' => 4,
      'length' => 
      array (
        0 => 4,
        1 => 5,
      ),
      'chars' => '123456789',
      'width' => 191,
      'height' => 50,
      'background' => 'f2f2f2',
      'colors' => '2980b9',
      'scratches' => 
      array (
        0 => 1,
        1 => 6,
      ),
      'inputId' => 'captcha',
    ),
  ),
  'broadcasting' => 
  array (
    'default' => 'pusher',
    'connections' => 
    array (
      'pusher' => 
      array (
        'driver' => 'pusher',
        'key' => '71b68429916df972419b',
        'secret' => '41c1f3dbf1087f7921bc',
        'app_id' => '965119',
        'options' => 
        array (
          'cluster' => 'ap1',
          'useTLS' => true,
        ),
      ),
      'redis' => 
      array (
        'driver' => 'redis',
        'connection' => 'default',
      ),
      'log' => 
      array (
        'driver' => 'log',
      ),
      'null' => 
      array (
        'driver' => 'null',
      ),
    ),
  ),
  'cache' => 
  array (
    'default' => 'array',
    'stores' => 
    array (
      'apc' => 
      array (
        'driver' => 'apc',
      ),
      'array' => 
      array (
        'driver' => 'array',
      ),
      'database' => 
      array (
        'driver' => 'database',
        'table' => 'cache',
        'connection' => NULL,
      ),
      'file' => 
      array (
        'driver' => 'file',
        'path' => '/Users/sicepat/Sites/adpoint-new/storage/framework/cache/data',
      ),
      'memcached' => 
      array (
        'driver' => 'memcached',
        'persistent_id' => NULL,
        'sasl' => 
        array (
          0 => NULL,
          1 => NULL,
        ),
        'options' => 
        array (
        ),
        'servers' => 
        array (
          0 => 
          array (
            'host' => '127.0.0.1',
            'port' => 11211,
            'weight' => 100,
          ),
        ),
      ),
      'redis' => 
      array (
        'driver' => 'redis',
        'connection' => 'default',
      ),
    ),
    'prefix' => 'adpoint_indonesia_cache',
  ),
  'database' => 
  array (
    'default' => 'mysql',
    'connections' => 
    array (
      'sqlite' => 
      array (
        'driver' => 'sqlite',
        'database' => 'db_adpoint',
        'prefix' => '',
      ),
      'mysql' => 
      array (
        'driver' => 'mysql',
        'host' => 'localhost',
        'port' => '3306',
        'database' => 'db_adpoint',
        'username' => 'root',
        'password' => 'root',
        'charset' => 'utf8mb4',
        'collation' => 'utf8mb4_unicode_ci',
        'prefix' => '',
        'strict' => false,
        'engine' => NULL,
      ),
      'pgsql' => 
      array (
        'driver' => 'pgsql',
        'host' => 'localhost',
        'port' => '3306',
        'database' => 'db_adpoint',
        'username' => 'root',
        'password' => 'root',
        'charset' => 'utf8',
        'prefix' => '',
        'schema' => 'public',
        'sslmode' => 'prefer',
      ),
      'sqlsrv' => 
      array (
        'driver' => 'sqlsrv',
        'host' => 'localhost',
        'port' => '3306',
        'database' => 'db_adpoint',
        'username' => 'root',
        'password' => 'root',
        'charset' => 'utf8',
        'prefix' => '',
      ),
      'tracker' => 
      array (
        'driver' => 'mysql',
        'host' => 'localhost',
        'database' => 'db_adpoint',
        'username' => 'root',
        'password' => 'root',
      ),
    ),
    'migrations' => 'migrations',
    'redis' => 
    array (
      'client' => 'predis',
      'default' => 
      array (
        'host' => 'localhost',
        'port' => '6379',
        'database' => '0',
        'read_write_timeout' => -1,
      ),
    ),
  ),
  'datatables' => 
  array (
    'search' => 
    array (
      'smart' => true,
      'multi_term' => true,
      'case_insensitive' => true,
      'use_wildcards' => false,
      'starts_with' => false,
    ),
    'index_column' => 'DT_RowIndex',
    'engines' => 
    array (
      'eloquent' => 'Yajra\\DataTables\\EloquentDataTable',
      'query' => 'Yajra\\DataTables\\QueryDataTable',
      'collection' => 'Yajra\\DataTables\\CollectionDataTable',
      'resource' => 'Yajra\\DataTables\\ApiResourceDataTable',
    ),
    'builders' => 
    array (
    ),
    'nulls_last_sql' => ':column :direction NULLS LAST',
    'error' => NULL,
    'columns' => 
    array (
      'excess' => 
      array (
        0 => 'rn',
        1 => 'row_num',
      ),
      'escape' => '*',
      'raw' => 
      array (
        0 => 'action',
      ),
      'blacklist' => 
      array (
        0 => 'password',
        1 => 'remember_token',
      ),
      'whitelist' => '*',
    ),
    'json' => 
    array (
      'header' => 
      array (
      ),
      'options' => 0,
    ),
  ),
  'debugbar' => 
  array (
    'enabled' => false,
    'except' => 
    array (
      0 => 'telescope*',
    ),
    'storage' => 
    array (
      'enabled' => true,
      'driver' => 'file',
      'path' => '/Users/sicepat/Sites/adpoint-new/storage/debugbar',
      'connection' => NULL,
      'provider' => '',
    ),
    'include_vendors' => true,
    'capture_ajax' => true,
    'add_ajax_timing' => false,
    'error_handler' => false,
    'clockwork' => false,
    'collectors' => 
    array (
      'phpinfo' => true,
      'messages' => true,
      'time' => true,
      'memory' => true,
      'exceptions' => true,
      'log' => true,
      'db' => true,
      'views' => true,
      'route' => true,
      'auth' => true,
      'gate' => true,
      'session' => true,
      'symfony_request' => true,
      'mail' => true,
      'laravel' => false,
      'events' => true,
      'default_request' => false,
      'logs' => true,
      'files' => false,
      'config' => false,
      'cache' => false,
      'models' => true,
    ),
    'options' => 
    array (
      'auth' => 
      array (
        'show_name' => true,
      ),
      'db' => 
      array (
        'with_params' => true,
        'backtrace' => true,
        'timeline' => false,
        'explain' => 
        array (
          'enabled' => false,
          'types' => 
          array (
            0 => 'SELECT',
          ),
        ),
        'hints' => true,
      ),
      'mail' => 
      array (
        'full_log' => false,
      ),
      'views' => 
      array (
        'data' => false,
      ),
      'route' => 
      array (
        'label' => true,
      ),
      'logs' => 
      array (
        'file' => NULL,
      ),
      'cache' => 
      array (
        'values' => true,
      ),
    ),
    'inject' => true,
    'route_prefix' => '_debugbar',
    'route_domain' => NULL,
  ),
  'dompdf' => 
  array (
    'show_warnings' => false,
    'orientation' => 'portrait',
    'convert_entities' => true,
    'defines' => 
    array (
      'font_dir' => '/Users/sicepat/Sites/adpoint-new/storage/fonts',
      'font_cache' => '/Users/sicepat/Sites/adpoint-new/storage/fonts',
      'temp_dir' => '/var/folders/gq/8bly9h252qlf3bbpv061h6y40000gn/T',
      'chroot' => '/Users/sicepat/Sites/adpoint-new',
      'enable_font_subsetting' => false,
      'pdf_backend' => 'CPDF',
      'default_media_type' => 'screen',
      'default_paper_size' => 'a4',
      'default_font' => 'serif',
      'dpi' => 96,
      'enable_php' => false,
      'enable_javascript' => true,
      'enable_remote' => true,
      'font_height_ratio' => 1.1,
      'enable_html5_parser' => false,
    ),
  ),
  'eloquent-viewable' => 
  array (
    'models' => 
    array (
      'view' => 
      array (
        'table_name' => 'views',
        'connection' => 'mysql',
      ),
    ),
    'cache' => 
    array (
      'key' => 'cyrildewit.eloquent-viewable.cache',
      'store' => 'array',
      'lifetime_in_minutes' => 60,
    ),
    'session' => 
    array (
      'key' => 'cyrildewit.eloquent-viewable.session',
    ),
    'ignore_bots' => true,
    'honor_dnt' => false,
    'visitor_cookie_key' => 'eloquent_viewable',
    'ignored_ip_addresses' => 
    array (
    ),
  ),
  'filesystems' => 
  array (
    'default' => 'local',
    'cloud' => 's3',
    'disks' => 
    array (
      'local' => 
      array (
        'driver' => 'local',
        'root' => '/Users/sicepat/Sites/adpoint-new/public',
      ),
      'public' => 
      array (
        'driver' => 'local',
        'root' => '/Users/sicepat/Sites/adpoint-new/storage/app/public',
        'url' => 'http://27.112.78.147//storage',
        'visibility' => 'public',
      ),
      's3' => 
      array (
        'driver' => 's3',
        'key' => NULL,
        'secret' => NULL,
        'region' => NULL,
        'bucket' => NULL,
        'url' => NULL,
      ),
    ),
  ),
  'hashing' => 
  array (
    'driver' => 'bcrypt',
    'bcrypt' => 
    array (
      'rounds' => 10,
    ),
    'argon' => 
    array (
      'memory' => 1024,
      'threads' => 2,
      'time' => 2,
    ),
  ),
  'hooks' => 
  array (
    'enabled' => true,
  ),
  'horizon' => 
  array (
    'domain' => NULL,
    'path' => 'horizon',
    'use' => 'default',
    'prefix' => 'horizon:',
    'middleware' => 
    array (
      0 => 'web',
    ),
    'waits' => 
    array (
      'redis:default' => 60,
    ),
    'trim' => 
    array (
      'recent' => 60,
      'recent_failed' => 10080,
      'failed' => 10080,
      'monitored' => 10080,
    ),
    'fast_termination' => false,
    'memory_limit' => 64,
    'environments' => 
    array (
      'production' => 
      array (
        'supervisor-1' => 
        array (
          'connection' => 'redis',
          'queue' => 
          array (
            0 => 'default',
          ),
          'balance' => 'simple',
          'processes' => 10,
          'tries' => 1,
        ),
      ),
      'local' => 
      array (
        'supervisor-1' => 
        array (
          'connection' => 'redis',
          'queue' => 
          array (
            0 => 'default',
          ),
          'balance' => 'simple',
          'processes' => 3,
          'tries' => 1,
        ),
      ),
    ),
  ),
  'image' => 
  array (
    'driver' => 'gd',
  ),
  'jwt' => 
  array (
    'secret' => '78XIzbnb7mrleJ70Q4K0qIYZfrNZ02SNzVeqaFlydpI3HnR9cLg3wVA8q5FLx8GE',
    'keys' => 
    array (
      'public' => NULL,
      'private' => NULL,
      'passphrase' => NULL,
    ),
    'ttl' => 1440,
    'refresh_ttl' => 20160,
    'algo' => 'HS256',
    'required_claims' => 
    array (
      0 => 'iss',
      1 => 'iat',
      2 => 'exp',
      3 => 'nbf',
      4 => 'sub',
      5 => 'jti',
    ),
    'persistent_claims' => 
    array (
    ),
    'lock_subject' => true,
    'leeway' => 0,
    'blacklist_enabled' => true,
    'blacklist_grace_period' => 0,
    'decrypt_cookies' => false,
    'providers' => 
    array (
      'jwt' => 'Tymon\\JWTAuth\\Providers\\JWT\\Lcobucci',
      'auth' => 'Tymon\\JWTAuth\\Providers\\Auth\\Illuminate',
      'storage' => 'Tymon\\JWTAuth\\Providers\\Storage\\Illuminate',
    ),
  ),
  'l5-swagger' => 
  array (
    'default' => 'default',
    'documentations' => 
    array (
      'default' => 
      array (
        'api' => 
        array (
          'title' => 'L5 Swagger UI',
        ),
        'routes' => 
        array (
          'api' => 'api/documentation',
        ),
        'paths' => 
        array (
          'use_absolute_path' => true,
          'docs_json' => 'api-docs.json',
          'docs_yaml' => 'api-docs.yaml',
          'format_to_use_for_docs' => 'json',
          'annotations' => 
          array (
            0 => '/Users/sicepat/Sites/adpoint-new/app',
          ),
        ),
      ),
    ),
    'defaults' => 
    array (
      'routes' => 
      array (
        'docs' => 'docs',
        'oauth2_callback' => 'api/oauth2-callback',
        'middleware' => 
        array (
          'api' => 
          array (
          ),
          'asset' => 
          array (
          ),
          'docs' => 
          array (
          ),
          'oauth2_callback' => 
          array (
          ),
        ),
        'group_options' => 
        array (
        ),
      ),
      'paths' => 
      array (
        'docs' => '/Users/sicepat/Sites/adpoint-new/storage/api-docs',
        'views' => '/Users/sicepat/Sites/adpoint-new/resources/views/vendor/l5-swagger',
        'base' => NULL,
        'swagger_ui_assets_path' => 'vendor/swagger-api/swagger-ui/dist/',
        'excludes' => 
        array (
        ),
      ),
      'scanOptions' => 
      array (
        'analyser' => NULL,
        'analysis' => NULL,
        'processors' => 
        array (
        ),
        'pattern' => NULL,
        'exclude' => 
        array (
        ),
      ),
      'securityDefinitions' => 
      array (
        'securitySchemes' => 
        array (
        ),
        'security' => 
        array (
          0 => 
          array (
          ),
        ),
      ),
      'generate_always' => true,
      'generate_yaml_copy' => false,
      'proxy' => false,
      'additional_config_url' => NULL,
      'operations_sort' => NULL,
      'validator_url' => NULL,
      'ui' => 
      array (
        'display' => 
        array (
          'doc_expansion' => 'none',
          'filter' => true,
        ),
        'authorization' => 
        array (
          'persist_authorization' => false,
        ),
      ),
      'constants' => 
      array (
        'L5_SWAGGER_CONST_HOST' => 'http://my-default-host.com',
      ),
    ),
    'api' => 
    array (
      'title' => 'L5 Swagger UI',
    ),
    'routes' => 
    array (
      'api' => 'api/documentation',
      'docs' => 'docs',
      'oauth2_callback' => 'api/oauth2-callback',
      'middleware' => 
      array (
        'api' => 
        array (
        ),
        'asset' => 
        array (
        ),
        'docs' => 
        array (
        ),
        'oauth2_callback' => 
        array (
        ),
      ),
    ),
    'paths' => 
    array (
      'docs' => '/Users/sicepat/Sites/adpoint-new/public/api-docs',
      'docs_json' => 'api-docs.json',
      'docs_yaml' => 'api-docs.yaml',
      'annotations' => 
      array (
        0 => '/Users/sicepat/Sites/adpoint-new/app',
      ),
      'views' => '/Users/sicepat/Sites/adpoint-new/resources/views/vendor/l5-swagger',
      'base' => NULL,
      'swagger_ui_assets_path' => 'vendor/swagger-api/swagger-ui/dist/',
      'excludes' => 
      array (
      ),
    ),
    'security' => 
    array (
    ),
    'generate_always' => true,
    'generate_yaml_copy' => false,
    'swagger_version' => '3.0',
    'proxy' => false,
    'additional_config_url' => NULL,
    'operations_sort' => NULL,
    'validator_url' => NULL,
    'constants' => 
    array (
      'L5_SWAGGER_CONST_HOST' => 'http://localhost:8000/mobile/api',
    ),
  ),
  'larecipe' => 
  array (
    'docs' => 
    array (
      'route' => '/documentation',
      'path' => '/resources/docs',
      'landing' => 'overview',
      'middleware' => 
      array (
        0 => 'web',
      ),
    ),
    'versions' => 
    array (
      'default' => '1.0',
      'published' => 
      array (
        0 => '1.0',
      ),
    ),
    'settings' => 
    array (
      'auth' => false,
      'ga_id' => '',
      'middleware' => 
      array (
        0 => 'web',
      ),
    ),
    'cache' => 
    array (
      'enabled' => false,
      'period' => 5,
    ),
    'search' => 
    array (
      'enabled' => false,
      'default' => 'algolia',
      'engines' => 
      array (
        'internal' => 
        array (
          'index' => 
          array (
            0 => 'h2',
            1 => 'h3',
          ),
        ),
        'algolia' => 
        array (
          'key' => '',
          'index' => '',
        ),
      ),
    ),
    'ui' => 
    array (
      'code_theme' => 'dark',
      'fav' => '',
      'fa_v4_shims' => true,
      'show_side_bar' => true,
      'colors' => 
      array (
        'primary' => '#787AF6',
        'secondary' => '#2b9cf2',
      ),
      'theme_order' => NULL,
    ),
    'seo' => 
    array (
      'author' => '',
      'description' => '',
      'keywords' => '',
      'og' => 
      array (
        'title' => '',
        'type' => 'article',
        'url' => '',
        'image' => '',
        'description' => '',
      ),
    ),
    'forum' => 
    array (
      'enabled' => false,
      'default' => 'disqus',
      'services' => 
      array (
        'disqus' => 
        array (
          'site_name' => '',
        ),
      ),
    ),
    'packages' => 
    array (
      'path' => 'larecipe-components',
    ),
    'blade-parser' => 
    array (
      'regex' => 
      array (
        'code-blocks' => 
        array (
          'match' => '/\\<pre\\>(.|\\n)*?<\\/pre\\>/',
          'replacement' => '<code-block>',
        ),
      ),
    ),
  ),
  'logging' => 
  array (
    'default' => 'stack',
    'channels' => 
    array (
      'stack' => 
      array (
        'driver' => 'stack',
        'channels' => 
        array (
          0 => 'daily',
        ),
      ),
      'single' => 
      array (
        'driver' => 'single',
        'path' => '/Users/sicepat/Sites/adpoint-new/storage/logs/laravel.log',
        'level' => 'debug',
      ),
      'daily' => 
      array (
        'driver' => 'daily',
        'path' => '/Users/sicepat/Sites/adpoint-new/storage/logs/laravel.log',
        'level' => 'debug',
        'days' => 7,
        'permissions' => 436,
      ),
      'slack' => 
      array (
        'driver' => 'slack',
        'url' => NULL,
        'username' => 'Laravel Log',
        'emoji' => ':boom:',
        'level' => 'critical',
      ),
      'papertrail' => 
      array (
        'driver' => 'monolog',
        'level' => 'debug',
        'handler' => 'Monolog\\Handler\\SyslogUdpHandler',
        'handler_with' => 
        array (
          'host' => NULL,
          'port' => NULL,
        ),
      ),
      'stderr' => 
      array (
        'driver' => 'monolog',
        'handler' => 'Monolog\\Handler\\StreamHandler',
        'with' => 
        array (
          'stream' => 'php://stderr',
        ),
      ),
      'syslog' => 
      array (
        'driver' => 'syslog',
        'level' => 'debug',
      ),
      'errorlog' => 
      array (
        'driver' => 'errorlog',
        'level' => 'debug',
      ),
    ),
  ),
  'mail' => 
  array (
    'driver' => 'smtp',
    'host' => 'smtp.mailtrap.io',
    'port' => '2525',
    'from' => 
    array (
      'address' => 'adpointdev@gmail.com',
      'name' => 'Adpoint',
    ),
    'encryption' => 'tls',
    'username' => 'b7d6a6d48c5213',
    'password' => '814d1105c5ba72',
    'sendmail' => '/usr/sbin/sendmail -bs',
    'markdown' => 
    array (
      'theme' => 'default',
      'paths' => 
      array (
        0 => '/Users/sicepat/Sites/adpoint-new/resources/views/vendor/mail',
      ),
    ),
  ),
  'models' => 
  array (
    '*' => 
    array (
      'path' => '/Users/sicepat/Sites/adpoint-new/app/Models',
      'namespace' => 'App\\Models',
      'parent' => 'Illuminate\\Database\\Eloquent\\Model',
      'use' => 
      array (
      ),
      'connection' => false,
      'timestamps' => true,
      'soft_deletes' => true,
      'date_format' => 'Y-m-d H:i:s',
      'per_page' => 15,
      'base_files' => false,
      'snake_attributes' => true,
      'indent_with_space' => 0,
      'qualified_tables' => false,
      'hidden' => 
      array (
        0 => '*secret*',
        1 => '*password',
        2 => '*token',
      ),
      'guarded' => 
      array (
      ),
      'casts' => 
      array (
        '*_json' => 'json',
      ),
      'except' => 
      array (
        0 => 'migrations',
      ),
      'only' => 
      array (
      ),
      'table_prefix' => '',
      'lower_table_name_first' => false,
      'model_names' => 
      array (
      ),
      'relation_name_strategy' => 'related',
      'with_property_constants' => false,
      'pluralize' => true,
      'override_pluralize_for' => 
      array (
      ),
      'fillable_in_base_files' => false,
    ),
  ),
  'queue' => 
  array (
    'default' => 'database',
    'connections' => 
    array (
      'sync' => 
      array (
        'driver' => 'sync',
      ),
      'database' => 
      array (
        'driver' => 'database',
        'table' => 'jobs',
        'queue' => 'default',
        'retry_after' => 90,
      ),
      'beanstalkd' => 
      array (
        'driver' => 'beanstalkd',
        'host' => 'localhost',
        'queue' => 'default',
        'retry_after' => 90,
      ),
      'sqs' => 
      array (
        'driver' => 'sqs',
        'key' => 'your-public-key',
        'secret' => 'your-secret-key',
        'prefix' => 'https://sqs.us-east-1.amazonaws.com/your-account-id',
        'queue' => 'your-queue-name',
        'region' => 'us-east-1',
      ),
      'redis' => 
      array (
        'driver' => 'redis',
        'connection' => 'default',
        'queue' => 'default',
        'retry_after' => 90,
        'block_for' => NULL,
      ),
    ),
    'failed' => 
    array (
      'database' => 'mysql',
      'table' => 'failed_jobs',
    ),
  ),
  'services' => 
  array (
    'mailgun' => 
    array (
      'domain' => NULL,
      'secret' => NULL,
      'endpoint' => 'api.mailgun.net',
    ),
    'ses' => 
    array (
      'key' => NULL,
      'secret' => NULL,
      'region' => 'us-east-1',
    ),
    'sparkpost' => 
    array (
      'secret' => NULL,
    ),
    'stripe' => 
    array (
      'model' => 'App\\User',
      'key' => NULL,
      'secret' => NULL,
    ),
    'google' => 
    array (
      'client_id' => NULL,
      'client_secret' => NULL,
      'redirect' => 'http://27.112.78.147//social-login/google/callback',
    ),
    'facebook' => 
    array (
      'client_id' => NULL,
      'client_secret' => NULL,
      'redirect' => 'http://27.112.78.147//social-login/facebook/callback',
    ),
  ),
  'session' => 
  array (
    'driver' => 'file',
    'lifetime' => '1440',
    'expire_on_close' => false,
    'encrypt' => false,
    'files' => '/Users/sicepat/Sites/adpoint-new/storage/framework/sessions',
    'connection' => NULL,
    'table' => 'sessions',
    'store' => NULL,
    'lottery' => 
    array (
      0 => 2,
      1 => 100,
    ),
    'cookie' => 'adpoint_indonesia_session',
    'path' => '/',
    'domain' => NULL,
    'secure' => false,
    'http_only' => true,
    'same_site' => NULL,
  ),
  'tracker' => 
  array (
    'enabled' => true,
    'cache_enabled' => true,
    'use_middleware' => true,
    'do_not_track_robots' => false,
    'do_not_track_environments' => 
    array (
    ),
    'do_not_track_routes' => 
    array (
      0 => 'tracker.stats.*',
    ),
    'do_not_track_paths' => 
    array (
      0 => 'api/*',
    ),
    'do_not_track_ips' => 
    array (
    ),
    'log_untrackable_sessions' => true,
    'log_enabled' => false,
    'console_log_enabled' => false,
    'log_sql_queries' => false,
    'connection' => 'tracker',
    'do_not_log_sql_queries_connections' => 
    array (
      0 => 'tracker',
    ),
    'geoip_database_path' => '/Users/sicepat/Sites/adpoint-new/config/geoip',
    'log_sql_queries_bindings' => false,
    'log_events' => false,
    'log_only_events' => 
    array (
    ),
    'id_columns_names' => 
    array (
      0 => 'id',
    ),
    'do_not_log_events' => 
    array (
      0 => 'illuminate.log',
      1 => 'eloquent.*',
      2 => 'router.*',
      3 => 'composing: *',
      4 => 'creating: *',
    ),
    'log_geoip' => false,
    'log_user_agents' => false,
    'log_users' => false,
    'log_devices' => false,
    'log_languages' => false,
    'log_referers' => false,
    'log_paths' => false,
    'log_queries' => false,
    'log_routes' => false,
    'log_exceptions' => false,
    'store_cookie_tracker' => false,
    'tracker_cookie_name' => 'please_change_this_cookie_name',
    'tracker_session_name' => 'tracker_session',
    'user_model' => 'PragmaRX\\Tracker\\Vendor\\Laravel\\Models\\User',
    'session_model' => 'PragmaRX\\Tracker\\Vendor\\Laravel\\Models\\Session',
    'log_model' => 'PragmaRX\\Tracker\\Vendor\\Laravel\\Models\\Log',
    'path_model' => 'PragmaRX\\Tracker\\Vendor\\Laravel\\Models\\Path',
    'query_model' => 'PragmaRX\\Tracker\\Vendor\\Laravel\\Models\\Query',
    'query_argument_model' => 'PragmaRX\\Tracker\\Vendor\\Laravel\\Models\\QueryArgument',
    'agent_model' => 'PragmaRX\\Tracker\\Vendor\\Laravel\\Models\\Agent',
    'device_model' => 'PragmaRX\\Tracker\\Vendor\\Laravel\\Models\\Device',
    'cookie_model' => 'PragmaRX\\Tracker\\Vendor\\Laravel\\Models\\Cookie',
    'domain_model' => 'PragmaRX\\Tracker\\Vendor\\Laravel\\Models\\Domain',
    'referer_model' => 'PragmaRX\\Tracker\\Vendor\\Laravel\\Models\\Referer',
    'referer_search_term_model' => 'PragmaRX\\Tracker\\Vendor\\Laravel\\Models\\RefererSearchTerm',
    'route_model' => 'PragmaRX\\Tracker\\Vendor\\Laravel\\Models\\Route',
    'route_path_model' => 'PragmaRX\\Tracker\\Vendor\\Laravel\\Models\\RoutePath',
    'route_path_parameter_model' => 'PragmaRX\\Tracker\\Vendor\\Laravel\\Models\\RoutePathParameter',
    'error_model' => 'PragmaRX\\Tracker\\Vendor\\Laravel\\Models\\Error',
    'geoip_model' => 'PragmaRX\\Tracker\\Vendor\\Laravel\\Models\\GeoIp',
    'sql_query_model' => 'PragmaRX\\Tracker\\Vendor\\Laravel\\Models\\SqlQuery',
    'sql_query_binding_model' => 'PragmaRX\\Tracker\\Vendor\\Laravel\\Models\\SqlQueryBinding',
    'sql_query_binding_parameter_model' => 'PragmaRX\\Tracker\\Vendor\\Laravel\\Models\\SqlQueryBindingParameter',
    'sql_query_log_model' => 'PragmaRX\\Tracker\\Vendor\\Laravel\\Models\\SqlQueryLog',
    'connection_model' => 'PragmaRX\\Tracker\\Vendor\\Laravel\\Models\\Connection',
    'event_model' => 'PragmaRX\\Tracker\\Vendor\\Laravel\\Models\\Event',
    'event_log_model' => 'PragmaRX\\Tracker\\Vendor\\Laravel\\Models\\EventLog',
    'system_class_model' => 'PragmaRX\\Tracker\\Vendor\\Laravel\\Models\\SystemClass',
    'language_model' => 'PragmaRX\\Tracker\\Vendor\\Laravel\\Models\\Language',
    'authentication_ioc_binding' => 
    array (
      0 => 'auth',
    ),
    'authentication_guards' => 
    array (
    ),
    'authenticated_check_method' => 'check',
    'authenticated_user_method' => 'user',
    'authenticated_user_id_column' => 'id',
    'authenticated_user_username_column' => 'email',
    'stats_panel_enabled' => true,
    'stats_routes_before_filter' => '',
    'stats_routes_after_filter' => '',
    'stats_routes_middleware' => 'web',
    'stats_template_path' => '/templates/sb-admin-2',
    'stats_base_uri' => 'stats',
    'stats_layout' => 'pragmarx/tracker::layout',
    'stats_controllers_namespace' => 'PragmaRX\\Tracker\\Vendor\\Laravel\\Controllers',
    'default_user_agent' => '',
  ),
  'view' => 
  array (
    'paths' => 
    array (
      0 => '/Users/sicepat/Sites/adpoint-new/resources/views',
    ),
    'compiled' => '/Users/sicepat/Sites/adpoint-new/storage/framework/views',
  ),
  'webpush' => 
  array (
    'vapid' => 
    array (
      'subject' => NULL,
      'public_key' => 'BJ8UEGdJOmmGa6SvxZyMJSkv_xaeRJ9mjG58CZU-gu3gZCu47AIAWLpgmTsVrGzjBKX655plYWTDP8FnYKUAtyk',
      'private_key' => '5mrOzz1-JZuZRRzGOLCe6yLvpIhr_HWZLUVK9WNKVLA',
      'pem_file' => NULL,
    ),
    'model' => 'NotificationChannels\\WebPush\\PushSubscription',
    'table_name' => 'push_subscriptions',
    'database_connection' => 'mysql',
    'client_options' => 
    array (
    ),
    'gcm' => 
    array (
      'key' => NULL,
      'sender_id' => NULL,
    ),
  ),
  'cors' => 
  array (
    'paths' => 
    array (
    ),
    'allowed_methods' => 
    array (
      0 => '*',
    ),
    'allowed_origins' => 
    array (
      0 => '*',
    ),
    'allowed_origins_patterns' => 
    array (
    ),
    'allowed_headers' => 
    array (
      0 => '*',
    ),
    'exposed_headers' => 
    array (
    ),
    'max_age' => 0,
    'supports_credentials' => false,
  ),
  'debug-server' => 
  array (
    'host' => 'tcp://127.0.0.1:9912',
  ),
  'excel' => 
  array (
    'exports' => 
    array (
      'chunk_size' => 1000,
      'pre_calculate_formulas' => false,
      'strict_null_comparison' => false,
      'csv' => 
      array (
        'delimiter' => ',',
        'enclosure' => '"',
        'line_ending' => '
',
        'use_bom' => false,
        'include_separator_line' => false,
        'excel_compatibility' => false,
        'output_encoding' => '',
      ),
      'properties' => 
      array (
        'creator' => '',
        'lastModifiedBy' => '',
        'title' => '',
        'description' => '',
        'subject' => '',
        'keywords' => '',
        'category' => '',
        'manager' => '',
        'company' => '',
      ),
    ),
    'imports' => 
    array (
      'read_only' => true,
      'ignore_empty' => false,
      'heading_row' => 
      array (
        'formatter' => 'slug',
      ),
      'csv' => 
      array (
        'delimiter' => NULL,
        'enclosure' => '"',
        'escape_character' => '\\',
        'contiguous' => false,
        'input_encoding' => 'UTF-8',
      ),
      'properties' => 
      array (
        'creator' => '',
        'lastModifiedBy' => '',
        'title' => '',
        'description' => '',
        'subject' => '',
        'keywords' => '',
        'category' => '',
        'manager' => '',
        'company' => '',
      ),
    ),
    'extension_detector' => 
    array (
      'xlsx' => 'Xlsx',
      'xlsm' => 'Xlsx',
      'xltx' => 'Xlsx',
      'xltm' => 'Xlsx',
      'xls' => 'Xls',
      'xlt' => 'Xls',
      'ods' => 'Ods',
      'ots' => 'Ods',
      'slk' => 'Slk',
      'xml' => 'Xml',
      'gnumeric' => 'Gnumeric',
      'htm' => 'Html',
      'html' => 'Html',
      'csv' => 'Csv',
      'tsv' => 'Csv',
      'pdf' => 'Dompdf',
    ),
    'value_binder' => 
    array (
      'default' => 'Maatwebsite\\Excel\\DefaultValueBinder',
    ),
    'cache' => 
    array (
      'driver' => 'memory',
      'batch' => 
      array (
        'memory_limit' => 60000,
      ),
      'illuminate' => 
      array (
        'store' => NULL,
      ),
    ),
    'transactions' => 
    array (
      'handler' => 'db',
      'db' => 
      array (
        'connection' => NULL,
      ),
    ),
    'temporary_files' => 
    array (
      'local_path' => '/Users/sicepat/Sites/adpoint-new/storage/framework/cache/laravel-excel',
      'remote_disk' => NULL,
      'remote_prefix' => NULL,
      'force_resync_remote' => NULL,
    ),
  ),
  'image-optimizer' => 
  array (
    'optimizers' => 
    array (
      'Spatie\\ImageOptimizer\\Optimizers\\Jpegoptim' => 
      array (
        0 => '-m85',
        1 => '--strip-all',
        2 => '--all-progressive',
      ),
      'Spatie\\ImageOptimizer\\Optimizers\\Pngquant' => 
      array (
        0 => '--force',
      ),
      'Spatie\\ImageOptimizer\\Optimizers\\Optipng' => 
      array (
        0 => '-i0',
        1 => '-o2',
        2 => '-quiet',
      ),
      'Spatie\\ImageOptimizer\\Optimizers\\Svgo' => 
      array (
        0 => '--disable=cleanupIDs',
      ),
      'Spatie\\ImageOptimizer\\Optimizers\\Gifsicle' => 
      array (
        0 => '-b',
        1 => '-O3',
      ),
      'Spatie\\ImageOptimizer\\Optimizers\\Cwebp' => 
      array (
        0 => '-m 6',
        1 => '-pass 10',
        2 => '-mt',
        3 => '-q 90',
      ),
    ),
    'binary_path' => '',
    'timeout' => 60,
    'log_optimizer_activity' => false,
  ),
  'datatables-buttons' => 
  array (
    'namespace' => 
    array (
      'base' => 'DataTables',
      'model' => '',
    ),
    'pdf_generator' => 'snappy',
    'snappy' => 
    array (
      'options' => 
      array (
        'no-outline' => true,
        'margin-left' => '0',
        'margin-right' => '0',
        'margin-top' => '10mm',
        'margin-bottom' => '10mm',
      ),
      'orientation' => 'landscape',
    ),
    'parameters' => 
    array (
      'dom' => 'Bfrtip',
      'order' => 
      array (
        0 => 
        array (
          0 => 0,
          1 => 'desc',
        ),
      ),
      'buttons' => 
      array (
        0 => 'create',
        1 => 'export',
        2 => 'print',
        3 => 'reset',
        4 => 'reload',
      ),
    ),
    'generator' => 
    array (
      'columns' => 'id,add your columns,created_at,updated_at',
      'buttons' => 'create,export,print,reset,reload',
      'dom' => 'Bfrtip',
    ),
  ),
  'datatables-html' => 
  array (
    'namespace' => 'LaravelDataTables',
    'table' => 
    array (
      'class' => 'table',
      'id' => 'dataTableBuilder',
    ),
    'callback' => 
    array (
      0 => '$',
      1 => '$.',
      2 => 'function',
    ),
    'script' => 'datatables::script',
    'editor' => 'datatables::editor',
  ),
  'trustedproxy' => 
  array (
    'proxies' => NULL,
    'headers' => 94,
  ),
  'datatables::search' => 
  array (
    'case_insensitive' => true,
    'use_wildcards' => false,
  ),
  'datatables::dataFullSupport' => false,
  'datatables-fractal' => 
  array (
    'includes' => 'include',
    'serializer' => 'League\\Fractal\\Serializer\\DataArraySerializer',
  ),
  'tinker' => 
  array (
    'commands' => 
    array (
    ),
    'alias' => 
    array (
    ),
    'dont_alias' => 
    array (
      0 => 'App\\Nova',
    ),
  ),
);
