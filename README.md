# Adpoint V2.0
Adpoint V2.0 built with Laravel 8.0 and PHP 8.0 with Mysql Databases. 

## Requirements :
![PHP](https://img.shields.io/badge/php-%23777BB4.svg?style=for-the-badge&logo=php&logoColor=white)
![MySQL](https://img.shields.io/badge/mysql-%2300f.svg?style=for-the-badge&logo=mysql&logoColor=white)
![Laravel](https://img.shields.io/badge/laravel-%23FF2D20.svg?style=for-the-badge&logo=laravel&logoColor=white)
1. PHP 8.0
2. Mysql >5
3. Composer

## Installation :
1. Clone repo 
```javascript
git clone https://gitlab.com/danudenny/adpoint-new.git
```

2. Install Composer
```sh
composer install

or

composer install --ignore-platform-reqs
```

3. Copy `.env.example` to `.env`
4. Run migration `php artisan migrate --seed`
5. Run
```javascript
php artisan serve
```

6. Access 
```javascript
http://localhost:8000/
```

## Telescope
Service to monitoring laravel application like Mail, Queue, Jobs, Query, Logging, etc. Laravel telescope documentation can be found [here](https://laravel.com/docs/9.x/telescope).

Access `http://localhost:8000/telescope`

## Payment Gateway
This app using two method of Payment. 
1. Manual Transfer Payment with manual confirmation and upload proof.
2. [Xendit](https://xendit.co/) payment gateway with automatic confirmation after successfully paid using Virtual Account.

## Administrator 
Contact administrator when needed some configurations like Server (SSH Access), Database Access, Mail Credentials, etc.
