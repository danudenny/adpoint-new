@extends('layouts.app')

@section('content')

    <div class="col-sm-12">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">{{__('Update testimoni')}}</h3>
            </div>
            <!--Horizontal Form-->
            <!--===================================================-->
            <form class="form-horizontal" action="{{ route('testimonials.update', ['id' => $data->id]) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PATCH')
                <div class="panel-body">

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="name">{{__('Name')}}</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="name" id="name" value="{{ $data->name }}" required placeholder="{{__('Name')}}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">{{__('Avatar')}}</label>
                        <div class="col-sm-10">
                            <img src="{{ asset($data->avatar) }}" alt="{{ $data->avatar }}" class="img-md">
                            <input type="file" class="form-control" name="avatar" accept="image/jpeg, image/jpg, image/png">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="office">{{__('Office')}}</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" value="{{ $data->office }}" name="office" id="office" required placeholder="{{__('Office')}}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="position">{{__('Position')}}</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" value="{{ $data->position }}" name="position" id="position" required placeholder="{{__('Position')}}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">{{__('Testimoni')}}</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" name="testimoni" cols="30" rows="3" required>{{ $data->testimoni }}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="rating" class="col-sm-2 control-label">{{__('Rating')}}</label>
                        <div class="col-sm-10">
                                <div class='rating-stars'>
                                    <ul id='stars'>
                                        <li class='star {{ $data->rating >=1 ? 'selected' : ''}}' title='Poor' data-value='1'>
                                            <i class='fa fa-star fa-fw'></i>
                                        </li>
                                        <li class='star {{ $data->rating >=2 ? 'selected' : ''}}' title='Fair' data-value='2'>
                                            <i class='fa fa-star fa-fw'></i>
                                        </li>
                                        <li class='star {{ $data->rating >=3 ? 'selected' : ''}}' title='Good' data-value='3'>
                                            <i class='fa fa-star fa-fw'></i>
                                        </li>
                                        <li class='star {{ $data->rating >=4 ? 'selected' : ''}}' title='Excellent' data-value='4'>
                                            <i class='fa fa-star fa-fw'></i>
                                        </li>
                                        <li class='star {{ $data->rating >=5 ? 'selected' : ''}}' title='WOW!!!' data-value='5'>
                                            <i class='fa fa-star fa-fw'></i>
                                        </li>
                                    </ul>
                                </div>
                            <input type="hidden" name="rating" id="rating" value="{{ $data->rating }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="via">{{__('Via')}}</label>
                        <div class="col-sm-10">
                            <select name="via" id="via" class="form-control">
                                <option value="0" {{ $data->via == 0 ? 'selected' : ''}}>Twitter</option>
                                <option value="1" {{ $data->via == 1 ? 'selected' : ''}}>Facebook</option>
                                <option value="2" {{ $data->via == 2 ? 'selected' : ''}}>Instagram</option>
                                <option value="3" {{ $data->via == 3 ? 'selected' : ''}}>Others</option>
                            </select>
                        </div>
                    </div>

                </div>
                <div class="panel-footer text-right">
                    <button class="btn btn-purple" type="submit">{{__('Update')}}</button>
                </div>
            </form>

            <!--===================================================-->
            <!--End Horizontal Form-->

        </div>
    </div>

@endsection

@section('script')
    <script>
        $(document).ready(function(){
            $('#msg').hide();
            /* 1. Visualizing things on Hover - See next part for action on click */
            $('#stars li').on('mouseover', function(){
                var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on

                // Now highlight all the stars that's not after the current hovered star
                $(this).parent().children('li.star').each(function(e){
                if (e < onStar) {
                    $(this).addClass('hover');
                }
                else {
                    $(this).removeClass('hover');
                }
                });

            }).on('mouseout', function(){
                $(this).parent().children('li.star').each(function(e){
                $(this).removeClass('hover');
                });
            });


            /* 2. Action to perform on click */
            $('#stars li').on('click', function(){
                var onStar = parseInt($(this).data('value'), 10); // The star currently selected
                var stars = $(this).parent().children('li.star');

                for (i = 0; i < stars.length; i++) {
                $(stars[i]).removeClass('selected');
                }

                for (i = 0; i < onStar; i++) {
                $(stars[i]).addClass('selected');
                }

                // JUST RESPONSE (Not needed)
                var ratingValue = parseInt($('#stars li.selected').last().data('value'), 10);
                $('#rating').val(ratingValue);
            });


        });

    </script>
@endsection
