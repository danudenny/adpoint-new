@extends('layouts.app')

@section('content')

    <div class="col-sm-12">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">{{__('Add new testimoni')}}</h3>
            </div>
            <!--Horizontal Form-->
            <!--===================================================-->
            <form class="form-horizontal" action="{{ route('testimonials.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="panel-body">

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="name">{{__('Name')}}</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="name" id="name" required placeholder="{{__('Name')}}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">{{__('Avatar')}}</label>
                        <div class="col-sm-10">
                            <input type="file" class="form-control" name="avatar" required accept="image/jpeg, image/jpg, image/png">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="office">{{__('Office')}}</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="office" id="office" required placeholder="{{__('Office')}}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="position">{{__('Position')}}</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="position" id="position" required placeholder="{{__('Position')}}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">{{__('Testimoni')}}</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" name="testimoni" cols="30" rows="3" required></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="rating" class="col-sm-2 control-label">{{__('Rating')}}</label>
                        <div class="col-sm-10">
                                <div class='rating-stars'>
                                    <ul id='stars'>
                                        <li class='star' title='Poor' data-value='1'>
                                            <i class='fa fa-star fa-fw'></i>
                                        </li>
                                        <li class='star' title='Fair' data-value='2'>
                                            <i class='fa fa-star fa-fw'></i>
                                        </li>
                                        <li class='star' title='Good' data-value='3'>
                                            <i class='fa fa-star fa-fw'></i>
                                        </li>
                                        <li class='star' title='Excellent' data-value='4'>
                                            <i class='fa fa-star fa-fw'></i>
                                        </li>
                                        <li class='star' title='WOW!!!' data-value='5'>
                                            <i class='fa fa-star fa-fw'></i>
                                        </li>
                                    </ul>
                                </div>
                            <input type="hidden" name="rating" id="rating" value="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="via">{{__('Via')}}</label>
                        <div class="col-sm-10">
                            <select name="via" id="via" class="form-control">
                                <option value="0">Twitter</option>
                                <option value="1">Facebook</option>
                                <option value="2">Instagram</option>
                                <option value="3">Others</option>
                            </select>
                        </div>
                    </div>

                </div>
                <div class="panel-footer text-right">
                    <button class="btn btn-purple" type="submit">{{__('Save')}}</button>
                </div>
            </form>

            <!--===================================================-->
            <!--End Horizontal Form-->

        </div>
    </div>

@endsection

@section('script')
    <script>
        $(document).ready(function(){
            $('#msg').hide();
            /* 1. Visualizing things on Hover - See next part for action on click */
            $('#stars li').on('mouseover', function(){
                var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on

                // Now highlight all the stars that's not after the current hovered star
                $(this).parent().children('li.star').each(function(e){
                if (e < onStar) {
                    $(this).addClass('hover');
                }
                else {
                    $(this).removeClass('hover');
                }
                });

            }).on('mouseout', function(){
                $(this).parent().children('li.star').each(function(e){
                $(this).removeClass('hover');
                });
            });


            /* 2. Action to perform on click */
            $('#stars li').on('click', function(){
                var onStar = parseInt($(this).data('value'), 10); // The star currently selected
                var stars = $(this).parent().children('li.star');

                for (i = 0; i < stars.length; i++) {
                $(stars[i]).removeClass('selected');
                }

                for (i = 0; i < onStar; i++) {
                $(stars[i]).addClass('selected');
                }

                // JUST RESPONSE (Not needed)
                var ratingValue = parseInt($('#stars li.selected').last().data('value'), 10);
                $('#rating').val(ratingValue);
            });


        });

    </script>
@endsection
