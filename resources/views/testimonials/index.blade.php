@extends('layouts.app')

@section('content')
    <div style="margin-bottom: 10px; display: flex; flex-direction: row; justify-content: end;">
        <a class="btn btn-success" href="{{ route('testimonials.create') }}"><i class="fa fa-plus"></i> ADD NEW TESTIMONI</a>
    </div>
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">{{__('Testimonials')}}</h3>
        </div>
        <div class="panel-body">
            <table class="table table-striped table-bordered demo-dt-basic" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>{{ __('No') }}</th>
                    <th>{{ __('Avatar') }}</th>
                    <th>{{ __('Name') }}</th>
                    <th>{{ __('Office') }}</th>
                    <th>{{ __('Position') }}</th>
                    <th>{{ __('Testimoni') }}</th>
                    <th>{{ __('Rating') }}</th>
                    <th>{{ __('Via') }}</th>
                    <th>{{ __('Activated') }}</th>
                    <th>{{ __('Options') }}</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($data as $key => $testi)
                    <tr>
                        <td>{{ ++$key }}</td>
                        <td><img class="img-md" src="{{ asset($testi->avatar) }}" alt="{{__('image')}}"></td>
                        <td>{{ $testi->name }}</td>
                        <td>{{ $testi->office }}</td>
                        <td>{{ $testi->position }}</td>
                        <td>{{ $testi->testimoni }}</td>
                        <td>
                            <div class="rating text-right clearfix d-block">
                                <span class="star-rating star-rating-sm float-right">
                                    @for ($i=0; $i < $testi->rating; $i++)
                                        <i class="fa fa-star active"></i>
                                    @endfor
                                    @for ($i=0; $i < 5-$testi->rating; $i++)
                                        <i class="fa fa-star"></i>
                                    @endfor
                                </span>
                            </div>
                        </td>
                        <td>{{ $testi->via == 0 ? 'Twitter' : ($testi->via == 1 ? 'Facebook' : ($testi->via == 2 ? 'Instagram' : 'Others')) }}</td>
                        <td>
                            <label class="switch">
                                <input onchange="update_post_activated(this)" value="{{ $testi->id }}" type="checkbox"
                                    {{ $testi->status == 1 ? 'checked' : '' }}>
                                <span class="slider round"></span></label>
                        </td>
                        <td>
                            <div class="btn-group dropdown">
                                <button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                    {{__('Actions')}} <i class="dropdown-caret"></i>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="{{ route('testimonials.edit', encrypt($testi->id)) }}">{{__('Edit')}}</a></li>
                                    <li><a onclick="confirm_modal('{{ route('testimonials.destroy', $testi->id) }}');">{{__('Delete')}}</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        function update_post_activated(el){
            if(el.checked){
                var status = 1;
            }
            else{
                var status = 0;
            }
            $.post('{{ route('testimonials.activated') }}', {_token:'{{ csrf_token() }}', id:el.value, status:status }, function(data){
                if(data == 1){
                    location.reload();
                }else{
                    showAlert('danger', 'Something went wrong');
                }
            });
        }
    </script>
@endsection
