@extends('layouts.app')

@section('content')
    <div style="margin-bottom: 10px; display: flex; flex-direction: row; justify-content: end;">
        <a class="btn btn-success" href="javascript:void(0)" id="newBenefit"><i class="fa fa-plus"></i> Create New Benefit</a>
    </div>
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">{{__($title)}}</h3>
        </div>
        <div class="panel-body">
            <table class="table table-striped table-bordered demo-dt-basic" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>{{ __('No') }}</th>
                    <th>{{ __('Image') }}</th>
                    <th>{{ __('Descriptions') }}</th>
                    <th>{{ __('Options') }}</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($data as $key => $benefit)
                    <tr>
                        <td>{{ ++$key }}</td>
                        <td><img class="img-md" src="{{ asset($benefit->image) }}" alt="{{__('image')}}"></td>
                        <td>{{ $benefit->description }}</td>
                        <td>
                            <div class="btn-group dropdown">
                                <button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                    {{__('Actions')}} <i class="dropdown-caret"></i>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a id="updateBenefit" data-id="{{ $benefit->id }}" data-image="{{ $benefit->image }}" data-description="{{ $benefit->description }}">{{__('Edit')}}</a></li>
                                    <li><a onclick="confirm_modal('{{ route('benefits.destroy', $benefit->id) }}');">{{__('Delete')}}</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
       @include('benefits.create')
       @include('benefits.update')
    </div>

    @yield('scripts')
    <script>
        $("body").on("click","#newBenefit",function(e){
            e.preventDefault;
            $('#newBenefitModal').html("Add New "+ "{{ $title }}");
            $('#submit').val("Save Data");
            $('#modal-id').modal('show', {backdrop: 'static', keyboard: false});
            $('#formStore').trigger("reset");
        });

        $("body").on("click","#updateBenefit",function(e){
            e.preventDefault;
            let id = $(this).attr('data-id');
            let image = $(this).attr('data-image');
            let description = $(this).attr('data-description');

            $('#updateBenefitModal').html("Update "+ "{{ $title }}");
            $('#submit-update').val("Update Data");
            $('#modal-update-id').modal('show', {backdrop: 'static', keyboard: false});
            $('#formUpadte').trigger("reset");

            $("#id_update").val(id);
            $("#image_update").attr("src", "/"+image);
            $("#description_update").val(description);
        });
    </script>
@endsection
