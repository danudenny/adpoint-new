<div class="modal fade" id="modal-update-id">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title" id="updateBenefitModal"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>

            <div class="modal-body">
                <form id="formUpadte" method="POST" action="{{ route('benefits.update') }}" enctype="multipart/form-data">
                    @csrf
                    @method('PATCH')
                    <div class="form-group">
                        <p>Image</p>
                        <input type="hidden" name="type" value="{{ \request()->get('type') == 'seller' ? 0 : 1 }}">
                        <input type="hidden" name="id" id="id_update">
                        <img id="image_update" class="img-md">
                        <input type="file" class="form-control" name="image" placeholder="Image" accept=".png">
                        <span class="text-danger"><b>{{ $errors->first('title') }}</b></span>
                    </div>
                    <div class="form-group">
                        <p>Description</p>
                        <textarea name="description" rows="5" class="form-control" id="description_update"></textarea>
                        <span class="text-danger"><b>{{ $errors->first('title') }}</b></span>
                    </div>
                    <div style="display: flex; flex-direction: row; column-gap: 10px; justify-content: end">
                        <input type="submit" value="Update Benefit" id="submit-update" class="btn btn-success">
                        <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">
                            <i class="fa fa-times"></i>
                            Cancel
                        </button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
