@extends('layouts.app')

@section('content')

    <div class="col-sm-12">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">{{__('Add new popular city')}}</h3>
            </div>
            <!--Horizontal Form-->
            <!--===================================================-->
            <form class="form-horizontal" action="{{ route('popular-city.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="panel-body">

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="title">{{__('City')}}</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="city" id="title" required placeholder="{{__('City')}}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">{{__('Descriptions')}}</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" name="description" cols="30" rows="3" required></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">{{__('Image')}}</label>
                        <div class="col-sm-10">
                            <input type="file" class="form-control" name="image" required accept="image/gif, image/jpeg, image/jpg, image/png">
                        </div>
                    </div>

                </div>
                <div class="panel-footer text-right">
                    <button class="btn btn-purple" type="submit">{{__('Save')}}</button>
                </div>
            </form>

            <!--===================================================-->
            <!--End Horizontal Form-->

        </div>
    </div>

@endsection
