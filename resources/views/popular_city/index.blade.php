@extends('layouts.app')

@section('content')
    <div style="margin-bottom: 10px; display: flex; flex-direction: row; justify-content: end;">
        <a class="btn btn-success" href="{{ route('popular-city.create') }}"><i class="fa fa-plus"></i> ADD NEW POPULAR CITY</a>
    </div>
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">{{__('Popular City Setting')}}</h3>
        </div>
        <div class="panel-body">
            <table class="table table-striped table-bordered demo-dt-basic" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>{{ __('No') }}</th>
                    <th>{{ __('City') }}</th>
                    <th>{{ __('Image') }}</th>
                    <th>{{ __('Descriptions') }}</th>
                    <th>{{ __('Activated') }}</th>
                    <th>{{ __('Options') }}</th>
                </tr>
                </thead>
                <tbody>
                @php $no = 0; @endphp
                @foreach ($data as $key => $city)
                    <tr>
                        <td>{{ ++$no }}</td>
                        <td>{{ $city->city }}</td>
                        <td><img class="img-md" src="{{ asset($city->image) }}" alt="{{__('image')}}"></td>
                        <td>{{ $city->description }}</td>
                        <td>
                            <label class="switch">
                                <input onchange="update_popular_city_activated(this)" value="{{ $city->id }}" type="checkbox" {{ $city->is_active == 1 ? 'checked' : '' }}>
                                <span class="slider round"></span></label>
                        </td>
                        <td>
                            <div class="btn-group dropdown">
                                <button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                    {{__('Actions')}} <i class="dropdown-caret"></i>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="{{ route('popular-city.edit', encrypt($city->id)) }}">{{__('Edit')}}</a></li>
                                    <li><a onclick="confirm_modal('{{ route('popular-city.destroy', $city->id) }}');">{{__('Delete')}}</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        function update_popular_city_activated(el){
            if(el.checked){
                var is_active = 1;
            }
            else{
                var is_active = 0;
            }
            $.post('{{ route('popular-city.activated') }}', {_token:'{{ csrf_token() }}', id:el.value, is_active:is_active }, function(data){
                if(data == 1) {
                    location.reload();
                } else {
                    showAlert('danger', 'Something went wrong');
                }
            });
        }
    </script>
@endsection
