@extends('layouts.app')

@section('content')

<!-- Basic Data Tables -->
<!--===================================================-->
<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">{{__('Transaction Detail')}}</h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-4">
                <table class="table">
                    <tr>
                        <th>Transaction Number</th>
                        <th> : {{ $transaction->code }}</th>
                    </tr>
                    <tr>
                        <th>Order Number</th>
                        <th> : {{ $transaction->order_code }}</th>
                    </tr>
                    <tr>
                        <th>Payment Type</th>
                        <th> : Transfer Bank {{ $transaction->payment_type }}</th>
                    </tr>
                    @if($transaction->status === 'confirmed payment' || $transaction->status === 'paid')
                    @php
                    $getBuktitrf = App\ConfirmPayment::select('id', 'bukti')->where('code_trx',
                    $transaction->code)->first();
                    @endphp
                    @if ($getBuktitrf)
                    <tr>
                        <th>Bukti Transfer</th>
                        <th> : <a href="{{ asset($getBuktitrf->bukti) }}" class="btn btn-sm btn-primary" download>
                                <i class="fa fa-download"></i> Download Image</a>
                        </th>
                    </tr>
                    @endif
                    @endif
                </table>
            </div>
            <div class="col-md-8">
                <div class="row pull-right">
                    @if ($transaction->status === "on proses")
                    <button class="btn btn-success" onclick="approve_by_admin({{ $transaction->id }})">
                        <i class="fa fa-check"></i> Approve
                    </button>
                    <button class="btn btn-danger" onclick="disapprove_by_admin({{$transaction->id}})">
                        <i class="fa fa-times"></i> Reject
                    </button>
                    @elseif ($transaction->status === "ready")
                    @php
                    $true = [];
                    $false = [];
                    $od_rejected = [];
                    @endphp
                    @foreach (\App\Transaction::where('id', $transaction->id)->get() as $key => $t)
                    @foreach ($t->orders as $key => $o)
                    @foreach ($o->orderDetails as $key => $od)
                    @php
                    array_push($false, $od->is_confirm);
                    array_push($od_rejected, $od->rejected);
                    @endphp
                    @if ($od->is_confirm === 1)
                    @php
                    array_push($true, $od->is_confirm);
                    @endphp
                    @endif
                    @endforeach
                    @endforeach
                    @endforeach

                    @if (count($od_rejected) > 0 && $od->is_reject == 0)
                    <button id="btn-cal" onclick="confirmToBuyerToCalculate({{$transaction->id}})"
                        class="btn btn-warning _btn_">
                        <i class="fa fa-calculator"></i> Recalculate</button>
                    @endif
                    @if (count($true) === count($false))
                    <button onclick="confirmToBuyer({{$transaction->id}})" class="btn btn-primary">
                        <i class="fa fa-check"></i> Confirm to Buyer</button>
                    @endif
                    @elseif ($transaction->status === "paid")
                    <a class="btn btn-primary"
                        href="{{ route('transaction.show.invoice', encrypt($transaction->id)) }}">
                        <i class="fa fa-money"></i> Invoice
                    </a>
                    <a class="btn btn-success" href="{{ route('transaction.show.payment', $transaction->code) }}">
                        <i class="fa fa-money"></i> View
                    </a>
                    @endif
                    @if ($transaction->status === 'confirmed payment')
                    <a class="btn btn-success" href="{{ route('transaction.change.to.paid', $transaction->code) }}">
                        <i class="fa fa-money"></i> Mark as paid
                    </a>
                    @endif
                </div>
            </div>
        </div>
        <div class="modal fade" id="approve{{$transaction->id}}" tabindex="-1" role="dialog"
            aria-labelledby="disapproveTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" style="width: 300px" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">#Approval </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="text-center">
                            <h3>Are you sure to approve ?</h3>
                            <strong># {{ $transaction->code }}</strong>
                            <div style="margin-top: 30px">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <a href="{{ route('approve.by.admin', encrypt($transaction->id)) }}"
                                    class="btn btn-primary">Yes, approve</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="disapprove{{$transaction->id}}" tabindex="-1" role="dialog"
            aria-labelledby="disapproveTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Reject TRX: {{ $transaction->code }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="{{ route('disapprove.by.admin') }}" method="POST">
                        @csrf
                        <div class="modal-body">
                            <div class="form-group">
                                <input type="hidden" name="trx_id" value="{{$transaction->id}}">
                                <label>Alasan</label>
                                <textarea name="alasan" class="form-control" placeholder="Tuliskan alasan" cols="20"
                                    rows="5"></textarea>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                @foreach ($transaction->orders as $key => $o)
                                @php
                                $seller = \App\User::where('id', $o->seller_id)->first();
                                @endphp
                                <tr>
                                    <th colspan="6">
                                        Media Owner: <span class="badge badge-danger"># {{ $seller->name }}</span>
                                    </th>
                                </tr>
                                <tr>
                                    <th style="text-align: center">No.</th>
                                    <th style="text-align: center">Description</th>
                                    <th style="text-align: center">Payment Status</th>
                                    <th style="text-align: center">File Advertising</th>
                                    <th style="text-align: center">Price</th>
                                    <th style="text-align: center">Tax 10%</th>
                                    <th style="text-align: center">Total</th>
                                    <th style="text-align: center"></th>
                                </tr>
                                @foreach ($o->orderDetails as $key => $od)
                                @php
                                $index = 1;
                                $product = \App\Product::where('id', $od->product_id)->first();
                                @endphp

                                @if ($od->status == 2)
                                <tr style="text-decoration: line-through">

                                    <td>{{ $index++ }}</td>
                                    <td>
                                        <i class="fa fa-fw fa-times text-danger"></i>

                                        <a target="_blank" href="{{ route('product', $product->slug) }}">
                                            <strong>{{ $product->name }}</strong> ( <strong>{{ $od->quantity }}</strong>
                                            <i><strong>{{ $od->variation }}</strong></i>)
                                        </a>
                                    </td>
                                    <td>
                                        @php
                                        $file = json_decode($od->file_advertising);
                                        @endphp
                                        @if ($file !== null)
                                        @if ($file->gambar !== null)
                                        @foreach ($file->gambar as $key => $g)
                                        <b><a href="{{ url($g) }}" download>Gambar {{ $key+1 }}</a></b> <br>
                                        @endforeach
                                        @endif
                                        @if ($file->video !== null)
                                        @foreach ($file->video as $key => $v)
                                        <b><a href="{{ url($v) }}" download>Video {{ $key+1 }}</a></b> <br>
                                        @endforeach
                                        @endif
                                        @if ($file->link !== null)
                                        @foreach ($file->link as $key => $l)
                                        <b><a href="{{ $l }}" target="_blank">Link {{ $key+1 }}</a></b> <br>
                                        @endforeach
                                        @endif
                                        @endif
                                    </td>
                                    <td>
                                        <strong>{{ single_price($od->price) }}</strong>
                                    </td>
                                    <td align="right">
                                        <strong>{{ single_price($od->grand_total) }}</strong>
                                    </td>
                                    <td>
                                        @if ($od->is_confirm == 0)
                                        <button data-toggle="tooltip" title="Details"
                                            onclick="rejectDetail({{ $od->id }})" class="btn btn-sm btn-primary">
                                            <i class="fa fa-eye"></i> Details
                                        </button>
                                        @endif
                                    </td>
                                </tr>
                                @else
                                <tr>
                                    <td>{{ $index++ }}</td>
                                    <td>
                                        <i class="fa fa-fw fa-check text-success"></i>
                                        <a target="_blank" href="{{ route('product', $product->slug) }}">
                                            <strong>{{ $product->name }}</strong> ( <strong>{{ $od->quantity }}</strong>
                                            <i><strong>{{ $od->variation }}</strong></i>)
                                        </a>
                                    </td>
                                    <td style="text-align: center">
                                        @if ($od->is_paid === 1)
                                        <span class="label label-success"><i class="fa fa-check"></i>
                                            PAID</span>
                                        @elseif ($od->is_paid === 0)
                                        <span class="label label-warning"><i class="fa fa-times"></i> UNPAID</span>
                                        @endif
                                    </td>
                                    <td>
                                        @php
                                        $file = json_decode($od->file_advertising);
                                        @endphp
                                        @if ($file !== null)
                                        @if ($file->gambar !== null)
                                        @foreach ($file->gambar as $key => $g)
                                        <b><a href="{{ url($g) }}" download><i class="fa fa-download"></i> Gambar {{
                                                $key+1 }}</a></b> <br>
                                        @endforeach
                                        @endif
                                        @if ($file->video !== null)
                                        @foreach ($file->video as $key => $v)
                                        <b><a href="{{ url($v) }}" download>Video {{ $key+1 }}</a></b> <br>
                                        @endforeach
                                        @endif
                                        @endif
                                    </td>
                                    <td align="right">
                                        <strong>{{ single_price($od->price) }}</strong>
                                    </td>
                                    <td align="right">
                                        <strong>{{ single_price($od->price * 0.1) }}</strong>
                                    </td>
                                    <td align="right">
                                        <strong>{{ single_price($od->price + ($od->price * 0.1)) }}</strong>
                                    </td>
                                    <td></td>
                                </tr>

                                @endif
                                @endforeach
                                <tr>
                                    <td colspan="6" align="right"><strong>GRAND TOTAL</strong></td>
                                    <td align="right"> <strong>{{ single_price($o->grand_total) }}</strong></td>
                                </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="confirmToBuyer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div id="confirm-modal-body">

        </div>
    </div>
</div>

<div class="modal fade" id="rejectDetail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-zoom product-modal" id="modal-size" role="document">
        <div class="modal-content position-relative">
            <div id="rejectDetailbody">

            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script>
    function disapprove_by_admin(id) {
            $('#disapprove'+id).modal('show');
        }

        function approve_by_admin(id) {
            $('#approve'+id).modal('show');
        }

        function confirmToBuyer(id) {
            $('#confirm-modal-body').html(null);
            $.post('{{ route('confirm.to.buyer') }}', { _token : '{{ @csrf_token() }}', trx_id : id}, function(data){
                $('#confirm-modal-body').html(data);
                $('#confirmToBuyer').modal();
            });
        }

        function rejectDetail(id) {
            $('#rejectDetailbody').html(null);
            $('#rejectDetail').modal();
            $.post('{{ route('reject.detail') }}', {_token:'{{ csrf_token() }}', order_detail_id:id}, function(data){
                $('#rejectDetailbody').html(data);
            });
        }

        function confirmToBuyerToCalculate(id){
            $('#btn-cal').html('<i class="fa fa-spin fa-spinner"></i>');
            let url = '{{ url("/admin/confirm-reject-to-buyer") }}'+"/"+id;
            location.replace(url);
        }

        
</script>
@endsection