@extends('frontend.layouts.app')

@section('header')
<style>
    .owl-carousel .item {
        background: #4DC7A0;
        margin: 10px;
    }
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" />
<link rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.green.min.css" />
@endsection

@section('content')
<style>
    .form-filter-area {
        padding: 6px;
    }

    .bubbles {
        display: inline-block;
        font-family: arial;
        position: relative;
    }

    .bubbles h1 {
        position: relative;
        margin: 1em 0 0;
        font-family: 'Luckiest Guy', cursive;
        color: #fff;
        z-index: 2;
    }

    .individual-bubble {
        position: absolute;
        border-radius: 100%;
        bottom: 10px;
        background-color: #fff;
        z-index: 1;
    }

    /* Float four columns side by side */
    .column {
        float: left;
        width: 33%;
        padding: 0 10px;
        margin-bottom: 20px;
    }

    /* Remove extra left and right margins, due to padding in columns */
    .row {
        margin: 0 -5px;
    }

    /* Clear floats after the columns */
    .row:after {
        content: "";
        display: table;
        clear: both;
    }

    /* Style the counter cards */
    .card {
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2); /* this adds the "card" effect */
        padding: 16px;
        text-align: left;
        background-color: #f1f1f1;
        border-radius: 10px;
        height: 227px;
        position: relative;
        overflow: hidden;
        box-sizing: border-box;
        border: none;
        outline: 0;
        vertical-align: baseline;
        text-decoration: none;
        margin: 0;
        color: #fff;
    }

    .card h3>a {
        color: #fff;
    }

    .listing-count {
        position: absolute;
        left: 20px;
        top: 30px;
        z-index: 10;
        padding: 6px 16px;
        z-index: 3;
        color: #fff;
        border-radius: 4px;
        font-size: 0.845em;
        font-weight: 600;
        box-shadow: 0px 0px 0px 4px rgb(255 255 255 / 30%);
    }

    .listing-item-grid-title {
        position: absolute;
        left: 20px;
        bottom: 20px;
        right: 20px;
        z-index: 10;
    }

    .bg {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background-size: cover;
        background-attachment: scroll;
        background-position: center;
        background-repeat: repeat;
        background-origin: content-box;
        z-index: auto;
    }

    .img-overly {
        background-color: #022344;
        opacity: 0.6;
        height: 600px;
        position: absolute;
        width: 100%;
        background: #022344;
        z-index: auto;
    }

    .card .bg {
        -webkit-transform: translateZ(0);
        transform: translateZ(0);
        -webkit-transition: all 2000ms cubic-bezier(0.19, 1, 0.22, 1) 0ms;
        -o-transition: all 2000ms cubic-bezier(0.19, 1, 0.22, 1) 0ms;
        transition: all 2000ms cubic-bezier(0.19, 1, 0.22, 1) 0ms;
    }

    .card-link {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        color: #000;
    }

    /* Responsive columns - one column layout (vertical) on small screens */
    @media screen and (max-width: 600px) {
        .column {
            width: 100%;
            display: block;
            margin-bottom: 20px;
        }
    }
</style>
<section class="home-banner-area">
    <div class="bg-img-overlay"></div>
    <div class="container">
        <div class="main-banner">
            <div class="row">
                <div class="col-md-12 mt-5 bubbles">
                    <h1 class="title-head" style="color: white">Cari space iklan yang sesuai dengan <br> kebutuhan Anda
                        sekarang juga</h1>
                </div>
            </div>
            <form action="{{ route('search') }}">
                <div class="row form-filter mt-5">
                    <div class="col-md-3">
                        <div class="form-group">
                            <input type="text" class="form-control form-filter-area" id="media_name" name="media_name"
                                placeholder="ex: Billboard" autocomplete="off">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <select name="category" class="form-control">
                                <option value="" selected disabled>{{__('Select Media')}}</option>
                                @foreach (\App\Category::all() as $category)
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <select class="form-control" name="location" onchange="filter()">
                                <option value="" selected disabled>{{__('Select Province')}}</option>
                                @foreach (\App\State::all() as $key => $state)
                                <option value="{{ urlencode($state->name) }}">{{ $state->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <button type="submit" class="btn btn-new primary-ar btn-lg button-search"><i class="fa fa-search"></i>
                            Search</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>

<section class="mb-4">
    @php
    $flash_deal = \App\FlashDeal::where('status', 1)->first();
    @endphp
    @if($flash_deal != null && strtotime(date('d-m-Y')) >= $flash_deal->start_date && strtotime(date('d-m-Y')) <=
        $flash_deal->end_date)
        <div class="container">
            <div class="px-2 py-4 p-md-4 bg-white shadow-sm">
                <div class="section-title-1 clearfix">
                    <h3 class="heading-5 strong-700 mb-0 float-left">
                        <span class="mr-4">{{__('Flash Deal')}}</span> <br><br>
                        <div class="countdown countdown--style-1 countdown--style-1-v1"
                            data-countdown-date="{{ date('m/d/Y', $flash_deal->end_date) }}"
                            data-countdown-label="show"></div>
                    </h3>
                </div>
                <div class="caorusel-box">
                    <div class="slick-carousel" data-slick-items="6" data-slick-xl-items="5" data-slick-lg-items="4"
                        data-slick-md-items="3" data-slick-sm-items="2" data-slick-xs-items="2">
                        @foreach ($flash_deal->flash_deal_products as $key => $flash_deal_product)
                        @php
                        $product = \App\Product::find($flash_deal_product->product_id);
                        @endphp
                        @if ($product != null)
                        <div class="product-card-2 card card-product m-2 shop-cards shop-tech">
                            <div class="card-body p-0">

                                <div class="card-image">
                                    <a href="{{ route('product', $product->slug) }}" class="d-block"
                                        style="background-image:url('{{ asset($product->flash_deal_img) }}');">
                                    </a>
                                </div>

                                <div class="p-3" style="height: 150px;">
                                    <div class="price-box">
                                        @if(home_base_price($product->id) != home_discounted_base_price($product->id))
                                        <del class="old-product-price strong-400">{{ home_base_price($product->id)
                                            }}</del>
                                        @endif
                                        <span class="product-price strong-600">{{
                                            home_discounted_base_price($product->id) }}</span>
                                    </div>
                                    <div class="star-rating star-rating-sm mt-1">
                                        {{ renderStarRating($product->rating) }}
                                    </div>
                                    <h2 class="product-title p-0 text-truncate-2">
                                        <a href="{{ route('product', $product->slug) }}">{{ __($product->name) }}</a>
                                    </h2>
                                </div>

                                <div class="ribbon ribbon-top-left">
                                    <span class="bg-warning">Flash Sale</span>
                                </div>
                            </div>
                        </div>
                        @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        @else
        <div class="container" hidden></div>
        @endif
</section>

<section class="mb-4">
    <div class="container">
        <div class="row mt-5">
            <div class="col-md-12">
                <h1 class="explore-media">Explore by Locations</h1>
            </div>
        </div>
        <div class="row mt-5">
            @foreach ($popularCities as $item)
                <div class="column" @if($loop->last) style="width: 66%;" @endif>
                    <div class="card">
                        <div class="bg" style="background-image: url('{{ URL::asset($item->image) }}');
                        ">
                            <div class="img-overly"></div>
                        </div>
                        <a href="{{ route('search') }}?popular_city={{$item->city}}" class="card-link"></a>
                        <div class="listing-count primary-ar">
                            <span>{{ $item->count}}</span>
                            &nbsp;Locations
                        </div>
                        <div class="listing-item-grid-title">
                            <h3>
                                <a href="{{ route('search') }}?popular_city={{$item->city}}">{{$item->city}}</a>
                            </h3>
                            <p>
                                {{ $item->description }}
                            </p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>

<section class="mb-4">
    <div class="container">
        <div class="row mt-5">
            <div class="col-md-12">
                <h1 class="explore-media">Explore by Media Categories</h1>
            </div>
        </div>


        @php
        $category = \App\Category::all();
        @endphp
        <div class="row">
            <div class="owl-carousel owl-theme mt-5">
                @foreach ($category as $key => $val)
                <div class="item">
                    <a href="{{ route('products.category', $val['id']) }}">
                        <img src="{{ asset($val['banner']) }}" alt="" class="img-fluid img">
                    </a>
                </div>
                @endforeach
            </div>

        </div>
    </div>
</section>

<section class="mb-4">
    <div>
        <div class="row mt-3">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    @foreach (\App\Slider::all() as $key => $slider)
                    <div @if ($key==0) class="carousel-item active" @endif class="carousel-item">
                        <img class="d-block w-100" src="{{ url($slider->photo) }}" alt="{{$key}} slide">
                    </div>
                    @endforeach
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
</section>

@if (\App\BusinessSetting::where('type', 'best_selling')->first()->value == 1)
<section class="mb-4">
    <div class="container">
        <div class="px-2 py-4 p-md-4 bg-white shadow-sm">
            <div class="section-title-1 clearfix">
                <h3 class="heading-5 strong-700 mb-0">
                    <span class="mr-4">{{__('Recommended Ad Space')}}</span>
                </h3>
            </div>
            <div class="caorusel-box">
                <div class="slick-carousel" data-slick-items="3" data-slick-lg-items="3" data-slick-md-items="2"
                    data-slick-sm-items="1" data-slick-xs-items="1" data-slick-dots="true" data-slick-rows="4">
                    @foreach (filter_products(\App\Product::where('published', 1)->orWhere('is_recommended', 1)
                    ->orderBy('is_recommended', 'desc')
                    ->orderBy('rating', 'desc')
                    ->orderBy('num_of_sale', 'desc'))->limit(20)->get() as $key => $product)
                    <div class="p-2">
                        <div class="row no-gutters product-box-2 align-items-center">
                            <div class="col-4">
                                <div class="position-relative overflow-hidden h-100">
                                    <a href="{{ route('product', $product->slug) }}" class="d-block product-image h-100"
                                        style="background-image:url('{{ asset($product->thumbnail_img) }}');">
                                    </a>
                                    <div class="product-btns">
                                        <button class="btn add-wishlist" title="Add to Wishlist"
                                            onclick="addToWishList({{ $product->id }})">
                                            <i class="la la-heart-o"></i>
                                        </button>
                                        <button class="btn add-compare" title="Add to Compare"
                                            onclick="addToCompare({{ $product->id }})">
                                            <i class="la la-refresh"></i>
                                        </button>
                                        <button class="btn quick-view" title="Quick view"
                                            onclick="showAddToCartModal({{ $product->id }})">
                                            <i class="la la-eye"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-8 border-left">
                                <div class="p-3">
                                    <h2 class="product-title mb-0 p-0 text-truncate-2">
                                        <a href="{{ route('product', $product->slug) }}">{{ __($product->name) }}</a>
                                    </h2>
                                    <div class="star-rating star-rating-sm mb-2">
                                        {{ renderStarRating($product->rating) }}
                                    </div>
                                    <div class="clearfix">
                                        <div class="price-box float-left">
                                            @if (Auth::check())
                                            @if(home_base_price($product->id) !=
                                            home_discounted_base_price($product->id))
                                            <del class="old-product-price strong-400">{{ home_base_price($product->id)
                                                }}</del>
                                            @endif<br>
                                            <span class="product-price strong-600">{{
                                                variation_price($product->id) }}</span>
                                            @else
                                            <span class="product-price strong-600"
                                                title="Please login for show price">Please login for show price</span>
                                            @endif
                                        </div>
                                        <div class="float-right">
                                            <button class="add-to-cart btn" title="Add to Cart"
                                                onclick="showAddToCartModal({{ $product->id }})">
                                                <i class="la la-shopping-cart"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
@endif


@if (\App\BusinessSetting::where('type', 'vendor_system_activation')->first()->value == 1)
@php
$array = array();
foreach (\App\Seller::all() as $key => $seller) {
if($seller->user != null && $seller->user->shop != null){
$total_sale = 0;
foreach ($seller->user->products as $key => $product) {
$total_sale += $product->num_of_sale;
}
$array[$seller->id] = $total_sale;
}
}
asort($array);
@endphp
<section class="mb-5">
    <div class="container">
        <div class="px-2 py-4 p-md-4 bg-white shadow-sm">
            <div class="section-title-1 clearfix">
                <h3 class="heading-5 strong-700 mb-0 float-left">
                    <span class="mr-4">{{__('Media Partner')}}</span>
                </h3>
            </div>
            <div class="caorusel-box">
                <div class="slick-carousel" data-slick-items="5" data-slick-lg-items="5" data-slick-md-items="2"
                    data-slick-sm-items="2" data-slick-xs-items="1" data-slick-dots="true" data-slick-rows="2"
                    data-slick-autoplay="true">
                    @php
                    $count = 0;
                    @endphp
                    @foreach ($array as $key => $value)
                    @if ($count < 20) @php $count ++; $seller=\App\Seller::find($key); $total=0; $rating=0; foreach
                        ($seller->user->products as $key => $seller_product) {
                        $total += $seller_product->reviews->count();
                        $rating += $seller_product->reviews->sum('rating');
                        }
                        @endphp
                        <div class="p-2">
                            <div class="row no-gutters box-3 align-items-center border" style="height: 91px;">
                                <div class="col-4">
                                    <a href="{{ route('shop.visit', $seller->user->shop->slug) }}"
                                        class="d-block product-image p-3">
                                        <img src="{{ asset($seller->user->shop->logo) }}" alt="" class="img-fluid">
                                    </a>
                                </div>
                                <div class="col-8 border-left">
                                    <div class="p-3">
                                        <h2 class="product-title mb-0 p-0 text-truncate">
                                            <a href="{{ route('shop.visit', $seller->user->shop->slug) }}">{{
                                                __($seller->user->shop->name) }}</a>
                                        </h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                        @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
@endif

<section class="mb-4">
    <div class="container">
        <div class="px-2 py-4 p-md-4 bg-white shadow-sm">
            <div class="section-title-1 clearfix">
                <h3 class="heading-5 strong-700 mb-0">
                    <span class="mr-4">{{__('Testimonials')}}</span>
                </h3>
            </div>
        </div>
    </div>
</section>

@endsection

@section('script')

<script>
    jQuery(document).ready(function($){
        $('.owl-carousel').owlCarousel({
            loop:true,
            nav:true,
            responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            1000:{
                items:5
            }
            }
        });


        // Define a blank array for the effect positions.
        // This will be populated based on width of the title.
        var bArray = [];
        // Define a size array, this will be used to vary bubble sizes
        var sArray = [4,6,8,10];

        // Push the header width values to bArray
        for (var i = 0; i < $('.bubbles').width(); i++) {
            bArray.push(i);
        }

        // Function to select random array element
        // Used within the setInterval a few times
        function randomValue(arr) {
            return arr[Math.floor(Math.random() * arr.length)];
        }

        // setInterval function used to create new bubble every 350 milliseconds
        setInterval(function(){
            // Get a random size, defined as variable so it can be used for both width and height
            var size = randomValue(sArray);
            // New bubble appeneded to div with it's size and left position being set inline
            // Left value is set through getting a random value from bArray
            $('.bubbles').append('<div class="individual-bubble" style="left: ' + randomValue(bArray) + 'px; width: ' + size + 'px; height:' + size + 'px;"></div>');
            // Animate each bubble to the top (bottom 100%) and reduce opacity as it moves
            // Callback function used to remove finsihed animations from the page
            $('.individual-bubble').animate({
                'bottom': '100%',
                'opacity' : '-=0.7'
            }, 3000, function(){
                $(this).remove()
            });

        }, 350);

    });
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
@endsection
