@extends('frontend.layouts.app')

@section('header')
<link rel="stylesheet" href="{{ asset('css/blog.css') }}" type="text/css">
<style>
    .blog-layout {
        padding-left: 6rem;
        padding-right: 6rem;
    }
</style>
@endsection

@section('content')
    <div style="margin-top: 80px;">
    </div>

    <div class="breadcrumb-area">
        <div class="row pt-0">
            <div class="col">
                <ul class="breadcrumb">
                    <li><a href="http://127.0.0.1:8000">Home</a></li>
                    <li><a href="http://127.0.0.1:8000/blog">Blog</a></li>
                </ul>
            </div>
        </div>
    </div>

    <section class="bg-gray py-2">
        <div class="row blog-layout">
            <div class="col-md-9">
                <div class="row bg-white">
                    <article>
                        content
                    </article>
                </div>
            </div>
            <div class="col-md-3 d-none d-md-block">
                <div class="bg-white sidebar-box mb-3">
                    navbar
                </div>
            </div>
        </div>
    </section>
@endsection
