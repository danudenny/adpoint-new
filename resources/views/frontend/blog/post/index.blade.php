@extends('frontend.layouts.app')

@section('header')
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
    <style>

        .card {
            background: #fff;
            transition: .5s;
            border: 0;
            margin-bottom: 30px;
            border-radius: 0.55rem;
            position: relative;
            width: 100%;
            box-shadow: 0 1px 2px 0 rgb(0 0 0 / 10%);
        }
        .card .body {
            color: #444;
            padding: 20px;
            font-weight: 400;
        }
        .card .header {
            color: #444;
            padding: 20px;
            position: relative;
            box-shadow: none;
        }
        .single_post {
            -webkit-transition: all .4s ease;
            transition: all .4s ease;
        }

        .single_post .body {
            padding: 30px;
        }

        .single_post .img-post {
            position: relative;
            overflow: hidden;
            max-height: 500px;
            margin-bottom: 30px;
        }

        .single_post .img-post>img {
            -webkit-transform: scale(1);
            -ms-transform: scale(1);
            transform: scale(1);
            opacity: 1;
            -webkit-transition: -webkit-transform .4s ease, opacity .4s ease;
            transition: transform .4s ease, opacity .4s ease;
            max-width: 100%;
            filter: none;
            -webkit-filter: grayscale(0);
            -webkit-transform: scale(1.01);
        }

        .single_post .img-post:hover img {
            -webkit-transform: scale(1.02);
            -ms-transform: scale(1.02);
            transform: scale(1.02);
            opacity: 0.7;
            filter: gray;
            -webkit-filter: grayscale(1);
            -webkit-transition: all .8s ease-in-out;
        }

        .single_post .img-post:hover .social_share {
            display: block;
        }

        .single_post .footer {
            padding: 0 30px 30px 30px;
            background: #ffffff!important;
        }

        .single_post .footer .actions {
            display: inline-block;
        }

        .single_post .footer .stats {
            cursor: default;
            list-style: none;
            padding: 0;
            display: inline-block;
            float: right;
            margin: 0;
            line-height: 35px;
        }

        .single_post .footer .stats li {
            border-left: solid 1px rgba(160, 160, 160, 0.3);
            display: inline-block;
            font-weight: 400;
            letter-spacing: 0.25em;
            line-height: 1;
            margin: 0 0 0 2em;
            padding: 0 0 0 2em;
            text-transform: uppercase;
            font-size: 13px;
        }

        .single_post .footer .stats li a {
            color: #777;
        }

        .single_post .footer .stats li:first-child {
            border-left: 0;
            margin-left: 0;
            padding-left: 0;
        }

        .single_post h3 {
            font-size: 20px;
            text-transform: uppercase;
            font-weight: 700;
        }

        .single_post h3 a {
            color: #242424;
            text-decoration: none;
        }

        .single_post p {
            font-size: 16px;
            line-height: 26px;
            font-weight: 300;
            margin: 0;
        }

        .single_post .blockquote p {
            margin-top: 0 !important;
        }

        .single_post .meta {
            list-style: none;
            padding: 0;
            margin: 0;
        }

        .single_post .meta li {
            display: inline-block;
            margin-right: 15px;
        }

        .single_post .meta li a {
            font-style: italic;
            color: #959595;
            text-decoration: none;
            font-size: 12px;
        }

        .single_post .meta li a i {
            margin-right: 6px;
            font-size: 12px;
        }

        .single_post2 {
            overflow: hidden;
        }

        .single_post2 .content {
            margin-top: 15px;
            margin-bottom: 15px;
            padding-left: 80px;
            position: relative
        }

        .single_post2 .content .actions_sidebar {
            position: absolute;
            top: 0px;
            left: 0px;
            width: 60px;
        }

        .single_post2 .content .actions_sidebar a {
            display: inline-block;
            width: 100%;
            height: 60px;
            line-height: 60px;
            margin-right: 0;
            text-align: center;
            border-right: 1px solid #e4eaec;
        }

        .single_post2 .content .title {
            font-weight: 100;
        }

        .single_post2 .content .text {
            font-size: 15px;
        }

        .right-box .categories-clouds li {
            display: inline-block;
            margin-bottom: 5px;
        }

        .right-box .categories-clouds li a {
            display: block;
            border: 1px solid;
            padding: 6px 10px;
            border-radius: 3px;
        }

        .right-box .instagram-plugin {
            overflow: hidden;
        }

        .right-box .instagram-plugin li {
            float: left;
            overflow: hidden;
            border: 1px solid #fff;
        }

        .comment-reply li {
            margin-bottom: 15px;
        }

        .comment-reply li:last-child {
            margin-bottom: none;
        }

        .comment-reply li h5 {
            font-size: 18px
        }

        .comment-reply li p {
            margin-bottom: 0px;
            font-size: 15px;
            color: #777;
        }

        .comment-reply .list-inline li {
            display: inline-block;
            margin: 0;
            padding-right: 20px;
        }

        .comment-reply .list-inline li a {
            font-size: 13px;
        }

        @media (max-width: 640px) {
            .blog-page .left-box .single-comment-box>ul>li {
                padding: 25px 0;
            }
            .blog-page .left-box .single-comment-box ul li .icon-box {
                display: inline-block;
            }
            .blog-page .left-box .single-comment-box ul li .text-box {
                display: block;
                padding-left: 0;
                margin-top: 10px;
            }
            .blog-page .single_post .footer .stats {
                float: none;
                margin-top: 10px;
            }
            .blog-page .single_post .body,
            .blog-page .single_post .footer {
                padding: 30px;
                background: #ffffff!important;
            }
        }
    </style>
@endsection

@section('content')

    <div class="breadcrumb-area mt-4">
        <div class="row pt-0">
            <div class="col">
                <ul class="breadcrumb">
                    <li><a href="{{ route('home') }}">Home</a></li>
                    <li><a href="{{ route('info.blog.post') }}">Blog</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div id="main-content" class="blog-page mt-4">
        <div class="container">
            <div class="row clearfix">
                <div class="col-lg-8 col-md-12 left-box">
                    @if($posts->total() > 0)
                    @foreach($posts as $post)
                        <div class="card single_post">
                        <div class="body">
                            <div class="img-post">
                                @php
                                    $images = json_decode($post->blog_image_id);
                                    $postImages = null;
                                    if ($images != 0) {
                                        $postImages = \App\BlogImages::select('path')->whereIn('id', $images)->get();
                                    }
                                @endphp
                                <img class="d-block img-fluid" src="{{ $postImages != null ? asset($postImages[0]['path']) : '' }}" alt="image post">
                            </div>
                            <h3><a href="{{ route('info.post.detail', encrypt($post->id)) }}">{{ $post->title }}</a></h3>
                            <p>{{ \Illuminate\Support\Str::limit(strip_tags(html_entity_decode($post->content)), 255, '...') }}</p>
                        </div>
                        <div class="footer">
                            <div class="actions">
                                <a href="{{ route('info.post.detail', encrypt($post->id)) }}" class="btn btn-outline-secondary">Continue Reading</a>
                            </div>
                            <ul class="stats">
                                <li><a href="javascript:void(0);" class="fa fa-calendar">&nbsp;{{ $post->created_at->format('M d, Y') }}</a></li>
                                <li><a href="javascript:void(0);">&nbsp;{{ $post->category->title }}</a></li>
                                <li><a href="javascript:void(0);" class="fa fa-eye">&nbsp;{{ $post->count_views }}</a></li>
                            </ul>
                        </div>
                    </div>
                    @endforeach
                    @else
                        <div class="m-3 alert alert-primary" role="alert">
                            <h6 class="text-italic"><i class="fa fa-filter"></i> Belum ada postingan, ditemukan <b
                                    style="color:orange">{{
									$posts->total() }}</b> post.</h6>
                        </div>
                        <div class="col mt-3 d-flex justify-content-center">
                            <img src="{{ url('img/Match.png') }}" alt="" width="300px">
                        </div>
                    @endif
                    @if ($posts->hasPages())
                        <div class="pagination-wrapper">
                            {{ $posts->links() }}
                        </div>
                    @endif
                </div>

                <div class="col-lg-4 col-md-12 right-box">
                    <div class="card">
                        <div class="body search">
                            <div class="input-group m-b-0">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" onclick="searchPost()" onkeypress="searchPost()"><i class="fa fa-search"></i></span>
                                </div>
                                <input type="text" class="form-control" id="search_post" placeholder="Search...">
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="header">
                            <h2>Blog Categories</h2>
                        </div>
                        <div class="body widget">
                            <ul class="list-unstyled categories-clouds m-b-0">
                                @foreach($blogCategories as $blogCategory)
                                    <li><a href="{{ route('info.blog.post') }}?category={{ encrypt($blogCategory->id) }}">{{ $blogCategory->title }}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="card">
                        <div class="header">
                            <h2>Popular Posts</h2>
                        </div>
                        <div class="body widget popular-post">
                            <div class="row">
                                <div class="col-lg-12">
                                    @foreach($popularPosts as $popularPost)
                                    @php
                                        $images = json_decode($popularPost->blog_image_id);
                                        $postImages = null;
                                        if ($images != 0) {
                                            $postImages = \App\BlogImages::select('path')->whereIn('id', $images)->get();
                                        }
                                    @endphp
                                    <div class="single_post">
                                        <p class="m-b-0">{{ $popularPost->title }}</p>
                                        <span>{{ $popularPost->created_at->format('M d, Y') }}</span>
                                        <div class="img-post">
                                            <img src="{{ $postImages != null ? asset($postImages[0]['path']) : '' }}" alt="image post">
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="header">
                            <h2>Instagram Post</h2>
                        </div>
                        <div class="body widget">
                            <ul class="list-unstyled instagram-plugin m-b-0">
                                @foreach($igPost as $post)
                                    <li>
                                        <a href="{{$post['link']}}"><img src="data:image/jpeg;base64,{{$post['url']}}" width="80px" height="80px" alt="instagram feed"></a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
