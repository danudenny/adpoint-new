@extends('frontend.layouts.app')
@section('content')
<section class="gry-bg py-4 profile">
    <div class="container">
        <div class="card">
            <div class="card-body">
                <div style="text-align: center">
                    <h3><strong>DETAIL PEMBAYARAN</strong></h3>
                    <p>No. Transaksi : {{ $getPaymentDetails->code }}</p>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-6" style="margin: 0 auto">
                        @php
                        $user = json_decode($getPaymentDetails->address);
                        @endphp
                        <table class="table table-striped">
                            <tr>
                                <td>Nama Pembeli</td>
                                <td> : </td>
                                <td>{{ $user->name }}</td>
                            </tr>
                            <tr>
                                <td>Alamat Pembeli</td>
                                <td> : </td>
                                <td>
                                    {{ $user->address }}<br>
                                    {{ $user->city }}<br>
                                    {{ $user->country }}<br>
                                    {{ $user->postal_code }}
                                </td>
                            </tr>
                            <tr>
                                <td>Jenis Pembayaran</td>
                                <td> : </td>
                                <td>Transfer Manual</td>
                            </tr>
                            <tr>
                                @if ($getPaymentDetails->payment_type === 'MANDIRI MANUAL')
                                <td>Bank</td>
                                <td> : </td>
                                <td>Bank Mandiri</td>
                                @else
                                <td>Bank</td>
                                <td> : </td>
                                <td>Bank BCA</td>
                                @endif
                            </tr>
                            <tr>
                                @if ($getPaymentDetails->payment_type === 'MANDIRI MANUAL')
                                <td>Nomor Rekening</td>
                                <td> : </td>
                                <td><strong>13300290929</strong></td>
                                @else
                                <td>Nomor Rekening</td>
                                <td> : </td>
                                <td><strong>14090279782</strong></td>
                                @endif
                            </tr>
                            <tr>
                                <td>Jumlah Yang Dibayarkan</td>
                                <td> : </td>
                                <td><strong>Rp {{ number_format($getPaymentDetails->grand_total) }}</strong></td>
                            </tr>
                            @php
                            $getBuktitrf = App\ConfirmPayment::select('id', 'bukti')->where('code_trx',
                            $getPaymentDetails->code)->first();
                            @endphp
                            @if ($getBuktitrf)
                            <tr>
                                <td>Bukti Transfer</td>
                                <td> : </td>
                                <td><a href="{{ asset($getBuktitrf->bukti) }}" class="btn btn-sm btn-base-2" download>
                                        <i class="fa fa-download"></i> Download Image</a>
                                </td>
                            </tr>
                            @endif
                        </table>
                    </div>
                </div>
                <div style="text-align: center">
                    <button type="button" class="btn btn-primary"
                        onclick=" window.open('{{ url('purchase_history') }}', '_self');">
                        <i class="fa fa-retweet"></i>
                        Kembali Ke Halaman Order</button>
                    @if ($getBuktitrf === null)
                    <a data-toggle="modal" data-target="#sure" class="btn btn-success text-white"><i
                            class="fa fa-check"></i>
                        Konfirmasi Pembayaran</a>
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Modal -->
<div class="modal fade" id="sure" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 600px" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Konfirmasi?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ url('confirm-payment') .'/'. $getPaymentDetails->id }}" method="POST"
                    enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <h6>Upload Bukti Transfer</h6>
                        <input type="file" class="form-control" name="bukti_trf">
                        @if ($errors->has('title'))
                        <span class="required-field" role="alert">
                            <strong>{{ $errors->first('bukti_trf') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group d-flex justify-content-end" style="column-gap: 10px;">
                        <button class="btn btn-success" type="submit">
                            <i class="fa fa-check"></i>
                            Submit</button>
                        <button type="button" class="btn btn-warning text-white" data-dismiss="modal"
                            aria-label="Close">
                            <i class="fa fa-close"></i>
                            Cancel
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
@endsection

@section('script')
<script>
    function uploadAndConfirm() {
        
    }
</script>
@endsection