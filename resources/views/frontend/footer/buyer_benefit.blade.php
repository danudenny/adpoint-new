@extends('frontend.layouts.app')

@section('content')
    <div style="margin-top: 87px;"></div>
    <div class="container card mt-10 mb-2">
        <div class="row">
            <div class="col-md-12">
                <h1>Buyer Benefit</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @foreach ($data as $benefit)
                    <div class="row">
                        <div class="col-md-3 d-flex flex-wrap align-content-center">
                            <img src="{{ asset($benefit->image) }}" class="w-75" alt="">
                        </div>
                        <div class="col-md-9 d-flex flex-wrap align-content-center">
                            <p>{{ $benefit->description }}</p>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
