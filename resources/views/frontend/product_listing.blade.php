@extends('frontend.layouts.app')

@if (isset($subcategory_id))
@php
$meta_title = \App\SubCategory::find($subcategory_id)->meta_title;
$meta_description = \App\SubCategory::find($subcategory_id)->meta_description;
@endphp
@elseif (isset($category_id))
@php
$meta_title = \App\Category::find($category_id)->meta_title;
$meta_description = \App\Category::find($category_id)->meta_description;
@endphp
@elseif (isset($brand_id))
@php
$meta_title = \App\Brand::find($brand_id)->meta_title;
$meta_description = \App\Brand::find($brand_id)->meta_description;
@endphp
@else
@php
$meta_title = env('APP_NAME');
$meta_description = \App\SeoSetting::first()->description;
@endphp
@endif

@section('meta_description'){{ $meta_description }}@stop

@section('meta')
<!-- Schema.org markup for Google+ -->
<meta itemprop="name" content="{{ $meta_title }}">
<meta itemprop="description" content="{{ $meta_description }}">

<!-- Twitter Card data -->
<meta name="twitter:title" content="{{ $meta_title }}">
<meta name="twitter:description" content="{{ $meta_description }}">

<!-- Open Graph data -->
<meta property="og:title" content="{{ $meta_title }}" />
<meta property="og:description" content="{{ $meta_description }}" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet.markercluster/1.5.3/MarkerCluster.css"
	integrity="sha512-mQ77VzAakzdpWdgfL/lM1ksNy89uFgibRQANsNneSTMD/bj0Y/8+94XMwYhnbzx8eki2hrbPpDm0vD0CiT2lcg=="
	crossorigin="anonymous" referrerpolicy="no-referrer" />

<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/leaflet.markercluster/1.5.3/MarkerCluster.Default.css"
	integrity="sha512-6ZCLMiYwTeli2rVh3XAPxy3YoR5fVxGdH/pz+KMCzRY2M65Emgkw00Yqmhh8qLGeYQ3LbVZGdmOX9KUjSKr0TA=="
	crossorigin="anonymous" referrerpolicy="no-referrer" />

<style>
	.marker-pin {
		width: 30px;
		height: 30px;
		border-radius: 50% 50% 50% 0;
		background: #c30b82;
		position: absolute;
		transform: rotate(-45deg);
		left: 50%;
		top: 50%;
		margin: -15px 0 0 -15px;
	}

	/*to draw white circle*/
	.marker-pin::after {
		content: '';
		width: 24px;
		height: 24px;
		margin: 3px 0 0 3px;
		background: #fff;
		position: absolute;
		border-radius: 50%;
	}

	/*to align icon*/
	.custom-div-icon i {
		position: absolute;
		width: 22px;
		font-size: 22px;
		left: 0;
		right: 0;
		margin: 10px auto;
		text-align: center;
	}

	.custom-div-icon i.awesome {
		margin: 12px auto;
		font-size: 17px;
	}

	.custom-div-icon img.img-marker {
		margin: 9px 11px 12px 3px;
		font-size: 17px;
	}

	.img-marker {
		height: 24px;
		position: fixed;
		border-radius: 50%;
	}

	.custom-popup .leaflet-popup-content-wrapper {
		background: #2c3e50;
		color: #fff;
		font-size: 16px;
		line-height: 24px;
		border-radius: 0px;
	}

	.custom-popup .leaflet-popup-content-wrapper a {
		color: rgba(255, 255, 255, 0.1);
	}

	.custom-popup .leaflet-popup-tip-container {
		width: 30px;
		height: 15px;
	}

	.custom-popup .leaflet-popup-tip {
		background: transparent;
		border: none;
		box-shadow: none;
	}

	/* css to customize Leaflet default styles  */
	.custom .leaflet-popup-tip,
	.custom .leaflet-popup-content-wrapper {
		background: #eaeaea;
		color: #052649;
		border-radius: 0
	}
</style>

@endsection

@section('content')
<div class="breadcrumb-area">
	<div class="row pt-0 pl-4 pr-4">
		<div class="col">
			<ul class="breadcrumb">
				<li><a href="{{ route('home') }}">{{__('Home')}}</a></li>
				<li><a href="{{ route('products') }}">{{__('All Categories')}}</a></li>
			</ul>
		</div>
	</div>
</div>

@foreach ($products as $key => $product)
<input type="hidden" id="prov" value="{{ $product->provinsi }}">
@endforeach
<form action="{{ route('search') }}" method="GET">
	<section class="bg-gray py-2">
		<div class="row p-4">
			<div class="col-md-12">
				<div id="mapView"
					style="width: 100%; height: 600px; display: none; position: relative; overflow: hidden; z-index: 0">
				</div>
			</div>
		</div>

		<div class="row pl-4 pr-4">
			<div class="col-md-3 d-none d-md-block">
				<div class="bg-white sidebar-box mb-3">
					<button class="btn btn-block btn-success btn-lg" onclick="viewMaps()" type="button">
						<i class="fa fa-map"></i> View Media Ads on Maps</button>
				</div>
				<div class="bg-white sidebar-box mb-3">
					<div class="box-title text-center">
						{{__('Filter')}}
					</div>

					{{-- Category Filter --}}
					<div class="box-content">
						<div class="mt-3">
							<div class="row">
								<div class="col-md-12">
									<p style="font-weight:600">Keyword</p>
									@if(Request::get('media_name') != null)
									<input type="text" class="form-control" placeholder="Masukkan keyword"
										name="media_name" value="{{ Request::get('media_name') }}">
									@else
									<input type="text" class="form-control" placeholder="Masukkan keyword"
										name="media_name">
									@endif
								</div>
							</div>
						</div>

						<div class="mt-3">
							<div class="row">
								<div class="col-md-12">
									<p style="font-weight:600">Category</p>
									<select class="form-control" data-placeholder="{{__('All Categories')}}"
										name="category" onchange="filter()">
										<option value="" selected>{{__('All Categories')}}</option>
										@foreach (\App\Category::all() as $key => $category)
										@php
										$getCat = Request::get('category');
										@endphp

										@if(Request::get('category') != null)
										<option value="{{ $category->id }} " @if($getCat==$category->id)
											selected
											@endif>{{ $category->name }}</option>
										@else
										<option value="{{ $category->id }}">{{ $category->name }}
										</option>
										@endif
										@endforeach
									</select>
								</div>
							</div>
						</div>

						{{-- Province Filter --}}
						<div class="mt-3">
							<div class="row">
								<div class="col-md-12">
									@php
									$loca = Request::get('location');
									if ($loca) {
									$splitName = explode('+', $loca);
									if (count($splitName) === 1) {
									$lastloc = $splitName[0];
									} else {
									$lastloc = $splitName[0] .' '. $splitName[1];
									}
									}
									@endphp
									<p style="font-weight:600">Province</p>
									<select class="form-control" data-placeholder="{{__('All Province')}}"
										name="location" onchange="filter()" id="location">
										<option value="" selected>{{__('All Province')}}</option>
										@foreach (\App\State::all() as $key => $state)

										@if(Request::get('location') != null)
										<option value="{{ urlencode($state->name) }} " @if($lastloc==$state->name)
											selected
											@endif>{{ $state->name }}</option>
										@else
										<option value="{{ urlencode($state->name) }}">{{ $state->name }}
										</option>
										@endif
										@endforeach
									</select>
								</div>
							</div>
						</div>

                        {{-- Cities Filter --}}

                        {{-- Districts Filter --}}


						{{-- Sort Filter --}}
						<div class="mt-3">
							<div class="row">
								<div class="col-md-12">
									<p style="font-weight:600">Sort By</p>
									<select class="form-control" data-minimum-results-for-search="Infinity"
										name="sort_by" id="sort_by" onchange="filter()">
										<option value="1" @isset($sort_by) @if ($sort_by=='1' ) selected @endif
											@endisset>
											{{__('Newest')}}</option>
										<option value="2" @isset($sort_by) @if ($sort_by=='2' ) selected @endif
											@endisset>
											{{__('Oldest')}}</option>
										<option value="3" @isset($sort_by) @if ($sort_by=='3' ) selected @endif
											@endisset>{{__('Price
											low to high')}}</option>
										<option value="4" @isset($sort_by) @if ($sort_by=='4' ) selected @endif
											@endisset>{{__('Price
											high to low')}}</option>
										<option value="5" @isset($sort_by) @if ($sort_by=='5' ) selected @endif
											@endisset>{{__('Highest
											Rating')}}</option>
									</select>
								</div>
							</div>
						</div>

						{{-- Media Owner Filter --}}
						<div class="mt-3">
							<div class="row">
								<div class="col-md-12">
									<p style="font-weight:600">Media Owner</p>
									<select class="form-control" data-placeholder="{{__('Media Owner')}}" name="brand"
										onchange="filter()" id="brand">
										<option value="" selected>{{__('Semua')}}</option>
										@php
										$brandSlug = Request::get('brand');
										@endphp
										@foreach (\App\Brand::orderBy('name', 'asc')->get() as $key => $brand)
										@if($brandSlug)
										<option value="{{ urlencode($brand->slug) }}" @if($brandSlug==$brand->slug)
											selected
											@endif>{{ $brand->name }}</option>
										@else
										<option value="{{ urlencode($brand->slug) }}">{{ $brand->name }}
										</option>
										@endif
										@endforeach
									</select>
								</div>
							</div>
						</div>

						{{-- Price Range Filter --}}
						<div class="mt-3">
							<div class="row">
								<div class="col-md-6">
									<p style="font-weight:600">Minimum (Rp)</p>
									@if (Request::get('min_price'))
									<input type="text" class="form-control" name="min_price"
										value="{{ Request::get('min_price') }}">
									@else
									<input type="text" class="form-control" name="min_price" value=0>
									@endif
								</div>
								<div class="col-md-6">
									<p style="font-weight:600">Maksimal (Rp)</p>
									@if (Request::get('max_price'))
									<input type="text" class="form-control" name="max_price"
										value="{{ Request::get('max_price') }}">
									@else
									<input type="text" class="form-control" name="max_price" value=0>
									@endif
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<button type="submit" class="btn btn-block btn-info mt-2"><i class="fa fa-filter"></i>
									Filter</button>
								<a href="{{ route('search') }}" class="btn btn-block btn-warning mt-2"><i
										class="fa fa-trash"></i> Reset
									Filter</a>
							</div>
						</div>
					</div>
				</div>
			</div>


			<div class="col-md-9">
				<div class="brands-bar row bg-white py-3">
					<div class="col-11">
						@isset($category_id)
						<input type="hidden" name="category" value="{{ \App\Category::find($category_id)->slug }}">
						@endisset
						@isset($subcategory_id)
						<input type="hidden" name="subcategory"
							value="{{ \App\SubCategory::find($subcategory_id)->slug }}">
						@endisset

						<div class="row bg-white pt-0">

							<div class="col-md-4">
								<div class="form-group">
									<label>Display</label><br>
									<div class="btn-group" role="group" aria-label="Basic example">
										<button type="button" id="grid" class="p-2 btn btn-secondary active"
											title="Grid View">
											<i class="fa fa-th"></i> Grid
										</button>
										<button type="button" id="list" class="p-2 btn btn-secondary" title="List View">
											<i class="fa fa-list"></i> List
										</button>
									</div>

								</div>
							</div>
						</div>
					</div>
				</div>


				<div class="row bg-white mt-2">
					<div class="col-md-12">
						@if ($products->total() > 0)
						<div class="m-3 alert alert-primary" role="alert">
							<h6 class="text-italic"><i class="fa fa-filter"></i> Hasil pencarian anda, ditemukan <b
									style="color:orange">{{
									$products->total() }}</b> media.</h6>
						</div>
						<div class="row md-no-gutters gutters-5 p-2">
							@foreach ($products as $key => $product)
							<div id="product_view" class="col-md-3 col-6">
								<div class="product-box-2 bg-white alt-box my-2">
									<div class="position-relative overflow-hidden">
										<a href="{{ route('product', $product->slug) }}"
											class="d-block product-image h-100"
											style="background-image:url('{{ asset($product->thumbnail_img) }}');"
											tabindex="0">
										</a>
										<div class="product-btns clearfix">
											<button class="btn add-wishlist" title="Add to Wishlist"
												onclick="addToWishList({{ $product->id }})" tabindex="0">
												<i class="la la-heart-o"></i>
											</button>
											<button class="btn add-compare" title="Add to Compare"
												onclick="addToCompare({{ $product->id }})" tabindex="0">
												<i class="la la-refresh"></i>
											</button>
											<button class="btn quick-view" title="Quick view"
												onclick="showAddToCartModal({{ $product->id }})" tabindex="0">
												<i class="la la-eye"></i>
											</button>
										</div>
									</div>
									<div class="p-3 border-top">
										<h2 class="product-title p-0 text-truncate">
											<a href="{{ route('product', $product->slug) }}" tabindex="0">{{
												__($product->name) }}</a>

										</h2>
										<div class="d-flex justify-content-between">
											<div>
												<div class="star-rating mb-1">
													{{ renderStarRating($product->rating) }}
												</div>
											</div>
										</div>
										<div class="clearfix">
											@if (Auth::check())
											<div class="price-box float-left">
												@if(home_base_price($product->id) !=
												home_discounted_base_price($product->id))
												<del class="old-product-price strong-400">{{
													home_base_price($product->id)
													}}</del>
												@endif
												<span class="product-price strong-600">{{
													variation_price($product->id) }}</span>
											</div>
											@else
											<span class="product-price strong-600"
												title="Please login for show price">Login
												to show price</span>
											@endif
										</div>
										<p>
											<i class="fa fa-map-marker"></i> {{ substr($product->alamat, 0, 40) }}...
										</p>
									</div>
								</div>
								@if ($product->available === 1)
								<div class="ribbon ribbon-top-left">
									<span class="bg-success">Available</span>
								</div>
								@else
								<div class="ribbon ribbon-top-left">
									<span class="bg-danger">Not Available</span>
								</div>
								@endif
							</div>
							@endforeach
						</div>
						@else
						<div class="m-3 alert alert-primary" role="alert">
							<h6 class="text-italic"><i class="fa fa-filter"></i> Hasil pencarian anda, ditemukan <b
									style="color:orange">{{
									$products->total() }}</b> media.</h6>
						</div>
						<div class="col mt-3 d-flex justify-content-center">
							<img src="{{ url('img/Match.png') }}" alt="" width="300px">
						</div>
						@endif
					</div>
				</div>
				<div class="row bg-white justify-content-center p-2">
					<nav aria-label="Center aligned pagination">
						<ul class="pagination justify-content-center">
							{{ $products->appends($_GET)->links() }}
						</ul>
					</nav>
				</div>
				{{-- @dd($products->links()) --}}

				<!-- </div> -->
			</div>
		</div>
	</section>
</form>

@endsection

@section('script')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.6.0/leaflet.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/leaflet.markercluster/1.5.3/leaflet.markercluster.js"
	integrity="sha512-OFs3W4DIZ5ZkrDhBFtsCP6JXtMEDGmhl0QPlmWYBJay40TT1n3gt2Xuw8Pf/iezgW9CdabjkNChRqozl/YADmg=="
	crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<script type="text/javascript">
	var map = L.map('mapView',{
		center: [-4.546775, 120.40584999999999],
		zoom: 5
	});
    // var map = L.map('mapView', {disableDoubleClickZoom: true, zoom: 5}).locate({setView : true, maxZoom: 19});

    let filterMaps = {
        'category' : $("input[name='category']").val(),
        'subCategory' : $("input[name='subcategory']").val(),
        'location' : $("#location option:selected" ).val(),
        'sort' : $("#sort_by option:selected").val(),
        'brand' : $("#brand option:selected" ).val(),
        'min_price' : $("input[name='min_price']").val(),
        'max_price' : $("input[name='max_price']").val(),
    };

    let icon = L.divIcon({
        className: 'custom-div-icon',
        html: "<div style='background-color:#ffc522;' class='marker-pin'></div><img class='img-marker' src='/uploads/DdWIUmkML1MvQvufdnr8DZ8RjyvRHFHlZcwdPvsS.ico'>",
        iconSize: [30, 42],
        iconAnchor: [15, 42]
    });

    getMaps();

	function getMaps() {
        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            maxZoom: 19,
            attribution: '&copy; <a href="https://openstreetmap.org/copyright">OpenStreetMap contributors</a>'
        }).addTo(map);

        // show the scale bar on the lower left corner
        L.control.scale({imperial: true, metric: true}).addTo(map);
        let isClicked = false;

		const markers = L.markerClusterGroup();
        $.get('<?php echo e(route('ads.loc')); ?>', {
            brand : filterMaps.brand??null,
            category : filterMaps.category??null,
            subcategory : filterMaps.subCategory??null,
            location : filterMaps.location??null,
            sort_by : filterMaps.sort??1,
            min_price : filterMaps.min_price??null,
            max_price : filterMaps.max_price??null
        }, function(response){
            L.geoJSON(response, {
                pointToLayer: function(geoJsonPoint, latlng) {
                    var marker =  L.marker(latlng, { icon: icon });
					var base_url = window.location.origin;

					var customPopup =
					`<div class="mt-2 mb-2"><h6 style="font-weight: 700">${geoJsonPoint.properties.name}</h6></div><hr>` +
					'<div>' +
						`<img src="${base_url}/${geoJsonPoint.properties.img}" alt="${geoJsonPoint.properties.name}" style="width: 100%"/>` +
					'</div>' +
					`<div class="mt-2"><p style="font-size: 11px;"><i class="fa fa-map-marker"></i> ${geoJsonPoint.properties.alamat}</p></div><hr>` +
					'<div class="mt-2">'+
						`<a href="${base_url}/product/${geoJsonPoint.properties.slug}" class="btn btn-flat btn-block btn-success">Detail</a>`+
					'</div>'


					var customOptions = {
						'maxWidth': '300',
						'width': '200',
						'className': 'custom',
						closeButton: true,
  						autoClose: false
					}

					marker.bindPopup(customPopup,customOptions);

					return markers.addLayer(marker)
                }
            }).addTo(map)
        })

		map.addLayer(markers)
    }

    map.on({
        click: function() {
            isClicked = false
        },
        popupclose: function () {
            isClicked = false
        }
    });

    function viewMaps() {
        map._onResize();
        if ($("#mapView").css("display") == "none") {
            $("#mapView").css("display", "block");
        }else {
            $("#mapView").css("display", "none");
        }
    };

    $('#list').on('click', function() {
            var productList = document.querySelectorAll('#product_view');
            productList.forEach(element => {
                $(element).removeClass('col-md-3');
                $(element).removeClass('col-6');
                $(element).addClass('col-md-12')
                $(element).addClass('col-12')
            });
            $(this).addClass('active');
            $('#grid').removeClass('active');
        });
        $('#grid').on('click', function() {
            var productList = document.querySelectorAll('#product_view');
            productList.forEach(element => {
                $(element).removeClass('col-md-12');
                $(element).removeClass('col-12');
                $(element).addClass('col-md-3')
                $(element).addClass('col-6')
            });
            $(this).addClass('active');
            $('#list').removeClass('active');
        });
        // function filter(){
        //     $('#search-form').submit();
        // }

        // function locations(e){
        //     $('#location').val(encodeURI(e));
        //     filter();
        // }
</script>
@endsection
