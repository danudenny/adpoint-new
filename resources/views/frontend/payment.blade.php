@extends('frontend.layouts.app')
@section('content')
<section class="gry-bg py-4 profile">
    <div class="container">
        <div class="card">
            <div class="card-body">
                @if ( $getPaymentDetails->expiration_date > now()->toDateTimeLocalString())
                <div style="text-align: center">
                    <h1>Detail Pembayaran</h1>
                    <h3>Pembayaran Telah Melewati Batas Waktu</b></h3>
                </div>
                <hr>
                @else
                <div style="text-align: center">
                    <h1>Detail Pembayaran</h1>
                    <h3>Bayar Sebelum Tanggal <b>{{ $getPaymentDetails->expiration_date }}</b></h3>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-6" style="margin: 0 auto">
                        @php
                        $user = json_decode($getPaymentDetails->address);
                        @endphp
                        <table class="table table-striped">
                            {{-- <tr>
                                <td>Nama Pembeli</td>
                                <td> : </td>
                                <td>{{ $user->name }}</td>
                            </tr>
                            <tr>
                                <td>Alamat Pembeli</td>
                                <td> : </td>
                                <td>
                                    {{ $user->address }}<br>
                                    {{ $user->city }}<br>
                                    {{ $user->country }}<br>
                                    {{ $user->postal_code }}
                                </td>
                            </tr> --}}
                            <tr>
                                <td>Jenis Pembayaran</td>
                                <td> : </td>
                                <td>Transfer Virtual Account</td>
                            </tr>
                            <tr>
                                <td style="color: red"><strong>External ID (For Demo Purpose)</strong></td>
                                <td> : </td>
                                <td style="color: red">{{ $getPaymentDetails->external_id }}</td>
                            </tr>
                            <tr>
                                <td>Bank</td>
                                <td> : </td>
                                <td>{{ $getPaymentDetails->bank_code }}</td>
                            </tr>
                            <tr>
                                <td>Nomor Rekening</td>
                                <td> : </td>
                                <td><strong>{{ $getPaymentDetails->account_number }}</strong></td>
                            </tr>
                            <tr>
                                <td>Jumlah Yang Dibayarkan</td>
                                <td> : </td>
                                <td><strong>Rp {{ number_format($getPaymentDetails->expected_amount) }}</strong></td>
                            </tr>
                        </table>
                        <div class="alert alert-primary" role="alert">
                            Untuk Bypass Payment Virtual Account menggunakan Postman:
                            <ul>
                                <li>Method POST, URL
                                    https://api.xendit.co/callback_virtual_accounts/external_id={external_id}/simulate_payment
                                </li>
                                <li>Isikan external_id={external_id} menjadi external_id=No External ID</li>
                                <li>Body JSON: <br>
                                    {
                                    "amount": sesuaikan dengan amount
                                    }
                                </li>
                                <li>
                                    Authorization: Basic Auth<br>
                                    username :
                                    xnd_development_tsIIB4Imbw0o9VgFegCfBmAsxflTru6BXEjH4ZKv81uguccmpcqoBVydg9At5w03
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                @endif
                <div style="text-align: center">
                    <button type="button" class="btn btn-primary"
                        onclick=" window.open('{{ url('purchase_history') }}', '_self');">
                        Kembali Ke Halaman Order</button>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('script')
<script>

</script>
@endsection