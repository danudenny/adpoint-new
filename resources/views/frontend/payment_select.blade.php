@extends('frontend.layouts.app')

@section('header')

<style>
    div.list-group-item:hover {
        background-color: #eaeaea !important;
    }

    div.list-group-item.active {
        background-color: #052141 !important;
        color: white !important;
    }

    #accordion.card-header {
        background-color: #052141 !important;
        color: white !important;
        text-decoration: none
    }
</style>

@endsection

@section('content')

<div id="page-content">
    <section class="slice-xs sct-color-2 border-bottom">
        <div class="container container-sm">
            <div class="row cols-delimited">
                <div class="col-4">
                    <div class="icon-block icon-block--style-1-v5 text-center">
                        <div class="block-icon c-gray-light mb-0">
                            <i class="la la-shopping-cart"></i>
                        </div>
                        <div class="block-content d-none d-md-block">
                            <h3 class="heading heading-sm strong-300 c-gray-light text-capitalize">1. {{__('My Cart')}}
                            </h3>
                        </div>
                    </div>
                </div>

                <div class="col-4">
                    <div class="icon-block icon-block--style-1-v5 text-center">
                        <div class="block-icon c-gray-light mb-0">
                            <i class="la la-truck"></i>
                        </div>
                        <div class="block-content d-none d-md-block">
                            <h3 class="heading heading-sm strong-300 c-gray-light text-capitalize">2. {{__('Billing
                                info')}}</h3>
                        </div>
                    </div>
                </div>

                <div class="col-4">
                    <div class="icon-block icon-block--style-1-v5 text-center">
                        <div class="block-icon mb-0">
                            <i class="la la-credit-card" style="color: #ff9400"></i>
                        </div>
                        <div class="block-content d-none d-md-block">
                            <h3 class="heading heading-sm strong-300 c-gray-light text-capitalize">4. {{__('Payment')}}
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>




    <section class="py-3 gry-bg">
        <div class="container">
            <div class="row cols-xs-space cols-sm-space cols-md-space">
                <div class="col-lg-8">
                    <form action="{{ route('payment.checkout') }}" class="form-default" data-toggle="validator"
                        role="form" method="POST" id="checkout-form">
                        @csrf
                        <div class="card">
                            <div class="card-title px-4 py-3">
                                <h3 class="heading heading-5 strong-500">
                                    {{__('Select a payment option')}}
                                </h3>
                            </div>
                            <input type="hidden" name="payment_option" id="payment_option" value="">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div id="accordion">
                                            <div class="card">
                                                <div class="card-header" id="headingOne" data-toggle="collapse"
                                                    data-target="#collapseOne" aria-expanded="true"
                                                    aria-controls="collapseOne"
                                                    style="cursor: pointer; background-color:#afafaf;">
                                                    <h5 class="mb-0" style="color: white; text-decoration: none">
                                                        TRANSFER BANK (Check Manual)
                                                    </h5>
                                                </div>

                                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                                                    data-parent="#accordion">
                                                    <div class="card-body">
                                                        <div class="list-group-item list-group-item-action flex-column align-items-start"
                                                            id="mandiri_manual">
                                                            <div class="d-flex w-100 justify-start">
                                                                <img src="{{ asset('frontend/images/icons/cards/mandiri.png')}}"
                                                                    class="img-fluid" width="150px">
                                                            </div>
                                                            <p class="mt-2 mb-2">No. Rekening <b>13300290929</b>
                                                            </p>
                                                            <small>PT. Adpoint Media Online</small>
                                                        </div>
                                                        <div class="list-group-item list-group-item-action flex-column align-items-start"
                                                            id="bca_manual">
                                                            <div class="d-flex w-100 justify-start">
                                                                <img src="{{ asset('frontend/images/icons/cards/bca.png')}}"
                                                                    class="img-fluid" width="100px">
                                                            </div>
                                                            <p class="mt-2 mb-2">No. Rekening <b>14090279782</b>
                                                            </p>
                                                            <small class="text-muted">PT. Adpoint Media Online</small>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card">
                                                <div class="card-header collapsed" id="headingTwo"
                                                    data-toggle="collapse" data-target="#collapseTwo"
                                                    aria-expanded="false" aria-controls="collapseTwo"
                                                    style="cursor: pointer; background-color:#afafaf;">
                                                    <h5 class="mb-0" style="color: white; text-decoration: none">VIRTUAL
                                                        ACCOUNTS (Check Otomatis)
                                                    </h5>
                                                </div>
                                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo"
                                                    data-parent="#accordion">
                                                    <div class="card-body">
                                                        <div class="list-group">
                                                            <div class="list-group-item list-group-item-action flex-column align-items-start"
                                                                id="payment_auto_mandiri">
                                                                <div class="d-flex w-100 justify-content-between">
                                                                    <img src="{{ asset('frontend/images/icons/cards/mandiri.png')}}"
                                                                        class="img-fluid" width="150px">
                                                                </div>
                                                            </div>
                                                            <div class="list-group-item list-group-item-action flex-column align-items-start"
                                                                id="payment_auto_bca">
                                                                <div class="d-flex w-100 justify-content-between">
                                                                    <img src="{{ asset('frontend/images/icons/cards/bca.png')}}"
                                                                        class="img-fluid" width="100px">
                                                                    <small class="text-muted">Check Otomatis</small>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row align-items-center pt-4">
                            <div class="col-6">
                                <a href="{{ route('home') }}" class="btn btn-danger btn-circle">
                                    <i class="la la-mail-reply"></i>
                                    {{__('Return to shop')}}
                                </a>
                            </div>
                            <div class="col-6 text-right">
                                <button type="submit" onclick="moveTab('#nav-unpaid','{{ route('trx.unpaid') }}')"
                                    id="complete" style="cursor: not-allowed" class="btn btn-orange btn-circle"
                                    disabled>
                                    <i class="fa fa-location-arrow"></i> {{__('Place Order')}}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="col-lg-4 ml-lg-auto">
                    @include('frontend.partials.cart_summary')
                </div>
            </div>
        </div>
    </section>
</div>
@endsection

@section('script')
<script type="text/javascript">
    $('#bca_manual').on('click', function(e) {
            document.getElementById('payment_option').value = 'BCA MANUAL';
            $('.list-group-item').removeClass("active");
            $(this).addClass("active rounded");
            enabledbutton();
        });
        $('#mandiri_manual').on('click', function(e) {
            document.getElementById('payment_option').value = 'MANDIRI MANUAL';
            $('.list-group-item').removeClass("active");
            $(this).addClass("active rounded");
            enabledbutton();
        });

        $('#payment_auto_bca').on('click', function(e) {
            document.getElementById('payment_option').value = 'BCA';
            $('.list-group-item').removeClass("active");
            $(this).addClass("active rounded");
            enabledbutton();
        });
        $('#payment_auto_mandiri').on('click', function(e) {
            document.getElementById('payment_option').value = 'MANDIRI';
            $('.list-group-item').removeClass("active");
            $(this).addClass("active rounded");
            enabledbutton();
        });

        function use_wallet(){
            $('input[name=payment_option]').val('wallet');
            $('#checkout-form').submit();
        }

        function enabledbutton(){
            $('#complete').prop('disabled', false);
            $('#complete').removeAttr('style');
        }

        // $('#mandiri').on('click', function(){
        //     enabledbutton();
        //     var body = `<div class="card border-dark mb-3" style="max-width: 18rem;">
        //                     <div class="card-body text-dark">
        //                         <h5 class="card-title">Bank Mandiri</h5>
        //                         <h5 class="card-title">13300290929</h5>
        //                         <p class="card-text">PT. Adpoint Media Online</p>
        //                     </div>
        //                 </div>`
        //     $('#detailbank').html(body);
        // })
        // $('#bca').on('click', function(){
        //     enabledbutton();
        //     var body = `<div class="card border-dark mb-3" style="max-width: 18rem;">
        //                     <div class="card-body text-dark">
        //                         <h5 class="card-title">Bank BCA</h5>
        //                         <h5 class="card-title">14090279782</h5>
        //                         <p class="card-text">PT. Adpoint Media Online</p>
        //                     </div>
        //                 </div>`
        //     $('#detailbank').html(body);
        // })
        
        function moveTab(activeTab, routeTab) {
            localStorage.setItem('activeTabTrx', activeTab);
            localStorage.setItem('routeTabTrx', routeTab);
        }

</script>
@endsection