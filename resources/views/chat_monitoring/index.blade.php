@extends('layouts.app')

@section('content')

<!-- Basic Data Tables -->
<!--===================================================-->
<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">{{__('Chat Monitoring')}}</h3>
    </div>
    <div class="panel-body">
        <table class="table table-striped table-bordered demo-dt-basic" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>{{__('From')}}</th>
                    <th>{{__('To')}}</th>
                    <th>{{__('Chat')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($list as $key => $roomChat)
                    <tr>
                        <td>{{ ++$key }}</td>
                        <td>{{ $roomChat->chats[0]['userFrom']['name'] }}</td>
                        <td>{{ $roomChat->chats[0]['userTo']['name'] }}</td>
                        <td>
                            <ul>
                                @foreach ($roomChat->chats as $chat)
                                    <li>
                                        {{ $chat['userFrom']['name'] }} : {{ $chat['body'] }}
                                    </li>
                                @endforeach
                            </ul>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

    </div>
</div>

@endsection

@section('script')

@endsection
