@extends('layouts.app')

@section('content')

    <!-- Basic Data Tables -->
    <!--===================================================-->
    @if (Session::has('message'))
        <div class="alert alert-success">
            {!! session('message') !!}
        </div>
    @endif
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">{{__('Invoices')}}</h3>
        </div>
        <div class="panel-body">
            <table class="table table-striped table-bordered demo-dt-basic" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Invoice Number</th>
                    <th>Transaction Code</th>
                    <th>Transaction Date</th>
                    <th>Payment Type</th>
                    <th width="10%">{{__('options')}}</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($invoices as $key => $invoice)
                    <tr>
                        <td>{{ $key+1 }}</td>
                        <td>
                            {{ $invoice->inv_number }}
                        </td>
                        <td>
                            {{ $invoice->tr_number }}
                        </td>
                        <td>
                            {{ date('d M Y H:i:s', strtotime($invoice->transaction_date)) }}
                        </td>
                        <td>
                            {{ $invoice->payment_type }}
                        </td>
                        <td>
                            <a class="btn btn-primary"
                               href="{{ route('invoices.detail', encrypt($invoice->id)) }}">{{__('Details')}}</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>

            </table>

        </div>
    </div>


@endsection
