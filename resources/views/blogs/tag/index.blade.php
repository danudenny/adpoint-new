@extends('layouts.app')

@section('content')
    <div class="tambah-button" style="display: flex; justify-content: end; margin-bottom: 10px">
        <a href="javascript:void(0)" data-backdrop="static" data-keyboard="false" class="btn btn-success"
           id="newTag"><i class="fa fa-plus" title="Tag Baru"></i> New Tag</a>
    </div>
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">{{__('Blog Tags')}}</h3>
        </div>
        <div class="panel-body">
            <table id="example" class="table table-bordered data-table">
                <thead>
                <tr>
                    <th style="text-align: center;">{{ __('No.')}}</th>
                    <th style="text-align: center;">{{ __('Title')}}</th>
                    <th style="text-align: center;">{{ __('Created At')}}</th>
                    <th style="text-align: center;">{{ __('Action')}}</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        @include('blogs.tag.create')
        @include('blogs.tag.update')
    </div>

    @yield('scripts')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    @if( Session::has('message'))
        <script>
            var type = "{{ Session::get('alert-type', 'info') }}";
            switch(type){
                case 'info':
                    toastr.info("{{ Session::get('message') }}");
                    break;

                case 'warning':
                    toastr.warning("{{ Session::get('message') }}");
                    break;

                case 'success':
                    toastr.success("{{ Session::get('message') }}");
                    break;

                case 'error':
                    toastr.error("{{ Session::get('message') }}");
                    break;
            }
        </script>
    @endif

    <script>
        $("body").on("click","#newTag",function(e){
            e.preventDefault;
            $('#newCategoryModal').html("Add New Tag");
            $('#submit').val("Save Data");
            $('#modal-id').modal('show', {backdrop: 'static', keyboard: false});
            $('#formCategories').trigger("reset");
        });

        $("body").on("click","#updateTag",function(e){
            e.preventDefault;
            let id = $(this).attr('data-id');
            let title = $(this).attr('data-title');

            $('#updateCategoryModal').html("Update Tag");
            $('#submit-update').val("Update Data");
            $('#modal-update-id').modal('show', {backdrop: 'static', keyboard: false});
            $('#formUpdateCategories').trigger("reset");

            $("#title_update").val(title);
            $("#id_tag_update").val(id);
        });

        $(function() {
            let table = $('#example').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                autoWidth: false,
                deferRender: true,
                ajax: {
                    url:"{{ route('blog_tags.index') }}",
                    data:function(d){
                        return d
                    }
                },
                columns: [
                    { data: 'DT_RowIndex'},
                    { data: 'title', name: 'title'},
                    { data: 'created_at', name: 'created_at' },
                    { data: 'action', name: 'action', orderable: false, searchable: false }
                ],
                columnDefs: [
                    {
                        targets: 0,
                        className: "text-center",
                        width: "4%"
                    },
                    {
                        targets: [1, 2, 3],
                        className: "text-center",
                        width: "20%"
                    },
                    { searchable: true, targets: 1 }
                ],
            });

        });
    </script>
@endsection
