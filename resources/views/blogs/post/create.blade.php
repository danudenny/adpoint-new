@extends('layouts.app')

@section('content')

<div class="col-sm-12">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">{{__('Create New Post')}}</h3>
        </div>
        <!--Horizontal Form-->
        <!--===================================================-->
        <form class="form-horizontal" action="{{ route('blog.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="title">{{__('Title')}}</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="title" id="title" required placeholder="{{__('Title')}}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">{{__('Meta Title')}}</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="meta_title" id="meta_title" required placeholder="{{__('Meta Title')}}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label" for="category">{{__('Category')}}</label>
                    <div class="col-sm-10">
                        <select class="form-control" name="category_id">
                            @foreach($categories as $category)
                                <option value="{{$category->id}}">{{$category->title}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label" for="name">{{__('Tags')}}</label>
                    <div class="col-sm-10">
                        <select name="tag_id[]" id="tag_id" class="form-control demo-select2" multiple="multiple" required data-placeholder="Choose Tags">
                            @foreach($tags as $tag)
                                <option value="{{$tag->id}}">{{$tag->title}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">{{__('Blog Images')}}</label>
                    <div class="col-sm-10">
                        <input type="file" class="form-control" id="images" name="images[]" multiple="multiple" required accept="image/gif, image/jpeg, image/jpg, image/png">
                    </div>
                </div>

                <div class="form-group">

                    <label class="col-sm-2 control-label" for="content">{{__('Content')}}</label>
                    <div class="col-sm-10">
                        <textarea class="editor" id="content" name="content" required></textarea>
                    </div>

                </div>
            </div>
            <div class="panel-footer text-right">
                <button class="btn btn-purple" type="submit">{{__('Save')}}</button>
            </div>
        </form>

        <!--===================================================-->
        <!--End Horizontal Form-->

    </div>
</div>

@endsection
