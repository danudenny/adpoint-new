@extends('layouts.app')

@section('content')
<div style="margin-bottom: 10px; display: flex; flex-direction: row; justify-content: end;">
    <a class="btn btn-success" href="{{ route('blog.create') }}"><i class="fa fa-plus"></i> CREATE NEW POST</a>
</div>
<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">{{__('Blog Post')}}</h3>
    </div>
    <div class="panel-body">
        <table class="table table-striped table-bordered demo-dt-basic" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>{{ __('No') }}</th>
                    <th>{{ __('Title') }}</th>
                    <th>{{ __('Categories') }}</th>
                    <th>{{ __('Author') }}</th>
                    <th>{{ __('Published') }}</th>
                    <th>{{ __('Created At') }}</th>
                    <th>{{ __('Options') }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($data as $key => $post)
                <tr>
                    <td>#</td>
                    <td>{{ $post->title }}</td>
                    <td>{{ $post->category->title }}</td>
                    <td>{{ $post->user->name }}</td>
                    <td>
                        <label class="switch">
                        <input onchange="update_post_published(this)" value="{{ $post->id }}" type="checkbox" {{ $post->published == 1 ? 'checked' : '' }}>
                        <span class="slider round"></span></label>
                    </td>
                    <td>{{ $post->created_at }}</td>
                    <td>
                        <div class="btn-group dropdown">
                            <button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                {{__('Actions')}} <i class="dropdown-caret"></i>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li><a href="{{ route('blog.edit', encrypt($post->id)) }}">{{__('Edit')}}</a></li>
                                <li><a onclick="confirm_modal('{{ route('blog.destroy', $post->id) }}');">{{__('Delete')}}</a></li>
                            </ul>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

    </div>
</div>
@endsection

@section('script')
    <script type="text/javascript">
        function update_post_published(el){
            if(el.checked){
                var published = 1;
            }
            else{
                var published = 0;
            }
            $.post('{{ route('blog.published') }}', {_token:'{{ csrf_token() }}', id:el.value, published:published }, function(data){
                if(data == 1){
                    location.reload();
                }else{
                    showAlert('danger', 'Something went wrong');
                }
            });
        }
    </script>
@endsection
