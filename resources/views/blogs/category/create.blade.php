<div class="modal fade" id="modal-id">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title" id="newCategoryModal"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>

            <div class="modal-body">
                <form id="formCategories">
                    @csrf
                    <div class="form-group">
                        <p>Category Name</p>
                        <input type="text" class="form-control" placeholder="Category Name" id="title">
                        <span class="text-danger"><b>{{ $errors->first('title') }}</b></span>
                    </div>
                    <div style="display: flex; flex-direction: row; column-gap: 10px; justify-content: end">
                        <input type="submit" value="Save Category" id="submit" class="btn btn-success">
                        <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">
                            <i class="fa fa-times"></i>
                            Cancel
                        </button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>

<script>
    $('body').on('click', '#submit', function(event) {
        event.preventDefault()
        let title = $("#title").val();

        $.ajax({
            url: "{{ route('blog_categories.create.admin') }}",
            type: "POST",
            data: {
                _token: "{{ csrf_token() }}",
                title: title,
            },
            dataType: 'json',
            success: function(data) {
                $('#formCategories').trigger("reset");
                window.location = "{{ url('admin/blog-categories') }}";
            },
            error: function(data) {
                console.log(data)
                console.log('Error......');
            }
        });
    });
</script>
