<div class="modal fade" id="modal-update-id">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title" id="updateCategoryModal"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>

            <div class="modal-body">
                <form id="formUpdateCategories">
                    @csrf
                    <div class="form-group">
                        <p>Category Name</p>
                        <input hidden name="id" id="id_category_update">
                        <input type="text" class="form-control" placeholder="Category Name" id="title_update">
                        <span class="text-danger"><b>{{ $errors->first('title') }}</b></span>
                    </div>
                    <div style="display: flex; flex-direction: row; column-gap: 10px; justify-content: end">
                        <input type="submit" value="Save Category" id="submit-update" class="btn btn-success">
                        <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">
                            <i class="fa fa-times"></i>
                            Cancel
                        </button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>

<script>
    $('body').on('click', '#submit-update', function(event) {
        event.preventDefault()
        let title = $("#title_update").val();
        let id = $("#id_category_update").val();

        $.ajax({
            url: "{{ route('blog_categories.update.admin') }}",
            type: "POST",
            data: {
                _token: "{{ csrf_token() }}",
                id: id,
                title: title,
            },
            dataType: 'json',
            success: function(data) {
                $('#formUpdateCategories').trigger("reset");
                window.location = "{{ url('admin/blog-categories') }}";
            },
            error: function(data) {
                console.log(data)
                console.log('Error......');
            }
        });
    });
</script>
