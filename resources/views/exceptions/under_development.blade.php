<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Page Under Development | Adpoint</title>
    <link href="{{ asset('frontend/css/under_development.css') }}" rel="stylesheet">
</head>
<body>
    <div class="overlay"></div>
    <div class="stars" aria-hidden="true"></div>
    <div class="starts2" aria-hidden="true"></div>
    <div class="stars3" aria-hidden="true"></div>
    <main class="main">
        <section class="contact">
            <h2 class="sub-title">Sorry..</h2>
            <h2 class="sub-title">Page Under Development</h2>
            <h4><a style="color: white" href="{{ \Illuminate\Support\Facades\URL::previous() }}">Back</a></h4>
        </section>
    </main>
</body>
</html>
